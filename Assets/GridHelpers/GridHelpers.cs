﻿using Gamelogic.Grids;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GridHelpers 
{
    public static FlatHexPoint GetRotatedFlatHexPoint(FlatHexPoint point, Direction rotation)
    {
        switch (rotation)
        {
            case Direction.UpRight:
                return point.Rotate60();
            case Direction.Up:
                return point;
            case Direction.DownRight:
                return point.Rotate120();
            case Direction.Down:
                return point.Rotate180();
            case Direction.DownLeft:
                return point.Rotate240();
            case Direction.UpLeft:
                return point.Rotate300();
        }
        return point;
    }

    public static RectPoint GetRotatedRectPoint(RectPoint point, Direction rotation)
    {
        switch (rotation)
        {
            case Direction.Right:
                return point.Rotate90();
            case Direction.Up:
                return point.Rotate180();
            case Direction.Left:
                return point.Rotate270();
        }
        return point;
    }

    public static FlatHexGrid<Cell> GetNewFlatHexPointGrid()
    {
        return FlatHexGrid<Cell>.FatRectangle(14, 7);
    }

    public static RectGrid<Cell> GetNewRectPointGrid()
    {
        return RectGrid<Cell>.Rectangle(8, 6);
    }

    internal static List<Direction> HexMainDirections()
    {
        return new List<Direction>() { Direction.Up, Direction.UpRight, Direction.DownRight, Direction.Down, Direction.DownLeft, Direction.UpLeft };
    }

    internal static List<Direction> RectMainDirections()
    {
        return new List<Direction>() { Direction.Up, Direction.Right, Direction.Down, Direction.Left };
    }

    public static Direction GetDirectionFromRectPoint(RectPoint p)
    {
        if (p == RectPoint.North)
            return Direction.Up;

        if (p == RectPoint.NorthEast)
            return Direction.UpRight;

        if (p == RectPoint.NorthWest)
            return Direction.UpLeft;

        if (p == RectPoint.SouthWest)
            return Direction.DownLeft;

        if (p == RectPoint.SouthEast)
            return Direction.DownRight;

        if (p == RectPoint.East)
            return Direction.Right;

        if (p == RectPoint.West)
            return Direction.Left;

        return Direction.Down;
    }

    public static RectPoint GetRectPointFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return RectPoint.North;
            case Direction.Down:
                return RectPoint.South;
            case Direction.UpLeft:
                return RectPoint.NorthWest;
            case Direction.UpRight:
                return RectPoint.NorthEast;
            case Direction.DownLeft:
                return RectPoint.SouthWest;
            case Direction.DownRight:
                return RectPoint.SouthEast;
            case Direction.Left:
                return RectPoint.West;
            case Direction.Right:
                return RectPoint.East;
        }

        return RectPoint.Zero;
    }

    public static Direction GetDirectionFromFlatHexPoint(FlatHexPoint p)
    {
        if (p == FlatHexPoint.North)
            return Direction.Up;

        if (p == FlatHexPoint.NorthEast)
            return Direction.UpRight;

        if (p == FlatHexPoint.NorthWest)
            return Direction.UpLeft;

        if (p == FlatHexPoint.SouthWest)
            return Direction.DownLeft;

        if (p == FlatHexPoint.SouthEast)
            return Direction.DownRight;

        return Direction.Down;
    }

    public static FlatHexPoint GetFlatHexPointFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return FlatHexPoint.North;
            case Direction.Down:
                return FlatHexPoint.South;
            case Direction.UpLeft:
                return FlatHexPoint.NorthWest;
            case Direction.UpRight:
                return FlatHexPoint.NorthEast;
            case Direction.DownLeft:
                return FlatHexPoint.SouthWest;
            case Direction.DownRight:
                return FlatHexPoint.SouthEast;
        }

        return FlatHexPoint.Zero;
    }
}
