﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
    None = 0,

    Money = 100,
    EnemySpawnPower = 101,
    PlayerScore = 102,
    EnemyScore = 103,
}

public class ResourceDef
{
    public static Sprite GetResourceSprite(ResourceType type)
    {
        return Helpers.GetSprite(GetResourceIconString(type));
    }

    public static string GetResourceIconString(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Money:
                return "crown-coin";
            case ResourceType.EnemyScore:
            case ResourceType.PlayerScore:
                return "laurel-crown";
            case ResourceType.EnemySpawnPower:
                return "evil-minion";
        }

        return "question";
    }

    public static Color GetResourceColor(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Money:
                return Constants.axesGold;
            case ResourceType.EnemySpawnPower:
            case ResourceType.EnemyScore:
                return Constants.GetPlayerColor(Player.Opponent);
            case ResourceType.PlayerScore:
                return Constants.GetPlayerColor(Player.You);
        }
        return Color.white;
    }

    internal static string GetResourceDesc(ResourceType resourceType)
    {
        return resourceType.ToString();
    }

    public static bool ShouldBeDisplayed(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Money:
                return false;
        }
        return true;
    }

    internal static string GetTextCode(ResourceType type)
    {
        return "{H}";
    }
}
