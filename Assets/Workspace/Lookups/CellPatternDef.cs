﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellPatternType
{
    None = 0,
    L = 1,
    T = 2,
    Line = 3,
    Knight = 4,
    Single = 5,
}

public enum SpaceType
{
    None = 0,
    Blank = 1,
    Start = 2,
    End = 3,
    Filled = 4,
    Tile = 5,
}

public static class CellPatternDef
{
    public static Color validPlaceColor = Helpers.HexToColor("1ABF1A77");//shows on each cell that is valid when hovering
    public static Color invalidPlaceColor = Color.red * 0.5f; //shows on each cell that is invalid when hvoering
    public static Color placedHighlightColor = Color.yellow * 0.5f; //shows placed cpo cells on the board

    public static CellPatternType GetNextCellPatternType()
    {
        return CellPatternType.Single;

        return Helpers.GetAllEnumTypes<CellPatternType>().RandomElement();
    }

    public static ActorType GetActorType(SpaceType type)
    {
        switch (type)
        {
            case SpaceType.Filled:
                return Helpers.ChanceRoll() ? ActorType.None : Actor.GetPlayerUnitTypes().RandomElement();
        }

        return Actor.GetPlayerUnitTypes().RandomElement();
    }

    public static TileType GetTileType(SpaceType type)
    {
        switch (type)
        {
            case SpaceType.Filled:
                return Helpers.GetRandomEnumType<TileType>();
        }

        return TileType.None;
    }

    public static CellPattern GetCellPattern(CellPatternType type, bool makeVisuals = true)
    {
        CellPattern p = MakeNewCellPatternObject(type);
        p.makeVisuals = makeVisuals;

        switch (type)
        {
            case CellPatternType.L:
                Create(0, 0, SpaceType.Filled, p);
                Create(0, 1, SpaceType.Filled, p);
                Create(0, 2, SpaceType.Filled, p);
                Create(1, 2, SpaceType.Filled, p);
                break;
            case CellPatternType.T:
                Create(0, 0, SpaceType.Filled, p);
                Create(0, 1, SpaceType.Filled, p);
                Create(1, 0, SpaceType.Filled, p);
                break;
            case CellPatternType.Line:
                Create(0, 0, SpaceType.Filled, p);
                Create(0, 1, SpaceType.Filled, p);
                Create(0, 2, SpaceType.Filled, p);
                Create(0, 3, SpaceType.Filled, p);
                break;
            case CellPatternType.Knight:
                Create(0, 0, SpaceType.Start, p);
                Create(0, 1, SpaceType.Blank, p);
                Create(0, 2, SpaceType.Blank, p);
                Create(1, 2, SpaceType.End, p);
                break;
        }

        p.Init(type);

        return p;
    }

    internal static CellPattern MakeNewCellPatternObject(CellPatternType cellPatternType)
    {
        CellPattern cpo = Helpers.CreateInstance<CellPattern>("CellPatternObject", null);
        cpo.Init(cellPatternType);
        return cpo;
    }

    static CellPatternSpace Create(int x, int y, SpaceType sType, CellPattern p)
    {
        CellPatternSpace cps = Helpers.CreateInstance<CellPatternSpace>("CellPatternSpaceObject", p.transform);
        cps.Init(x, y, sType, p.makeVisuals);
        p.AddPatternSpace(cps);
        return cps;
    }
}
