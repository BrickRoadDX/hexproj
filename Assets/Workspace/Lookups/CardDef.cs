﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardType
{
    None = 0,

    Knight = 101,
    One = 1,
    Two = 2,
    Three = 3,

    Soldier = 10,
}

public class CardDef : MonoBehaviour
{
    public static Sprite GetBackgroundSprite(CardType type)
    {
        switch (type)
        {
            case CardType.Knight:
                return Helpers.GetSprite("cardfront");
        }

        return Helpers.GetSprite("cardfront");
    }

    public static Color GetBackgroundColor(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return Color.black;
            case CardType.Knight:
                return Color.white;
        }

        return Color.white;
    }

    internal static bool ValidCardPlacement(Card dragCard, Cell cell)
    {
        switch(dragCard.cardType)
        {
            case CardType.Soldier:
                return !cell.isOccupied;
            case CardType.Knight:
                return cell.HasReadyUnit();
        }
        return false;
    }

    public static string GetTitleText(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return "";
            case CardType.Knight:
                return "Concept";
        }

        return type.ToString();
    }

    internal static string GetMiddleText(CardType type)
    {
        switch (type)
        {
            case CardType.Knight:
                return "Draw 2 Cards";
        }

        return "Hello my name is brett and this is a long card string";

        return type.ToString();
    }

    internal static Cost GetCost(CardType type)
    {
        switch(type)
        {
            case CardType.One:
                return new Cost(1);
            case CardType.Two:
                return new Cost(2);
            case CardType.Three:
                return new Cost(3);
            case CardType.Knight:
                return new Cost(5);
        }

        return new Cost(1);
    }

    public static CellPattern GetCellPatternForCardType(CardType type)
    {
        switch(type)
        {
            case CardType.Knight:
                return CellPatternDef.GetCellPattern(CellPatternType.Knight, false);
        }

        return null;
    }

    public static bool CanPlayOnBoard(CardType type)
    {
        switch(type)
        {
            case CardType.Soldier:
            case CardType.Knight:
                return true;
        }
        return false;
    }

    public static bool MovesToBoard(CardType type)
    {
        switch (type)
        {
            case CardType.Soldier:
                return true;
        }
        return false;
    }

    public static float CardScaleOnBoard()
    {
        return 0.5f;
    }
}
