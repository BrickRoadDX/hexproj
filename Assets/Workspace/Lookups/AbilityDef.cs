﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityType
{
    None = 0,
    Move = 1,
    Attack = 2,
    MoveAttack = 3,
    GainMoney = 4,
}

public static class AbilityDef 
{
    public static Color GetAbilitySpriteColor(AbilityType abilityType)
    {
        switch(abilityType)
        {
            case AbilityType.Attack:
                return Constants.bggRed;
            case AbilityType.Move:
                return Constants.bggYellow;
            case AbilityType.MoveAttack:
                return Constants.bggGreen;
            case AbilityType.GainMoney:
                return ResourceDef.GetResourceColor(ResourceType.Money);
        }

        return Color.magenta;
    }

    public static Color GetAbilityBackgroundColor(AbilityType abilityType)
    {
        return GetAbilitySpriteColor(abilityType) + (Color.grey * 0.5f);
    }

    public static string GetAbilitySpriteName(AbilityType abilityType)
    {
        switch(abilityType)
        {
            case AbilityType.Attack:
                return "screen-impact";
            case AbilityType.Move:
                return "boot";
            case AbilityType.MoveAttack:
                return "move-attack";
            case AbilityType.GainMoney:
                return ResourceDef.GetResourceIconString(ResourceType.Money);
        }

        return "question";
    }

    public static bool RequiresUnitSelection(AbilityType type)
    {
        switch (type)
        {
            case AbilityType.Move:
            case AbilityType.Attack:
            case AbilityType.MoveAttack:
                return true;
        }
        return false;
    }

    public static void DoAbility(AbilityType type)
    {
        switch(type)
        {
            case AbilityType.GainMoney:
                ResourceManager.Instance.Gain(ResourceType.Money, 3);
                AbilityManager.Instance.AbilityDone();
                break;
        }
    }

    public static bool CanBeUsed(AbilityType type)
    {
        if (RequiresUnitSelection(type))
            return Board.Instance.NumReadyUnits() > 0;

        return true;
    }

    public static int NumActions(AbilityType type)
    {
        switch(type)
        {
            case AbilityType.Move:
                return 5;
            case AbilityType.MoveAttack:
                return 2;
        }

        return 1;
    }

    public static string GetReminderText(AbilityType type, InputState state)
    {
        switch(type)
        {
            case AbilityType.Move:
                switch(state)
                {
                    case InputState.ChoosingSpace:
                        return "Choose a unit to move";
                    case InputState.Moving:
                        return "Choose a space to move to";
                }
                break;
            case AbilityType.Attack:
                switch (state)
                {
                    case InputState.ChoosingSpace:
                        return "Choose a unit to attack with";
                    case InputState.Attacking:
                        return "Choose a space to attack";
                }
                break;
            case AbilityType.MoveAttack:
                switch (state)
                {
                    case InputState.ChoosingSpace:
                        return "Choose a unit to move with";
                    case InputState.Moving:
                        return "Choose a space to move to";
                    case InputState.Attacking:
                        return "Choose a space to attack";
                }
                break;
        }

        return "";
    }
}
