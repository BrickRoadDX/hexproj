﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DiceType
{
    None = 0,
    Orange = 1,
    Treasure = 2,
}

public enum DieFace
{
    None = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,

    Any = 100,
    Wild = 101,
}


public class DieDef : MonoBehaviour
{
    public static List<DieFace> GetFacesForDiceType(DiceType type)
    {
        switch (type)
        {
            case DiceType.Orange:
            case DiceType.Treasure:
                return new List<DieFace> { DieFace.One, DieFace.Two, DieFace.Three, DieFace.Four, DieFace.Five, DieFace.Six };
        }

        return new List<DieFace> { DieFace.One };
    }

    public static string GetDieFaceSpriteName(DieFace type)
    {
        switch (type)
        {
            case DieFace.One:
                return "inverted-dice-1";
            case DieFace.Two:
                return "inverted-dice-2";
            case DieFace.Three:
                return "inverted-dice-3";
            case DieFace.Four:
                return "inverted-dice-4";
            case DieFace.Five:
                return "inverted-dice-5";
            case DieFace.Six:
                return "inverted-dice-6";
            case DieFace.Any:
                return "asterisk";
        }

        return "question-mark";
    }

    public static Color GetDieFaceColor(DieFace face)
    {
        return default(Color);
    }

    public static string GetFaceDesc(DieFace actionType)
    {
        switch (actionType)
        {
            case DieFace.One:
                return "Has one pip";
        }

        return "";
    }

    public static Color GetDieColor(DiceType type)
    {
        switch (type)
        {
            case DiceType.Orange:
                return Helpers.HexToColor("FD5C02");
            case DiceType.Treasure:
                return Constants.axesGold;
        }
        return Helpers.Color255(16, 25, 41);
    }

    public static int DiceFaceValue(DieFace face)
    {
        if ((int)face <= 6)
        {
            return (int)face;
        }

        return 0;
    }

    public static bool CanPlayOnBoard(DiceType type)
    {
        switch (type)
        {
            case DiceType.Orange:
            case DiceType.Treasure:
                return true;
        }
        return false;
    }

    public static bool MovesToBoard(DiceType type)
    {
        switch (type)
        {
            case DiceType.Orange:
                return true;
        }
        return false;
    }

    public static float DiceScaleOnBoard()
    {
        return 0.5f;
    }

    internal static bool ValidDiePlacement(Die dragDie, Cell cell)
    {
        switch (dragDie.diceType)
        {
            case DiceType.Orange:
                return !cell.isOccupied;
            case DiceType.Treasure:
                return !cell.isOccupied;
        }
        return false;
    }

    internal static float DieScaleOnBoard()
    {
        return 0.5f;
    }

    public static string GetDieName(DiceType diceType)
    {
        return diceType.ToString();
    }

    internal static string GetDieDesc(DiceType diceType)
    {
        switch (diceType)
        {
            case DiceType.Treasure:
                return "Play on any space to spawn one pickup for each pip shown";

        }
        return "";
    }

    public static bool TryDoRightClickAbility(Die clicked)
    {
        switch (clicked.diceType)
        {
            case DiceType.Orange:
            case DiceType.Treasure:
                ResourceManager.Instance.Gain(ResourceType.Money, clicked.DieValue());
                return true;
        }
        return false;
    }
}
