﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TerrainType
{
    None = 0,

    Road = 1,
    Grass = 2,
    Forest = 3,
    Water = 5,
    Ice = 6,
    Mountain = 7,

    Stronghold = 20,

    Hole = 100,
}

public class TerrainDef 
{
    public static TerrainType GetDefaultTerrainType()
    {
        return TerrainType.Grass;
    }

    public static bool HasTerrainSprite(TerrainType type)
    {
        switch(type)
        {
            case TerrainType.Grass:
            case TerrainType.Forest:
            case TerrainType.Stronghold:
                return true;
        }
        return false;
    }

    public static string GetBackgroundSpriteString(TerrainType type)
    {
        //switch (type) //for a see through hole
        //{
        //    case TerrainType.Hole:
        //        return "holeSprite";
        //}
        return "512hex";
    }


    public static Color GetBackgroundSpriteColor(TerrainType type, Color defaultColor = default(Color))
    {
        switch(type)
        {
            case TerrainType.Grass:
                return Helpers.HexToColor("239510");
            case TerrainType.Forest:
                return Helpers.HexToColor("223E2A");
            case TerrainType.Stronghold:
                return Helpers.HexToColor("9FA0BF");
            case TerrainType.Road:
                return Helpers.HexToColor("9FA0BF");
        }

        return defaultColor;
    }

    public static string GetTerrainSpriteString(TerrainType type)
    {
        switch(type)
        {
            case TerrainType.Grass:
                return "simplegrass";
            case TerrainType.Stronghold:
                return "castle";
            case TerrainType.Forest:
                return "forest";
        }
        return "";
    }

    public static Color GetTerrainSpriteColor(TerrainType type)
    {
        switch (type)
        {
            case TerrainType.Grass:
                return Helpers.HexToColor("1ABB00");
            case TerrainType.Forest:
                return Helpers.HexToColor("446642");
            case TerrainType.Stronghold:
                return Helpers.HexToColor("3E3E42");
        }

        return Color.white;
    }

    public static Vector3 GetLocalScale(TerrainType type)
    {
        switch(type)
        {
            case TerrainType.Forest:
            case TerrainType.Stronghold:
                return Vector3.one * 0.7f;
        }
        return Vector3.one;
    }

    public static int GetTerrainCost(Cell cell, Actor mover)
    {
        if (cell.isOccupied)
            return 100;

        switch (cell.terrainType)
        {
            case TerrainType.Road:
                return 1;
            case TerrainType.Grass:
                return 2;
            case TerrainType.Forest:
                if (mover != null && mover.type == ActorType.Cavalry)
                    return 100;

                return 3;
            case TerrainType.Stronghold:
                return 2;
        }

        return 1;
    }
}
