﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    None = 0,
    Meeple = 1,
    Pickup = 2,
    Cube = 3,
}

public enum ItemClass
{
    None = 0,
    Red = 1,
    Blue = 2,
    Green = 3,
    Yellow = 4,
    Purple = 5,
}


public static class ItemDef
{
    internal static Vector3 GetItemScale(ItemType iType)
    {
        return Vector3.one * 0.25f;
    }

    internal static string GetSpriteName(ItemType iType)
    {
        switch(iType)
        {
            case ItemType.Meeple:
                return "person";
            case ItemType.Pickup:
                return "star";
        }
        return "question";
    }

    internal static Color GetItemColor(ItemType iType)
    {
        switch(iType)
        {
            case ItemType.Meeple:
                return Color.blue;
            case ItemType.Pickup:
                return Color.yellow;
        }
        return Color.magenta;
    }

    public static bool IsItemSpinning(ItemType iType)
    {
        switch(iType)
        {
            case ItemType.Meeple:
                return false;
            case ItemType.Pickup:
                return true;
        }

        return false;
    }

    internal static Color GetItemClassColor(ItemClass itemClass)
    {
        switch (itemClass)
        {
            case ItemClass.Red:
                return Constants.bggRed;
            case ItemClass.Purple:
                return Constants.bggPurple;
            case ItemClass.Blue:
                return Constants.bggBlue;
            case ItemClass.Yellow:
                return Constants.bggYellow;
            case ItemClass.Green:
                return Constants.bggGreen;
        }

        return Color.magenta;
    }
}
