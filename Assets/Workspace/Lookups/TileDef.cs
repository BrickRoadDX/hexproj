﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    None = 0,
    Stronghold = 1,
    House = 2,
    Field = 3,
    Tower = 4,

    CarcassoneRoad = 10,
}

public enum ExitConfiguration
{
    None = 0,
    Straight = 1,
    YCross = 2,
    SwitchBack = 3,
}

public enum ExitType
{
    None = 0,
    Road = 1,
}


public static class TileDef
{
    public static Vector3 GetTileScale(TileType tType)
    {
        return Vector3.one * 0.7f;
    }

    internal static string GetSpriteName(TileType tType)
    {
        switch (tType)
        {
            case TileType.Stronghold:
                return "castle";
            case TileType.Tower:
                return "evil-tower";
            case TileType.House:
                return "village";
            case TileType.Field:
                return "grass";
        }
        return "";
    }

    internal static Color GetSpriteColor(TileType tType)
    {
        switch (tType)
        {
            case TileType.Stronghold:
                return Color.gray;
            case TileType.House:
                return Constants.axesRoad;
            case TileType.Field:
                return Constants.axesGold;
            case TileType.Tower:
                return Constants.takeoverPurple;
        }
        return Color.black;
    }

    internal static Color GetBackroundColor(TileType tType)
    {
        switch (tType)
        {
            case TileType.Stronghold:
                return Color.black;
        }
        return Color.clear;
    }

    public static string GetExitSpriteName(ExitType type)
    {
        return "star";
    }

    internal static bool ShouldRotate(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.CarcassoneRoad:
                return true;
        }
        return false;
    }

    public static Color GetExitColor(ExitType type)
    {
        return Color.white;
    }

    public static Vector3 GetExitScale(ExitType type)
    {
        return Vector3.one * 0.1f;
    }

    public static Dictionary<Direction, ExitType> GetExits(TileType tileType)
    {
        Dictionary<Direction, ExitType> exits = BlankExits();

        switch (tileType)
        {
            case TileType.CarcassoneRoad:

                ExitConfiguration exitConfig = Helpers.GetRandomEnumType<ExitConfiguration>();
                foreach (Direction d in GetDirectionList(exitConfig))
                    exits[d] = ExitType.Road;
                break;
        }

        return exits;
    }

    public static Dictionary<Direction, ExitType> BlankExits()
    {
        Dictionary<Direction, ExitType> exits = new Dictionary<Direction, ExitType>();

        foreach (Direction d in Helpers.MainDirections())
            exits.Add(d, ExitType.None);

        return exits;
    }

    public static List<Direction> GetDirectionList(ExitConfiguration exitConfiguration)
    {
        List<Direction> dirList = new List<Direction>();
        switch (exitConfiguration)
        {
            case ExitConfiguration.Straight:
                return new List<Direction>() { Direction.Up, Direction.Down };
            case ExitConfiguration.YCross:
                return new List<Direction>() { Direction.Down, Direction.UpRight, Direction.UpLeft };
            case ExitConfiguration.SwitchBack:
                return new List<Direction>() { Direction.DownRight, Direction.UpRight };
        }
        return new List<Direction>();
    }
}
