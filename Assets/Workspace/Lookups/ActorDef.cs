﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActorType
{
    None = 0,
    Hunter = 1,
    Armored = 2,
    Cavalry = 3,
    Pikemen = 4,


    Minion = 100,
    Bruiser = 101,

    Explosive = 1000,
}

public enum ActorStatType
{
    None = 0,
    Power = 1,
    Toughness = 2,
    Health = 3,
    Speed = 4,
}

public class ActorDef : MonoBehaviour
{
    public static int NumActorsPerCell()
    {
        return 1;
    }

    public static bool IsEnemy(ActorType type)
    {
        return (int)type >= 100 && (int)type < 1000;
    }

    public static bool IsPlayerUnit(ActorType type)
    {
        return (int)type < 100 && type != ActorType.None;
    }

    public static Vector3 GetActorScale(ActorType type)
    {
        return Vector3.one * 0.7f;
    }

    internal static Color GetActorColor(ActorType type)
    {
        switch (type)
        {
            case ActorType.Minion:
            case ActorType.Bruiser:
                return Color.black;

            case ActorType.Hunter:
            case ActorType.Pikemen:
            case ActorType.Armored:
                return Constants.axesMeat;
            case ActorType.Cavalry:
                return Constants.axesRoad;

        }
        return Color.black;
    }

    public static string GetSpriteName(ActorType type)
    {
        switch (type)
        {
            case ActorType.Minion:
                return "evil-minion";
            case ActorType.Hunter:
                return "caveman";
            case ActorType.Cavalry:
                return "mounted-knight";
            case ActorType.Armored:
                return "shield-bash";
            case ActorType.Pikemen:
                return "pikeman";
            case ActorType.Bruiser:
                return "brute";
        }
        return "question";
    }
    internal static string GetActorName(ActorType actorType)
    {
        return actorType.ToString();
    }

    internal static string GetActorDesc(ActorType actorType)
    {
        switch (actorType)
        {
            case ActorType.Minion:
                return "A snivelling little scamp";
            case ActorType.Hunter:
                return "Can throw a spear once for +1 damage";
            case ActorType.Cavalry:
                return "Gets +1 damage if moves in the same direction on its last 2 steps";
            case ActorType.Armored:
                return "Extra durable";
            case ActorType.Pikemen:
                return "Hits enemies that move into spaces next to it for 1 damage";
            case ActorType.Bruiser:
                return "Tough to take down";
        }
        return "";
    }


    public static Color GetActorStatColor(ActorStatType type)
    {
        switch (type)
        {
            case ActorStatType.Power:
                return Helpers.HexToColor("B53031");
            case ActorStatType.Toughness:
                return Helpers.HexToColor("304DB5");
            case ActorStatType.Health:
                return Helpers.HexToColor("00CC29");
            case ActorStatType.Speed:
                return Helpers.HexToColor("9D9239");
        }
        return Color.magenta;
    }

    public static Dictionary<ActorStatType, int> GetStartStats(ActorType actorType)
    {
        Dictionary<ActorStatType, int> startStats = new Dictionary<ActorStatType, int>();

        return startStats;
    }
}
