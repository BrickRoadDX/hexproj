﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class Game : Singleton<Game> {

    public Camera mainCamera;
    public Camera tk2dCamera;
    public Transform cameraHolder;

    public CardType showcaseCardOverride = CardType.None;

    public EnemyTurnModule enemyTurn = new EnemyTurnModule();

    private void Start()
    {
        NewGame();
    }

    internal void NewGame()
    {
        mGameOver = false;

        Board.Instance.Init();
        ResourceManager.Instance.Init();
        CardManager.Instance.Init();
        InputManager.Instance.Init();
    }

    internal bool IsGameOver()
    {
        return false;
    }

    private bool mGameOver = false;
    public void GameOver(bool playerWon, string gameOverText = "", bool abandon = false)
    {
        if (mGameOver)
            return;

        mGameOver = true;

        PopupManager.Instance.ClearQueue();
        PopupManager.Instance.DismissPopup();

        Board.Instance.RemoveCellHighlights();

        if (playerWon && gameOverText.Length < 1)
            gameOverText = "You won!";

        if (!playerWon && gameOverText.Length < 1)
            gameOverText = "You lost!";

        PopupManager.Instance.ShowPopupOneButton(gameOverText, "New Game", NewGame);
    }

    internal void CheckGameOver()
    {
        if(ResourceManager.Instance.GetValue(ResourceType.PlayerScore) >= 3)
        {
            GameOver(true);
            return;
        }

        if(ResourceManager.Instance.GetValue(ResourceType.EnemyScore) >= 3)
        {
            GameOver(false);
            return;
        }
    }

    void HandleEnemySpawns()
    {
        Helpers.StartWhileLoopCounter();
        bool doneSpawning = false;

        List<Cell> spawnLocations = Board.Instance.GetTerrainCells(TerrainType.Forest);
        spawnLocations.RemoveAll(x => x.isOccupied);

        while (Helpers.WhileLoopTick() && !doneSpawning)
        {
            spawnLocations.Shuffle();

            ActorType toSpawn = Actor.GetNextEnemyUnit();
            int spawnValue = Actor.GetActorBaseHealth(toSpawn);

            if (ResourceManager.Instance.GetValue(ResourceType.EnemySpawnPower) >= spawnValue)
            {
                Cell spawnCell = spawnLocations.RandomElement(true);
                Helpers.ShowCellReminder(spawnCell, "Spawned!");

                spawnCell.SpawnActor(toSpawn);
                ResourceManager.Instance.AdjustValue(ResourceType.EnemySpawnPower, -spawnValue);
            }
            else
            {
                doneSpawning = true;
            }

            if (spawnLocations.Count < 1)
                doneSpawning = true;
        }
    }

    internal void StartEnemyTurn()
    {
        enemyTurn.StartEnemyTurn();

        HandleEnemySpawns();
    }
}
