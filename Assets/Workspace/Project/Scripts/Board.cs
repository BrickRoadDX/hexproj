﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Board : BoardBase
{
    public void Init()
    {
        ClearBoard();

        SetupBoard();

        BuildGridAnimation();
    }

    void SetupBoard()
    {
        foreach (Cell cell in AllCells())
            cell.SetTerrainType(TerrainType.Grass);

        List<Cell> setupCells = AllCells();

        foreach (Cell cell in GetSpacedCells(14, 5))
        {
            cell.SetTerrainType(TerrainType.Stronghold);
            setupCells.Remove(cell);
        }

        for(int i = 0; i < 10; i++)
        {
            setupCells.RandomElement(true).SetTerrainType(TerrainType.Forest);
        }

        for (int i = 0; i < 10; i++)
        {
            setupCells.RandomElement(true).SetTerrainType(TerrainType.Road);
        }

        List<ActorType> typesToSpawn = Actor.GetAllEnemyUnitTypes();

        foreach (ActorType type in typesToSpawn)
            SpawnAppropriateNumber(type);

        CacheEnemyMoves();

        SpawnPlayerUnits();

        CacheEnemyMoves();

        BoardSetupDone();
    }

    public List<Cell> GetUnoccupiedGrassCells()
    {
        List<Cell> cells = AllCells();

        cells.RemoveAll(x => x.isOccupied);
        cells.RemoveAll(x => x.terrainType != TerrainType.Grass);

        return cells;
    }

    void BoardSetupDone()
    {
        foreach (Actor a in actorsOnBoard)
            a.EnteredBoard();
    }

    public void SpawnPlayerUnits()
    {
        List<Cell> spawnCells = GetPlayerSpawnCells();

        for (int i = 0; i < 3; i++)
        {
            Cell c = spawnCells.First();
            if (spawnCells.Count > 0)
                c = spawnCells.PopFirst();

            c.SpawnActor(Actor.GetNextPlayerUnit());
        }
    }

    public List<Cell> GetPlayerSpawnCells()
    {
        List<Cell> cells = GetUnoccupiedCells();
        cells.RemoveAll(x => x.IsThreatened());

        cells.RemoveAll(x => !x.IsTerrainWalkable());
        cells.Shuffle();

        cells.Sort(new CellComparer());

        return cells;
    }

    internal void ResetActorPaths()
    {
        foreach (Actor a in GetPlayerUnits())
            a.pathArrows.ResetPath();
    }

    internal void UpdateCellsFromStateChange()
    {
        foreach (Cell cell in AllCellsDontModify())
            cell.ChangeStateUpdate();

        ResourceManager.Instance.SetValue(ResourceType.PlayerScore, NumStrongholdsControlled(Player.You));
        ResourceManager.Instance.SetValue(ResourceType.EnemyScore, NumStrongholdsControlled(Player.Opponent));
    }

    public int NumStrongholdsControlled(Player player)
    {
        int count = 0;

        foreach(Cell cell in GetTerrainCells(TerrainType.Stronghold))
        {
            if (cell.controllingPlayer == player)
                count++;
        }
        return count;
    }

    public Cell GetClosestUnclaimedStronghold(Cell origin)
    {
        return ClosestCellInCellList(origin, GetUnclaimedStrongholdList());
    }

    public List<Cell> GetUnclaimedStrongholdList()
    {
        List<Cell> candidates = GetTerrainCells(TerrainType.Stronghold);
        candidates.RemoveAll(x => x.controllingPlayer != Player.None);
        return candidates;
    }
}
