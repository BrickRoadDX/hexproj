﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy
{
    public Actor actor;
    public PathArrows actorPath = new PathArrows();

    public Enemy(Actor thisActor)
    {
        actor = thisActor;
        actorPath.Init(actor);
    }

    [HideInInspector]
    public List<Cell> validMoves = new List<Cell>();
    internal void CacheEnemyMoves()
    {
        List<FlatHexPoint> candidates = new List<FlatHexPoint>();

        switch (actor.type)
        {
            case ActorType.Minion:
            case ActorType.Bruiser:
                validMoves = Board.Instance.GetAdjacent(actor.cell);
                validMoves.RemoveAll(x => !x.IsTerrainWalkable());
                //validMoves.RemoveAll(x => x == previousPosition && !x.HasPlayerUnit());
                break;
        }
    }

    public void EnteredBoard()
    {
        switch(actor.type)
        {

        }
    }

    internal void DoEnemyMove()
    {
        if (InputManager.Instance.inputState != InputState.EnemyTurn)
            return;

        if (!actor.isReady)
        {
            actor.SetReady(true);
            return;
        }

        validMoves.RemoveAll(x => x.HasEnemy());

        //if (validMoves.Count > 1)
        //    validMoves.RemoveAll(x => x == actor.previousPosition && !x.HasPlayerUnit());

        if (validMoves.Any(x => x.HasTerrainType(TerrainType.Stronghold)))
            validMoves.RemoveAll(x => !x.HasTerrainType(TerrainType.Stronghold));
        else if (validMoves.Any(x => x.HasPlayerUnit()))
            validMoves.RemoveAll(x => !x.HasPlayerUnit());
        else
        {
            List<FlatHexPoint> path = Board.Instance.ShortestPath(this.actor.cell.point, Board.Instance.GetClosestUnclaimedStronghold(this.actor.cell).point, this.actor, true);
            if (path.Count > 0)
            {
                validMoves.Clear();
                validMoves.Add(path[0].ToCell());
            }
        }

        switch (actor.type)
        {
            case ActorType.Minion:
            case ActorType.Bruiser:
                validMoves.Shuffle();

                if (validMoves.Count > 0)
                {
                    if(validMoves[0].HasPlayerUnit())
                        validMoves[0].Attack(actor, actor.GetCurrentDamage());

                    if(validMoves[0].IsEnemyWalkable())
                        actor.Move(validMoves[0].point);
                }
                break;
        }
    }

    internal void DoPreMove(Action donePreMoveCallback)
    {
        donePreMoveCallback();
    }
}
