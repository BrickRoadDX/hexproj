﻿using DG.Tweening;
using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class Actor : ActorBase
{
    public Enemy enemy;
    public HealthBar healthBar;
    public PathArrows pathArrows = new PathArrows();
    private bool hasSpear = true;

    public override void Init(ActorType type)
    {
        Color healthColor = IsEnemy() ? Color.red : Color.green;
        healthBar = Helpers.MakeHealthBar(this.transform, GetActorBaseHealth(type), "HealthBarPip", new Vector3(0f, -2.5f, 0f), healthColor);
        healthBar.pipDisplay.cols = 5;

        base.Init(type);
        pathArrows.Init(this);
        pathArrows.ResetPath();
        enemy = new Enemy(this);
        hasSpear = true;
    }

    internal void ShowMoves()
    {
        int moveSpeed = GetMovespeed(type);
        List<Cell> moves = Board.Instance.GetMovesFromMovespeed(this.cell, moveSpeed, this).ToCells();
        Board.Instance.HighlightCells(moves);
    }

    internal void ShowAttacks()
    {
        List<Cell> attacks = Board.Instance.PointsWithinDistance(this.cell.point, this.GetCurrentRange()).ToCells();
        if(attacks.Contains(this.cell))
            attacks.Remove(this.cell);
        Board.Instance.HighlightCells(attacks, Color.red * 0.5f);
    }

    internal void SetSpear(bool spear)
    {
        hasSpear = spear;
        if (!hasSpear)
            spriteRenderer.sprite = Helpers.GetSprite("caveman-nospear");
    }



    public static int GetActorBaseHealth(ActorType type)
    {
        switch(type)
        {
            case ActorType.Armored:
                return 3;
            case ActorType.Minion:
                return 2;
            case ActorType.Bruiser:
                return 4;
        }

        return 2;
    }

    public static int GetRange(ActorType type)
    {
        switch(type)
        {
            case ActorType.Hunter:
                return 2;
        }

        return 1;
    }

    public static int GetDamage(ActorType type)
    {
        return 1;
    }

    public static int GetMovespeed(ActorType type)
    {
        switch (type)
        {
            case ActorType.Hunter:
                return 5;
            case ActorType.Cavalry:
                return 6;
            case ActorType.Pikemen:
                return 4;
        }

        return 4;
    }

    internal void HandleMoved()
    {
        if(this.IsEnemy())
        {
            foreach(Cell cell in Board.Instance.GetAdjacent(this.cell))
            {
                if(cell.GetActorType() == ActorType.Pikemen)
                {
                    this.Attack(cell.GetRandomActor(), 1);
                    Helpers.ShowCellReminder(cell, "Pikeman attack!");
                }
            }
        }
    }

    public void EnteredBoard()
    {
        if (enemy != null)
            enemy.EnteredBoard();
    }

    public void CacheEnemyMoves()
    {
        if (enemy != null)
            enemy.CacheEnemyMoves();
    }

    public bool HasPreMove()
    {
        if (enemy != null)
            return false;

        return false;
    }

    public void DoPreMove(Action donePreMoveCallback)
    {
        if (enemy != null)
            enemy.DoPreMove(donePreMoveCallback);
        else
            donePreMoveCallback();
    }

    public void DoEnemyMove()
    {
        if (enemy != null)
            enemy.DoEnemyMove();
    }

    public List<Cell> EnemyValidMoves()
    {
        if (enemy != null)
            return enemy.validMoves;

        return new List<Cell>();
    }

    static List<ActorType> playerUnits = new List<ActorType>();
    public static ActorType GetNextPlayerUnit()
    {
        if(playerUnits.Count < 1)
        {
            playerUnits = Helpers.GetAllEnumTypes<ActorType>(true);
            playerUnits.RemoveAll(x => !ActorDef.IsPlayerUnit(x));
        }

        return playerUnits.RandomElement(true);
    }

    static List<ActorType> enemyUnits = new List<ActorType>();
    public static ActorType GetNextEnemyUnit()
    {
        if (enemyUnits.Count < 1)
            enemyUnits = GetAllEnemyUnitTypes();

        return enemyUnits.RandomElement(true);
    }

    public static List<ActorType> GetAllEnemyUnitTypes()
    {
        List<ActorType> enemyUnits = Helpers.GetAllEnumTypes<ActorType>(true);
        enemyUnits.RemoveAll(x => !ActorDef.IsEnemy(x));

        return enemyUnits;
    }

    private bool charging = false;
    internal void CheckForCharge()
    {
        charging = false;

        if (type != ActorType.Cavalry)
            return;

        List<Cell> path = pathArrows.GetPath();
        if(path.Count > 1)
        {
            List<Cell> lastTwo = path.GetLast(2);

            if(pathArrows.GetPathStep(lastTwo[0]).dir == pathArrows.GetPathStep(lastTwo[1]).dir)
            {
                foreach (Cell c in path)
                    pathArrows.SetColor(c, Color.white);

                charging = true;
            }
            else
            {
                foreach (Cell c in path)
                    pathArrows.SetColor(c, Color.black);
            }
        }
    }

    public bool TakeDamage(int damage)
    {
        healthBar.AdjustHp(-damage);

        return !healthBar.IsDead();
    }

    internal int GetCurrentMovespeed()
    {
        return GetMovespeed(type);
    }

    public int GetCurrentRange()
    {
        int range = GetRange(type);

        if (type == ActorType.Hunter && !hasSpear)
            range -= 1;

        return range;
    }

    public int GetCurrentDamage()
    {
        int totalDamage = GetDamage(type);

        if (type == ActorType.Cavalry && charging)
            totalDamage += 1;

        return totalDamage;
    }

    public void ResetPath()
    {
        pathArrows.ResetPath();
    }

    Sequence moveSequence;
    public override bool HandleMoveAnimation(Cell Cell)
    {
        moveAnimationPaused = true;

        if (moveSequence != null)
            moveSequence.Kill();

        moveSequence = DOTween.Sequence();

        float totalTime = 0.3f;
        float stepTime = totalTime / pathArrows.GetPath().Count;

        foreach (Cell cell in pathArrows.GetPath())
        {
            if (cell != pathArrows.GetPath().Last())
                moveSequence.Append(this.transform.DOMove(cell.transform.position, stepTime));
        }

        moveSequence.AppendCallback(UnpauseMoveAnimation);
        moveSequence.AppendCallback(cell.actorDisplay.AnimationUnpaused);
        return false;
    }

    public override void HandleActorKilled()
    {
        base.HandleActorKilled();

        if (this.IsEnemy())
            ResourceManager.Instance.AdjustValue(ResourceType.EnemySpawnPower, healthBar.MaxHp());
    }
}
