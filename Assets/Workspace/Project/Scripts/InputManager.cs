﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputState
{
    None = 0,
    Playing = 1,
    EnemyTurn = 2,
    CastingCard = 3,
    Moving = 4,
    Attacking = 5,

    Ending = 10,

    ChoosingSpace = 30,
}

public class InputManager : InputManagerBase, BrainGoodInputManager
{
    public override void Init()
    {
        base.Init();
        TestSetup();
    }

    void TestSetup()
    {
        DiceManager.Instance.DrawUpTo(4);
    }

    public override void HandleCellClicked(Cell cell)
    {
        base.HandleCellClicked(cell);
        switch(inputState)
        {
            case InputState.Playing:
                TrySelectUnit(cell);
                break;
            case InputState.Moving:
                if(Board.Instance.cellHighlightCells.Contains(cell))
                {
                    selectedActor.Move(cell.point);
                    GoToState(InputState.Attacking);
                    selectedActor.ShowAttacks();
                }
                else
                {
                    TrySelectUnit(cell);
                }
                break;
            case InputState.Attacking:
                Actor attacker = selectedActor;
                if (Board.Instance.cellHighlightCells.Contains(cell))
                {
                    selectedActor.SetReady(false);

                    if (cell.isOccupied)
                    {
                        if(cell.HasEnemy())
                        {
                            int damage = attacker.GetCurrentDamage();
                            if (attacker.type == ActorType.Hunter && cell.point.DistanceFrom(attacker.cell.point) > 1)
                            {
                                attacker.SetSpear(false);
                                Helpers.ShowCellReminder(cell, "Spear thrown!");
                                damage += 1;
                            }
                            cell.Attack(attacker, damage);
                        }

                        if (attacker.type == ActorType.Armored)
                            cell.Push(cell.point-attacker.cell.point);
                    }

                    UnselectActor();
                    GoToBaseState();
                    PlayerTurnDone();
                }
                break;
            case InputState.ChoosingSpace:
                if (Board.Instance.cellHighlightCells.Contains(cell))
                {
                    if (DiceManager.Instance.dragDie != null)
                    {
                        Die toActivateOnSpace = DiceManager.Instance.dragDie;
                        switch (toActivateOnSpace.diceType)
                        {
                            case DiceType.Treasure:
                                DiceManager.Instance.SpendDie(toActivateOnSpace);
                                cell.SpawnItems(ItemType.Pickup, toActivateOnSpace.DieValue(), false);
                                GoToBaseState();
                                break;
                        }
                    }
                }
                break;
        }
    }

    void TrySelectUnit(Cell c)
    {
        if (c.HasPlayerUnit() && c.GetRandomActor().isReady)
        {
            GoToState(InputState.Moving);
            SelectActor(c.GetFirstActor());
            c.GetFirstActor().ShowMoves();
        }
        else
        {
            GoToBaseState();
        }
    }

    public override void GoToState(InputState state)
    {
        base.GoToState(state);

        inputState = state;

        Board.Instance.UpdateCellsFromStateChange();
        Board.Instance.ResetActorPaths();

        Game.Instance.CheckGameOver();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void HandleDieClicked(Die die)
    {
        base.HandleDieClicked(die);
        die.Roll();
    }

    public override void DebugInput()
    {
        base.DebugInput();

        if (Input.GetKeyUp(KeyCode.D))
            CardManager.Instance.DrawCard();

        if (Input.GetKeyUp(KeyCode.S))
            Helpers.RotateGameSpeed();

        if (Input.GetKeyUp(KeyCode.N))
            Game.Instance.NewGame();

        if (Input.GetKeyUp(KeyCode.E))
            Game.Instance.StartEnemyTurn();

        if(Input.GetKeyUp(KeyCode.R))
            RefreshAllies();

        if (Input.GetKeyUp(KeyCode.M))
        {
            ResourceManager.Instance.GainFromWorld(ResourceType.EnemySpawnPower, 2, Board.Instance.GetCell(0,0).transform.position);
        }

        if(Input.GetKeyUp(KeyCode.P))
        {
            Debug.Log(ResourceManager.Instance.GetValue(ResourceType.EnemySpawnPower));
        }
    }

    public override void StartTurn()
    {
        RefreshAllies();
        GoToBaseState();
    }

    void RefreshAllies()
    {
        foreach (Actor a in Board.Instance.GetPlayerUnits())
            a.SetReady(true);
    }

    void PlayerTurnDone()
    {
        if(Board.Instance.NumReadyUnits() < 1)
        {
            Game.Instance.StartEnemyTurn();
        }
    }

    public override void HoverCell(Cell cell)
    {
        base.HoverCell(cell);
        switch(inputState)
        {
            case InputState.Moving:
                Actor mover = selectedActor;
                if (cell == mover.cell)
                {
                    mover.pathArrows.ResetPath();
                }
                else if(Board.Instance.cellHighlightCells.Contains(cell))
                {
                    mover.pathArrows.TryAddToPath(cell, false);

                    if (!mover.pathArrows.Reaches(cell))
                        mover.pathArrows.SetPath(Board.Instance.ShortestPath(mover.cell.point, cell.point, mover, true));

                    if (Board.Instance.CalculatePathMoveCost(mover.pathArrows.GetPath(), mover) > mover.GetCurrentMovespeed())
                        mover.pathArrows.SetPath(Board.Instance.ShortestPath(mover.cell.point, cell.point, mover, true));

                    if (Board.Instance.CalculatePathMoveCost(mover.pathArrows.GetPath(), mover) > mover.GetCurrentMovespeed())
                        mover.pathArrows.ResetPath();

                    mover.CheckForCharge();
                }
                break;
        }
    }

    public override void DelayHoverCell(Cell cell)
    {

    }

    public override bool CanPlayCards()
    {
        return true;
    }

    public override bool CanUndoCards()
    {
        return inputState == InputState.CastingCard;
    }
}
