﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public class CardID
{
    public string descText = "";
    public CardType cardType = CardType.None;

    public int cardId = 0;
    public static int ID_GEN = 0;

    public CardID(CardType type)
    {
        descText = "default title";
        this.cardType = type;
        ID_GEN++;
        cardId = ID_GEN;
    }

    public string GetCardString()
    {
        return cardType.ToString();
    }
}
