﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : CellBase
{
    public Player controllingPlayer = Player.None;

    internal void ChildInit()
    {
        SetTerrainNone();
        SetControllingPlayer(Player.None);
    }

    public void ChangeStateUpdate()
    {
        UpdateControllingPlayer();
        SetDebugText("");
    }

    public void UpdateControllingPlayer()
    {
        if (terrainType != TerrainType.Stronghold)
            return;

        if (HasPlayerUnit())
            SetControllingPlayer(Player.You);

        if (HasEnemy())
            SetControllingPlayer(Player.Opponent);
    }

    void SetControllingPlayer(Player player)
    {
        controllingPlayer = player;
        terrainSprite.color = Constants.GetPlayerColor(player);
    }
}
