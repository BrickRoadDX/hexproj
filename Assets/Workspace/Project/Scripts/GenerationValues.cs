﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationValues : MonoBehaviour
{

    public static int NumToSpawn(ActorType type)
    {
        switch(type)
        {
            case ActorType.Minion:
                return 5;
            case ActorType.Bruiser:
                return 2;
        }

        return 1;
    }
}
