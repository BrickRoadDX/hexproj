﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ItemBase
{
    [HideInInspector]
    public ItemClass itemClass = ItemClass.None;

    public override void Init(ItemType iType)
    {
        base.Init(iType);

        if (iType == ItemType.Cube)
            SetItemClass(Helpers.GetRandomEnumType<ItemClass>());
    }

    public void SetItemClass(ItemClass itemClass)
    {
        this.itemClass = itemClass;
        spriteRenderer.color = ItemDef.GetItemClassColor(itemClass);
    }
}
