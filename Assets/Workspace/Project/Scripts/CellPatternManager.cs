﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CellPatternManager : CellPatternManagerBase
{
    public override bool SpaceIsDroppable(Cell cell)
    {
        if (cell.isOccupied || cell.HasCpo())
            return false;

        return true;
    }
}
