﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDrawer : MonoBehaviour
{
    public tk2dUIItem uiItem;
    public Vector3 slideLocalPos;

    bool extended = false;
    Vector3 startLocalPos = Vector3.zero;

    public Transform arrowTransform;

    private Tweener tweener;

    private void Start()
    {
        startLocalPos = this.transform.localPosition;
    }

    private void OnEnable()
    {
        uiItem.OnClick += Toggle;
    }

    public void Toggle()
    {
        extended = !extended;

        Vector3 targetPos = extended ? slideLocalPos : startLocalPos;

        if (tweener != null)
            tweener.Goto(5f);

        tweener = transform.DOLocalMove(targetPos, 0.3f);

        arrowTransform.localRotation = extended ? Quaternion.Euler(new Vector3(0f, 0f, 180f)) : Quaternion.identity;
    }
}
