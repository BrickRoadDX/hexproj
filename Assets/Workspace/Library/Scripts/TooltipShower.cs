﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum TooltipType
{
    None = 0,
}

public class TooltipShower : MonoBehaviour
{
	//Statics
	public static TooltipObject currentTooltip = null;
	public static int currentShownId = -1;
	public static float currentHoverDuration = 0f;
	public static int nextShowerId = 0;
	//End Statics


	//Settings
	public tk2dUIItem hoverUiItem;
	public float hoverDelay = 0.3f;

	private int mThisTooltipId = -1;

	public string textToShow = "";

    public TooltipType tooltipType = TooltipType.None;
    public string prefabName = "TooltipObject";

    [HideInInspector]
    public Camera tk2dCamera;

	void OnEnable()
	{
		mThisTooltipId = nextShowerId;
		nextShowerId++;

        if (tk2dCamera == null)
            tk2dCamera = FindObjectOfType<tk2dCamera>().GetComponent<Camera>();

        if(hoverUiItem != null)
        {
            hoverUiItem.isHoverEnabled = true;
		    hoverUiItem.OnHoverOver += HandleHoverOver;
		    hoverUiItem.OnHoverOut += HandleHoverOut;
        }
    }

	void OnDisable()
	{
        if(hoverUiItem != null)
        {
		    hoverUiItem.OnHoverOver -= HandleHoverOver;
		    hoverUiItem.OnHoverOver += HandleHoverOut;
        }
	}

	void LateUpdate()
	{
		if(mThisTooltipId == currentShownId)
		{
			currentHoverDuration += Time.deltaTime;

			if(currentHoverDuration > hoverDelay)
			{
				ShowTooltip();
			}
		}
	}

	void HandleHoverOver()
	{
		DestroyCurrentTooltip();
		currentShownId = mThisTooltipId;
	}

	public virtual TooltipObject ShowTooltip(Vector3 worldPos = default(Vector3))
	{
        if (Helpers.IsGameScene())
            tk2dCamera = Game.Instance.tk2dCamera;

        if (currentTooltip == null)
		{
            if(textToShow.Length > 0)
            {
                currentTooltip = Helpers.CreateInstance<TooltipObject>(prefabName, this.transform);

                if(worldPos != default(Vector3))
                {
                    UpdatePositionFromWorldPoint(worldPos);
                }
                else
                {
                    UpdatePositionFromMouse();
                }

                currentShownId = mThisTooltipId;

                currentTooltip.tooltipText.text = textToShow;
            }
        }

        return currentTooltip;
	}

    void UpdatePositionFromWorldPoint(Vector3 worldPos)
    {
        currentTooltip.SetDefaultPosition(Helpers.TranslateFromMainWorldToTk2dWorld(worldPos));
        NudgeBasedOnScreenSide();
    }

    void UpdatePositionFromMouse()
    {
        currentTooltip.SetDefaultPosition(tk2dCamera.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0f, 0f, 0f));
        NudgeBasedOnScreenSide();
    }

    void NudgeBasedOnScreenSide()
    {
        Vector3 viewportPoint = tk2dCamera.WorldToScreenPoint(currentTooltip.transform.position);
        currentTooltip.onLeftSide = true;

        if (tk2dCamera.ScreenToViewportPoint(viewportPoint).x < 0.5f)
        {
            float posX = currentTooltip.transform.position.x + currentTooltip.width * 1.5f + currentTooltip.xOffset;
            currentTooltip.SetDefaultPosition(new Vector3(posX, currentTooltip.transform.position.y, currentTooltip.transform.position.z));
        }
    }

    void HandleHoverOut()
	{
		if(currentShownId == mThisTooltipId)
		{
			DestroyCurrentTooltip();
		}
	}

	public static void DestroyCurrentTooltip()
	{
		if (currentTooltip != null)
		{
			Destroy(currentTooltip.gameObject);
		}

		currentHoverDuration = 0f;
		currentTooltip = null;
		currentShownId = -1;
	}

    public bool IsShowingTooltip()
    {
        return currentShownId == mThisTooltipId;
    }
}
