﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBase : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Rotater rotater;

    [HideInInspector]
    public ItemType itemType = ItemType.None;

    [HideInInspector]
    public Cell cell;

    [HideInInspector]
    public int itemId = 0;

    public static int nextItemId = 1;

    public virtual void Init(ItemType iType)
    {
        itemType = iType;

        if (!(this is Item))
            Debug.LogError("Whoa, why is this the base class ItemBase");

        this.spriteRenderer.transform.localScale = ItemDef.GetItemScale(iType);

        itemId = nextItemId;
        nextItemId++;
        gameObject.name = itemType.ToString() + " " + itemId.ToString();

        spriteRenderer.sprite = Helpers.GetSprite(ItemDef.GetSpriteName(iType));
        spriteRenderer.color = ItemDef.GetItemColor(iType);
        rotater.enabled = ItemDef.IsItemSpinning(iType);
    }

    internal static Item CreateItem(ItemType type)
    {
        Item item = Helpers.CreateInstance<Item>("Item", null);
        item.Init(type);

        return item;
    }
}
