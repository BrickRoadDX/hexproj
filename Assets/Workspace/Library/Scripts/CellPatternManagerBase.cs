﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CellPatternManagerBase : Singleton<CellPatternManagerBase>
{
    [HideInInspector]
    public ScrollwheelHandler scrollwheelHandler;

    [HideInInspector]
    public List<CellPattern> cellPatternObjects = new List<CellPattern>();

    public ObjectDisplay cellPatternStore;

    public override void Awake()
    {
        if (!(this is CellPatternManager))
            Debug.LogError("Whoa, why is this the base class CellPatternManagerBase");

        base.Awake();
        scrollwheelHandler = Helpers.MakeScrollWheelHandler(this.transform, RightScroll, LeftScroll);
    }

    public virtual void Init()
    {
        cellPatternObjects.ClearAndDestroyList();
        cellPatternStore.ClearAndDestroyAll();
    }

    public virtual void CreateCPO(CellPatternType type = CellPatternType.None)
    {
        if (type == CellPatternType.None)
            type = CellPatternDef.GetNextCellPatternType();


        CellPattern cpo = CellPatternDef.GetCellPattern(type);

        if (cellPatternStore == null)
            cpo.transform.position = Helpers.CirclePoint(Helpers.RandomFloatFromRangeInclusive(0f, 100f), 10f, cpo.transform.position);
        else
            cellPatternStore.AddObject(cpo);

        cellPatternObjects.Add(cpo);
    }

    void SetPlacedColliders(bool active)
    {
        foreach(CellPattern cpo in cellPatternObjects)
        {
            if (cpo.IsPlaced())
                cpo.SetColliders(active);
        }
    }


    protected Vector3 lastMousePos = Vector3.zero;
    CellPatternSpace currentlyHoveredCps;
    protected CellPattern dragCpo;
    protected CellPatternSpace dragCps;
    void Update()
    {
        SetPlacedColliders(false); //we wanna raycast for an UNPLACED one first
        currentlyHoveredCps = Helpers.RayCastForObjectType<CellPatternSpace>(Game.Instance.mainCamera);

        if(currentlyHoveredCps == null) //try again with placed ones
        {
            SetPlacedColliders(true);
            currentlyHoveredCps = Helpers.RayCastForObjectType<CellPatternSpace>(Game.Instance.mainCamera);
        }

        if (currentlyHoveredCps != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragCps = currentlyHoveredCps;
                dragCpo = currentlyHoveredCps.cellPatternObject;
                PickUp();
            }
        }

        if (Input.GetMouseButton(0)) //continue drag
        {
            if (dragCpo != null)
                Drag();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (dragCpo != null)
                Drop(dragCpo);
        }
    }

    public virtual void PickUp()
    {
        if(cellPatternStore != null)
            cellPatternStore.RemoveObject(dragCpo);

        lastMousePos = Game.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Board.Instance.PickupCellPattern(dragCpo);
    }

    public virtual void Drag()
    {
        Vector3 mouseMove = Game.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition) - lastMousePos;
        dragCpo.transform.position += mouseMove;
        lastMousePos = Game.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Board.Instance.HighlightCpoBoardSpaces(dragCpo);
        dragCpo.SetPlaced(false);
    }

    public virtual void Drop(CellPattern cpo)
    {
        bool canDrop = CanDrop();
        dragCpo.SetPlaced(canDrop);
        if (canDrop)
            Board.Instance.DropCellPattern(dragCpo);

        dragCps = null;
        dragCpo = null;
    }

    public virtual void LeftScroll()
    {
        if (dragCpo != null)
            dragCpo.Rotate(false, dragCps);
    }

    public virtual void RightScroll()
    {
        if (dragCpo != null)
            dragCpo.Rotate(true, dragCps);
    }

    internal void HandleDestroyed(CellPattern cellPatternObject)
    {
        if(cellPatternStore.Contains(cellPatternObject))
            cellPatternStore.RemoveObject(cellPatternObject);

        if(cellPatternObjects.Contains(cellPatternObject))
            cellPatternObjects.Remove(cellPatternObject);

        if (Board.Instance.cellPatternObjectsOnBoard.Contains(cellPatternObject))
            Board.Instance.cellPatternObjectsOnBoard.Remove(cellPatternObject);

        if (dragCpo == cellPatternObject)
            dragCpo = null;
    }

    public virtual bool CanDrop()
    {
        bool canDrop = true;
        if (dragCpo.GetBoardSpaces().Any(x => !x.SpaceCanHoldCPO())) //can't place
            canDrop = false;

        if (!dragCpo.IsOverAtleastOneBoardSpace())
            canDrop = false;

        return canDrop;
    }

    public void DrawUpTo(int count)
    {
        int currentCount = NumUnplaced();
        for (int i = 0; i < count - currentCount; i++)
            CreateCPO();
    }

    public int NumPlaced()
    {
        int count = 0;
        foreach (CellPattern cp in cellPatternObjects)
        {
            if (cp.IsPlaced())
                count++;
        }
        return count;
    }

    public int NumUnplaced()
    {
        int count = 0;
        foreach (CellPattern cp in cellPatternObjects)
        {
            if (!cp.IsPlaced())
                count++;
        }
        return count;
    }

    public virtual bool SpaceIsDroppable(Cell Cell)
    {
        return !Cell.HasCpo();
    }
}
