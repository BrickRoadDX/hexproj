﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceManager : Singleton<ResourceManager>
{
    public Dictionary<ResourceType, int> resourceValues = new Dictionary<ResourceType, int>();

    public ObjectDisplay objectDisplay;

    public Dictionary<ResourceType, ResourceDisplay> resourceDisplays = new Dictionary<ResourceType, ResourceDisplay>();

    public void Init()
    {
        if(objectDisplay == null)
        {
            Debug.LogError("No objectDipslay set in resource manager");
            return;
        }

        List<ResourceType> resources = Helpers.GetAllEnumTypes<ResourceType>(true);
        if(resourceValues.Count < 1) //only if we have not initialized
        {
            foreach (ResourceDisplay display in FindObjectsOfType<ResourceDisplay>())
                resourceDisplays.Add(display.resourceType, display);

            resourceValues.Clear();
            foreach (ResourceType type in resources)
            {
                if (!ResourceDef.ShouldBeDisplayed(type))
                    continue;

                if(!resourceDisplays.ContainsKey(type)) //make a generic text one
                {
                    resourceValues.Add(type, 0);
                    ResourceDisplay display = Helpers.CreateInstance<ResourceDisplay>("ResourceDisplay", this.transform, true);
                    display.Init(type);
                    resourceDisplays.Add(type, display);
                    objectDisplay.AddObject(display);
                }

                gainedResourceObjects.Add(type, new List<GainedResourceObject>());
                gainedResouceCounts.Add(type, new List<int>());
            }
        }

        foreach(ResourceType type in resources)
        {
            SetValue(type, 0);
        }
    }

    public bool CanAfford(Cost cost)
    {
        foreach(ResourceType type in cost.values.Keys)
        {
            if (GetValue(type) < cost.Get(type))
                return false;
        }
        return true;
    }

    public int GetValue(ResourceType type)
    {
        int total = 0;

        foreach (int i in gainedResouceCounts[type])
            total += i;

        return resourceValues[type] + total;
    }

    public void GainFromWorld(ResourceType type, int amount, Vector3 worldPos)
    {
        Vector3 worldUIpos = Game.Instance.mainCamera.WorldToScreenPoint(worldPos);
        Vector3 tk2dWorldUIPos = Game.Instance.tk2dCamera.ScreenToWorldPoint(worldUIpos);
        Gain(type, amount, false, tk2dWorldUIPos);
    }

    public void Gain(ResourceType type, int amount)
    {
        Gain(type, amount, false);
    }

    public void GainInstant(ResourceType type, int amount)
    {
        Gain(type, amount, true);
    }

    public Dictionary<ResourceType, List<GainedResourceObject>> gainedResourceObjects = new Dictionary<ResourceType, List<GainedResourceObject>>();
    public Dictionary<ResourceType, List<int>> gainedResouceCounts = new Dictionary<ResourceType, List<int>>();
    public void Gain(ResourceType type, int amount, bool instant = true, Vector3 uiOrigin = default(Vector3))
    {
        if(instant)
        {
            AdjustValue(type, amount, true);
            return;
        }

        if (amount > 0)
        {
            GainedResourceObject gro = Helpers.CreateInstance<GainedResourceObject>("GainedResourceObject");

            if (uiOrigin != default(Vector3))
                gro.transform.position = uiOrigin;
            else
                gro.transform.position = Game.Instance.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);

            gainedResouceCounts[type].Add(amount);
            gro.resourceType = type;
            gro.transform.DOMove(resourceDisplays[type].gainTarget.position, 2f).SetEase(Ease.OutQuint).Play();

            gro.temporary.mLifespan = 2f;

            gro.sprite.color = ResourceDef.GetResourceColor(type);
            gro.sprite.sprite = ResourceDef.GetResourceSprite(type);

            gainedResourceObjects[type].Add(gro);
        }
    }

    // only used for delayed resource objects from gains
    public void ProcessNextResource(ResourceType type)
    {
        SoundManager.Instance.PlaySound("pickup");

        if (gainedResouceCounts[type].Count < 1)
            return;

        if (gainedResouceCounts[type][0] > 1)
            Helpers.ShowReminderText("+" + gainedResouceCounts[type][0].ToString(), 1, resourceDisplays[type].gainTarget.position, ResourceDef.GetResourceColor(type), true, false);

        AdjustValue(type, gainedResouceCounts[type][0], false);
        gainedResouceCounts[type].RemoveAt(0);
        gainedResourceObjects[type].RemoveAt(0);
    }

    public void AdjustValue(ResourceType type, int amount)
    {
        //generally speaking we DO want to keep in mind "incoming" resources when we adjust
        //because this is indicating we are "keeping a balance"
        //similar logic to "spend"
        AdjustValue(type, amount, true);
    }

    private void AdjustValue(ResourceType type, int amount, bool processIncoming = true)
    {
        SetValueInternal(type, resourceValues[type] + amount, processIncoming);
    }

    public void Pay(Cost cost)
    {
        foreach(ResourceType type in cost.values.Keys)
        {
            if (cost.Get(type) > 0)
                Spend(type, cost.Get(type));
        }
    }

    //spend a # of resources. will process "incoming resources" instantly
    public void Spend(ResourceType type, int amount)
    {
        AdjustValue(type, -amount, true);
    }

    public void SetValue(ResourceType type, int value)
    {
        if (!ResourceDef.ShouldBeDisplayed(type))
            return;
        //generally speaking we want to override ALL earlier gains and losses when we set the value like this
        //so process incoming = false
        //BUT we have to clear the queue
        CovertIncomingIntoValuesNow(type);
        SetValueInternal(type, value, false);
    }

    private void SetValueInternal(ResourceType type, int value, bool processIncoming = true)
    {
        if (!ResourceDef.ShouldBeDisplayed(type))
            return;

        // if we have "incoming" resources, need to process them first
        //before we spend
        //so we don't end up in negatives or something
        int incoming = 0;
        if (processIncoming)
            incoming = CovertIncomingIntoValuesNow(type);

        resourceValues[type] = value + incoming;
        if(resourceDisplays.ContainsKey(type))
        {
            resourceDisplays[type].UpdateText(resourceValues[type].ToString());
        }
        else
        {
            Debug.LogError(string.Format("No resource display for type {0}", type));
        }
    }

    int CovertIncomingIntoValuesNow(ResourceType type)
    {
        int value = 0;
        Helpers.StartWhileLoopCounter();
        while(gainedResouceCounts[type].Count > 0 && Helpers.WhileLoopTick())
        {
            value += gainedResouceCounts[type][0];
            gainedResouceCounts[type].RemoveAt(0);
            gainedResourceObjects[type][0].resourceType = ResourceType.None; //prevents later processing
            gainedResourceObjects[type].RemoveAt(0);
        }
        return value;
    }
}
