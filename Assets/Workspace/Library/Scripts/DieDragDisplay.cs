﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DieDragDisplay : DragDisplay
{
    public TextMeshPro pipCountText;

    public override void Init()
    {
        base.Init();
        pipCountText.text = "";
    }

    public override bool DraggingDroppableObject()
    {
        return DiceManager.Instance.dragDie != null;
    }

    public override void Drop(Draggable toDrop)
    {
        base.Drop(toDrop);

        int pipCount = 0;
        foreach (Die d in GetCastedList<Die>())
            pipCount += d.DieValue();

        pipCountText.text = "Total Die Value: " + pipCount.ToString();
    }
}
