﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;

public enum DiceAction
{
    None = 0,
    Reroll = 1,
}

public class DiceManagerBase : Singleton<DiceManagerBase> {

    public bool gameHasDice = false;

    public ObjectDisplay diceBox;
    public ObjectDisplay refreshBox;

    [HideInInspector]
    public Die lastActionDie;

    [HideInInspector]
    public float soundDelay = 0f;

    [HideInInspector]
    public float clatterSoundDelay = 0f;

    [HideInInspector]
    internal Die currentAbilityDie;

    [HideInInspector]
    public Die lastDiscardedDie;

    [HideInInspector]
    public Die lastChangedDie;

    [HideInInspector]
    float pressTime = 0f;

    [HideInInspector]
    public bool tabletMode = false;

    [HideInInspector]
    internal DiceAction currentDiceAction = DiceAction.None;

    private void Start()
    {
        if (!(this is DiceManager))
            Debug.LogError("Whoa, why is this the base class DiceManagerBase");
    }

    public void Init()
    {
        diceBox.ClearAndDestroyAll();
        refreshBox.ClearAndDestroyAll();
    }

    public int SelectedDiceCount()
    {
        return SelectedDice().Count;
    }

    public void DrawUpTo(int numDice, bool fast = false)
    {
        if (diceBox == null)
            Debug.LogError("Dice box has not been initialized");

        Helpers.StartWhileLoopCounter();
        while (diceBox.Count < numDice && Helpers.WhileLoopTick())
            RollNewDie();
    }

    public List<Die> SelectedDice()
    {
        List<Die> selectedList = new List<Die>();
        foreach (Die d in diceBox.GetCastedList<Die>())
        {
            if (d.selected)
                selectedList.Add(d);
            
        }
        return selectedList;
    }

    public List<Die> GetDiceBoxDice()
    {
        return diceBox.GetCastedList<Die>();
    }

    public List<DieFace> AllFaces()
    {
        List<DieFace> selectedList = new List<DieFace>();
        foreach (Die d in GetDiceBoxDice())
        {
            if (!selectedList.Contains(d.dieFaceType)) 
                selectedList.Add(d.dieFaceType);
        }
        return selectedList;
    }

    public List<DieFace> SelectedFaces()
    {
        List<DieFace> selectedList = new List<DieFace>();
        foreach(Die d in GetDiceBoxDice())
        {
            if (d.selected && !selectedList.Contains(d.dieFaceType))
                selectedList.Add(d.dieFaceType);
        }
        return selectedList;
    }

    public void DeselectDice()
    {
        foreach (Die die in GetDiceBoxDice())
            die.SetSelected(false);
    }

    public void SelectAllDice(bool excludeAbility = false)
    {
        foreach (Die die in GetDiceBoxDice())
        {
            if(currentAbilityDie != die)
                die.SetSelected(true);
        }
    }

    public Die CreateDie(DiceType type)
    {
        Die die = Helpers.CreateInstance<Die>("Die", this.transform);
        die.Init(type);

        return die;
    }

    void RollDice()
    {
        foreach (Die die in GetDiceBoxDice())
            die.Roll();
    }

    internal void MoveToDiceBox(Die die)
    {
        die.SetSelected(false);

        refreshBox.RemoveObject(die);
        diceBox.AddObject(die);

        die.hoverZoom.Init(1f);
        die.hoverZoom.SetHoverEventsEnabled(true);
    }

    internal void SpendDie(Die die)
    {
        die.SetSelected(false);

        diceBox.RemoveObject(die);
        refreshBox.AddObject(die);

        die.hoverZoom.Init(0.5f);
        die.hoverZoom.SetHoverEventsEnabled(false);
    }

    public void DestroyAndRemoveDie(Die d)
    {
        if (diceBox.Contains(d))
            diceBox.RemoveObject(d);

        if (refreshBox.Contains(d))
            refreshBox.RemoveObject(d);

        d.gameObject.DestroySelf();
    }

    bool AnyCastingDie()
    {
        return SelectedDiceCount() > 0;
    }

    bool IsSelected(Die d)
    {
        return d.selected;
    }

    [HideInInspector]
    public Die hoveredDie;
    Collider raycastCollider = null;
    RollingTransform hitRollingTransform = null;
    Die newHoveredDie;
    float middleXpos = 0f;
    void LateUpdate()
    {
        if (!gameHasDice)
            return;

        if (soundDelay > 0f)
            soundDelay -= Time.deltaTime;

        if (!Game.HasPrivateInstance() || Game.Instance.IsGameOver())
            return;

        hitRollingTransform = Helpers.RayCastForObjectType<RollingTransform>(Helpers.tk2dCamera);

        if (hitRollingTransform != null)
        {
            newHoveredDie = hitRollingTransform.die;

            if (!newHoveredDie.IsPlayersDie())
                return;

            if (newHoveredDie != null && hoveredDie != null && newHoveredDie != hoveredDie)
            {
                HoverOut(hoveredDie);
            }

            if (hoveredDie != newHoveredDie && newHoveredDie != null)
                NewHoveredDie(newHoveredDie);

            hoveredDie = newHoveredDie;
        }
        else if (hoveredDie != null)
        {
            HoverOut(hoveredDie);
        }

        HandleDrag();

        if (tabletMode)
        {
            if (!Input.GetMouseButton(0) && hoveredDie != null)
            {
                middleXpos = hoveredDie.transform.localPosition.x;
                pressTime = 0f;
            }

            if (Input.GetMouseButton(0) && hoveredDie != null)
            {
                pressTime += Time.deltaTime;
                hoveredDie.transform.SetLocalPositionX(middleXpos + (pressTime * Helpers.RandomFloatFromRangeInclusive(-15f, 15f)));

                if (pressTime >= 1f)
                {
                    pressTime = 0f;
                    InputManager.Instance.HandleDieRightClicked(hoveredDie);
                }
            }
        }
    }

    public virtual bool TryRightClick()
    {
        if (InputManager.Instance.inputState != InputState.Playing)
            return false;

        if (hoveredDie != null && diceBox.Contains(hoveredDie))
        {
            pressTime = 0f;
            InputManager.Instance.HandleDieRightClicked(hoveredDie);
            return true;
        }
        return false;
    }

    public virtual bool TryUndo()
    {
        if(SelectedDiceCount() > 0)
        {
            DeselectDice();
            return true;
        }

        return false;
    }

    internal int NumSelected()
    {
        return SelectedDiceCount();
    }

    public Die GetFirstSelected()
    {
        if (NumSelected() > 0)
            return SelectedDice()[0];

        return null;
    }

    [HideInInspector]
    public Die dragDie;
    private Vector3 lastMousePos = Vector3.zero;
    Vector3 startMousePos = Vector3.zero;
    void HandleDrag()
    {
        if (hoveredDie != null && diceBox.Contains(hoveredDie))
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragDie = hoveredDie;
                PickUp();
            }
        }

        if (Input.GetMouseButton(0)) //continue drag
        {
            if (dragDie != null)
                Drag();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (dragDie != null)
                Drop(dragDie);
        }
    }

    public virtual void PickUp()
    {
        lastMousePos = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);
        startMousePos = lastMousePos;
        readyToDragPlay = false;
    }

    bool readyToDragPlay = false;
    public virtual void Drag()
    {
        Vector3 mouseMove = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition) - lastMousePos;
        dragDie.transform.position += mouseMove;
        lastMousePos = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);

        if (DieDef.CanPlayOnBoard(dragDie.diceType) && !readyToDragPlay && Vector3.Distance(lastMousePos, startMousePos) > 2f)
        {
            readyToDragPlay = true;

            if (diceBox.Contains(dragDie))
                diceBox.RemoveObject(dragDie);

            if (DieDef.CanPlayOnBoard(dragDie.diceType))
                Board.Instance.ShowDicePlacements(dragDie);
        }

        if (!readyToDragPlay)
            return;

        if (!Board.Instance.gameHasBoard)
            return;

        Board.Instance.RemoveCellVisuals();
        if (dragDie.GetHoveredPoint().HasCell())
            Board.Instance.CreateCellVisual(dragDie.GetHoveredPoint().ToCell());
    }

    public virtual void Drop(Die toDrop)
    {
        if (DragDisplay.ReadyToDrop())
        {
            RemoveDieFromDiceBox(toDrop);
            DragDisplay.hoveredDisplay.Drop(toDrop);
            dragDie = null;
            return;
        }

        bool playingOnBoard = true;
        Cell toDropCell = null;
            
        if(Board.Instance.gameHasBoard)
            toDropCell = toDrop.GetHoveredPoint().ToCell();

        if (!DieDef.CanPlayOnBoard(dragDie.diceType))
            playingOnBoard = false;

        if (toDropCell == null)
            playingOnBoard = false;

        if (playingOnBoard && !Board.Instance.cellHighlightCells.Contains(toDropCell))
            playingOnBoard = false;

        if (Board.Instance.gameHasBoard && playingOnBoard)
        {
            if (DieDef.MovesToBoard(toDrop.diceType))
            {
                MoveDieToBoard(toDrop.GetHoveredPoint().ToCell(), toDrop);
            }
            else
            {
                InputManager.Instance.GoToState(InputState.ChoosingSpace);
                InputManager.Instance.HandleCellClicked(toDrop.GetHoveredPoint().ToCell());
            }
        }
        else
        {
            returnDie = toDrop;
            this.CallActionDelayed(ReturnDie);
            InputManager.Instance.GoToBaseState();
        }

        dragDie = null;
    }

    Die returnDie;
    void ReturnDie()
    {
        diceBox.UpdateObjectPositions();

        if (returnDie == null)
            return;

        if (!diceBox.Contains(returnDie))
            diceBox.AddObject(returnDie);
    }

    void SelectOnlyThis(Die d)
    {
        DeselectDice();
        d.SetSelected(true);
    }

    void HoverOut(Die die)
    {
        hoveredDie = null;

        if(die.allowUIHover)
            Helpers.AdjustSortingOrders(die.gameObject, -100, die.dontAdjustRenderers);
    }

    void NewHoveredDie(Die die)
    {
        if (soundDelay <= 0f)
            SoundManager.Instance.PlaySound("bip");

        if (die.allowUIHover)
            Helpers.AdjustSortingOrders(die.gameObject, 100, die.dontAdjustRenderers);

        InfoPane.Instance.ShowDie(die.diceType, die.dieFaceType);
    }

    internal void DestroyAndRemoveSelectedDice()
    {
        List<Die> toRemove = SelectedDice();
        foreach (Die d in toRemove)
            DestroyAndRemoveDie(d);
    }

    void ResetHoveredDie()
    {
        hoveredDie = null;
    }

    public void DrawRandomDie()
    {
        RollNewDie();
    }

    public void RollNewDie(DiceType diceType = DiceType.None, DieFace face = DieFace.None)
    {
        //if(diceBox.Count >= 16)
        //{
        //    Helpers.ShowReminderText(Helpers.AddIcons("Dice pool is full!"));
        //    return;
        //}

        if (diceType == DiceType.None)
            diceType = Helpers.GetRandomEnumType<DiceType>();

        Die die = CreateDie(diceType);
        if(face == DieFace.None)
        {
            die.Roll();
        }
        else
        {
            die.rollingAnimation = false;
            die.SetDieFace(face);
        }
        diceBox.AddObject(die);
    }

    internal bool TryUndoDiscard()
    {
        if (lastDiscardedDie != null && hoveredDie == null && SelectedDice().Count == 0)
        {
            MoveToDiceBox(lastDiscardedDie);
            lastDiscardedDie = null;
            DeselectDice();
            return true;
        }
        return false;
    }

    internal bool TryUndoChangeDie()
    {
        if (lastChangedDie != null && hoveredDie == null && SelectedDice().Count == 0)
        {
            lastChangedDie.dieFaceType = lastChangedDie.previousDieFace;
            lastChangedDie.SetSprite(lastChangedDie.previousDieFace);

            lastChangedDie = null;
            DeselectDice();
            return true;
        }

        return false;
    }

    public List<Die> RemoveDuplicateDice(List<Die> list)
    {
        int i = 0;
        while(i < list.Count)
        {
            Die current = list[i];
            list.RemoveAll(x => x != current && x.dieFaceType == current.dieFaceType && x.diceType == current.diceType);
            i++;
        }

        return list;
    }

    public List<Die> GetAvailablePlacementDice()
    {
        List<Die> availableDice = new List<Die>();
        if (DiceManager.Instance.SelectedDiceCount() < 1)
        {
            availableDice = new List<Die>(GetDiceBoxDice());
        }
        else
        {
            availableDice = new List<Die>(SelectedDice());
        }
        return availableDice;
    }

    internal void UpdatePlusMinusButtons()
    {
        foreach (Die d in GetDiceBoxDice())
            d.plusMinusButtonsContainer.gameObject.SetActive(d.selected && SelectedDice().Count == 1);
    }

    public void RegainRefreshBox()
    {
        foreach (Die d in refreshBox.GetCastedList<Die>())
            MoveToDiceBox(d);

        RerollDiceBox();
    }

    void RerollDiceBox()
    {
        foreach (Die d in GetDiceBoxDice())
            d.Roll(false, true);
    }

    internal static List<DieFace> ValidSequences(DieFace face)
    {
        List<DieFace> validSequences = new List<DieFace>();

        int index = DieDef.DiceFaceValue(face);
        int oneMore = index + 1;
        int oneLess = index - 1;

        if (DieDef.DiceFaceValue((DieFace)oneMore) <= 6)
            validSequences.Add((DieFace)oneMore);

        if (DieDef.DiceFaceValue((DieFace)oneLess) > 0)
            validSequences.Add((DieFace)oneLess);

        if (DieDef.DiceFaceValue(face) == 6)
            validSequences.Add(DieFace.One);

        if (DieDef.DiceFaceValue(face) == 1)
            validSequences.Add(DieFace.Six);

        return validSequences;
    }

    internal bool IsPairSelected()
    {
        if (SelectedDiceCount() == 2)
        {
            if (DieDef.DiceFaceValue(SelectedDice()[0].dieFaceType) == DieDef.DiceFaceValue(SelectedDice()[1].dieFaceType))
                return true;
        }
        return false;
    }

    public bool IsStraightSelected(int length)
    {
        List<DieFace> faces = SelectedFaces();
        int numWild = faces.Count(x => x == DieFace.Wild);

        int xInARow = 0;
        for(int i = 1; i <= 6; i++)
        {
            if(SelectedDice().Any(x => DieDef.DiceFaceValue(x.dieFaceType) == i))
            {
                xInARow++;
            }
            else if(numWild > 0)
            {
                numWild--;
                xInARow++;
            }
            else //straight broken
            {
                xInARow = 0;
                numWild = faces.Count(x => x == DieFace.Wild);
            }

            if (xInARow >= length)
                return true;
        }

        return false;
    }

    internal void MoveDieToBoard(Cell cell, Die dieToMove = null)
    {
        RemoveDieFromDiceBox(dieToMove);

        Helpers.SetLayer(dieToMove.gameObject, GameLayer.Default);
        cell.diceDisplay.AddObject(dieToMove);
    }

    public void RemoveDieFromDiceBox(Die dieToRemove)
    {
        if (dieToRemove == null)
            dieToRemove = SelectedDice()[0];

        dieToRemove.allowUIHover = false;

        dieToRemove.SetSelected(false);

        diceBox.RemoveObject(dieToRemove);
        dieToRemove.hoverZoom.Init(DieDef.DieScaleOnBoard());
        InputManager.Instance.GoToBaseState();
        Helpers.SetSortingOrders(dieToRemove.gameObject, 30);
    }
}
