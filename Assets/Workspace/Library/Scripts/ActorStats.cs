﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActorStats : MonoBehaviour
{
    public ObjectDisplay statDisplay;
    public Dictionary<ActorStatType, ActorStatVisual> visuals = new Dictionary<ActorStatType, ActorStatVisual>();

    public void AddStat(ActorStatType type, int startValue)
    {
        ActorStatVisual asv = Helpers.CreateInstance<ActorStatVisual>("ActorStatVisual", this.transform);
        statDisplay.AddObject(asv);
        visuals[type] = asv;
        asv.backgroundSprite.color = ActorDef.GetActorStatColor(type);
        UpdateStat(type, startValue);
    }

    public void AdjustValue(ActorStatType type, int amount)
    {
        int newValue = GetValue(type) + amount;
        newValue = Helpers.AtLeast(newValue, 0);
        UpdateStat(type, newValue);
    }

    public void UpdateStat(ActorStatType type, int value)
    {
        if(visuals.ContainsKey(type))
            visuals[type].UpdateValue(value);
    }

    public int GetValue(ActorStatType type)
    {
        if (!visuals.ContainsKey(type))
            return 0;

        return visuals[type].value;
    }
}