﻿using TMPro;
using UnityEngine;

public class ActorStatVisual : MonoBehaviour
{
    [HideInInspector]
    public int value = 0;
    public TextMeshPro statText;
    public SpriteRenderer backgroundSprite;

    public void UpdateValue(int value)
    {
        this.value = value;
        statText.text = value.ToString();
    }
}
