﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDisplay : ObjectDisplay
{
    public SpriteRenderer dropArea;
    public SpriteRenderer objectInDropAreaHighlight;
    public SpriteRenderer draggingObjectHighlight;

    public static DragDisplay hoveredDisplay;

    bool hovered = false;
    bool dragging = false;

    public static List<DragDisplay> dragDisplays = new List<DragDisplay>();

    public virtual void Awake()
    {
        if(!dragDisplays.Contains(this))
            dragDisplays.Add(this);

        Init();
    }

    public virtual void Init()
    {
        objectInDropAreaHighlight.gameObject.SetActive(false);
        hovered = false;
        dragging = true;
        displayObjects.ClearAndDestroyList();
    }

    private void Update()
    {
        if(ObjectInDropArea())
        {
            if (!hovered)
            {
                hovered = true;         
                HoverOver();
            }
        }
        else
        {
            if (hovered)
            {
                hovered = false;
                HoverOut();
            }
        }

        if(DraggingDroppableObject())
        {
            if(!dragging)
            {
                draggingObjectHighlight.gameObject.SetActive(true);
                dragging = true;
            }
        }
        else
        {
            if (dragging)
            {
                draggingObjectHighlight.gameObject.SetActive(false);
                dragging = false;
            }
        }
    }

    internal static void ResetDisplays()
    {
        foreach (DragDisplay display in dragDisplays)
            display.Init();
    }

    public virtual bool DraggingDroppableObject()
    {
        return false;
    }

    public bool ObjectInDropArea()
    {
        return MouseInHoverArea() && DraggingDroppableObject();
    }

    protected bool MouseInHoverArea()
    {
        return Helpers.IsMouseWithinBounds2d(dropArea.bounds);
    }

    void HoverOver()
    {
        objectInDropAreaHighlight.gameObject.SetActive(true);
        hoveredDisplay = this;
    }

    void HoverOut()
    {
        objectInDropAreaHighlight.gameObject.SetActive(false);
        hoveredDisplay = null;
    }

    public virtual void Drop(Draggable toDrop)
    {
        AddObject(toDrop.GetMonoBehaviour());
        InputManager.Instance.GoToBaseState();
    }

    internal static bool ReadyToDrop()
    {
        if (hoveredDisplay == null)
            return false;

        return hoveredDisplay.ObjectInDropArea();
    }
}

public interface Draggable
{
    MonoBehaviour GetMonoBehaviour();
}
