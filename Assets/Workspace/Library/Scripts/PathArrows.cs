﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//add patharrows to Actor.cs and initialize as new
//call Init(), then you can
//ResetPath, SetColor, TryAddToPath, SetPath, GetPath, Reaches, GetPathStep

public class PathStep
{
    public SpriteRendererGo srgo;
    public Cell cell;
    public FlatHexPoint dir;

    public PathStep(SpriteRendererGo srgo, Cell cell, FlatHexPoint dir)
    {
        this.srgo = srgo;
        this.cell = cell;
        this.dir = dir;
    }
}

public class PathArrows 
{
    Dictionary<Cell, PathStep> pathSteps = new Dictionary<Cell, PathStep>();
    List<Cell> path = new List<Cell>();
    Actor actor;

    Color arrowColor = Color.black;

    public void Init(ActorBase actorBase)
    {
        actor = (Actor)actorBase;
    }

    public void ResetPath()
    {
        RemovePathArrowVisuals();
        path.Clear();
    }

    public List<Cell> GetPath()
    {
        return path;
    }

    public bool Reaches(Cell cell)
    {
        if (path.Count < 1)
            return false;

        if (path.Last() == cell)
            return true;

        return false;
    }

    public void SetColor(Cell c, Color color)
    {
        pathSteps[c].srgo.spriteRenderer.color = color;
    }

    public PathStep GetPathStep(Cell c)
    {
        return pathSteps[c];
    }

    public void TryAddToPath(Cell cell, bool lookForShorterPaths = true)
    {
        if (actor == null)
            Debug.LogError("Forgot to initialize patharrows");

        Cell firstAdjacent = null;

        if(lookForShorterPaths)
        {
            if (actor.cell.GetAdjacent().Contains(cell))
                firstAdjacent = actor.cell;

            for(int x = 0; x < path.Count; x++)
            {
                Cell c = path[x];
                if (firstAdjacent == null)
                {
                    if(c.GetAdjacent().Contains(cell))
                        firstAdjacent = c;
                }
                else
                {
                    path.RemoveAt(x);
                    x--;
                }
            }
        }
        else
        {
            if (path.Count < 1)
            {
                if(actor.cell.GetAdjacent().Contains(cell))
                    firstAdjacent = actor.cell;
            }
            else
            {
                if (path.Last().GetAdjacent().Contains(cell))
                    firstAdjacent = path.Last();
            }
        }

        if (firstAdjacent == null)
            return;

        if (!path.Contains(cell))
        {
            path.Add(cell);
            ShowPathArrows();
        }
    }

    public void SetPath(List<FlatHexPoint> path)
    {
        this.path = path.ToCells();
        ShowPathArrows();
    }

    public bool IsEmpty()
    {
        return path.Count < 1;
    }

    Cell lastPathArrowCell;
    FlatHexPoint lastDir;
    private void ShowPathArrows()
    {
        RemovePathArrowVisuals();

        for (int i = 0; i < path.Count; i++)
        {
            lastPathArrowCell = actor.cell;
            if (i > 0)
                lastPathArrowCell = path[i - 1];

            FlatHexPoint dir = path[i].point - lastPathArrowCell.point;

            CreateStraightArrow(path[i], dir , i);

            lastDir = dir;
        }
    }

    private SpriteRendererGo CreateArrow()
    {
        SpriteRendererGo srgo = Helpers.MakeSpriteRenderer("vertical-arrow", Board.Instance.transform, arrowColor, 20);
        srgo.transform.localScale = Vector3.one * 0.8f;
        return srgo;
    }

    private SpriteRendererGo newSRGO;
    private void CreateStraightArrow(Cell cell, FlatHexPoint dir, int index)
    {
        FlatHexPoint nextPoint = cell.point + dir;

        SpriteRendererGo middleSrgo = CreateArrow();
        middleSrgo.transform.position = Vector3.Lerp(lastPathArrowCell.point.PointPos(), cell.point.PointPos(), 0.5f);
        pathSteps.Add(cell, new PathStep(middleSrgo, cell, dir));
        SetArrowRotation(dir, middleSrgo);
        middleSrgo.gameObject.name = "Path Arrow: " + index;
    }

    private void SetArrowRotation(FlatHexPoint dir, SpriteRendererGo go)
    {
        Vector3 rot = new Vector3(0f, 0f, Helpers.GetAngleFromDirection(GridHelpers.GetDirectionFromFlatHexPoint(dir)));
        go.transform.localRotation = Quaternion.Euler(rot);
    }

    private void RemovePathArrowVisuals()
    {
        foreach (PathStep pathArrow in pathSteps.Values)
        {
            pathArrow.srgo.gameObject.DestroySelf();
        }

        pathSteps.Clear();
    }


}
