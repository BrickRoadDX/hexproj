﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellPatternSpace : MonoBehaviour
{
    public FlatHexPoint position;
    public SpaceType spaceType;
    public FlatHexPoint direction = FlatHexPoint.Zero;
    public Tile tile = null;
    public ActorType actorType = ActorType.None;

    public Collider myCollider;
    public CellPattern cellPatternObject;
    public SpriteRenderer spriteRenderer;

    private List<GameObject> objects = new List<GameObject>();

    public void Init(int x, int y, SpaceType sType, bool makeVisual = true)
    {
        this.position = new FlatHexPoint(x, y);
        this.spaceType = sType;

        TileType tileType = CellPatternDef.GetTileType(sType);
        actorType = CellPatternDef.GetActorType(sType);

        if (!makeVisual)
        {
            spriteRenderer.gameObject.SetActive(false);
            myCollider.enabled = false;
        }

        if(makeVisual)
        {
            spriteRenderer.sprite = Helpers.GetSprite(Helpers.GetSelectVisualSpriteName());
            spriteRenderer.sortingOrder = 60;

            if (tileType != TileType.None)
            {
                tile = Tile.CreateTile(tileType);
                tile.transform.parent = this.transform;
                tile.transform.localPosition = Vector3.zero;
                objects.Add(tile.gameObject);
                Helpers.SetSortingOrders(tile.gameObject, 60);
            }

            if (actorType != ActorType.None)
            {
                Actor a = Actor.CreateActor(actorType);
                a.transform.parent = this.transform;
                a.transform.localPosition = Vector3.zero;
                objects.Add(a.gameObject);
                Helpers.SetSortingOrders(a.gameObject, 60);

            }
        }
    }

    bool ShouldRotate()
    {
        if (tile == null)
            return false; //most things we want to keep facing up

        return TileDef.ShouldRotate(tile.tileType); //some things rotate like carcassone tiles
    }

    internal void HandleRotated(bool right)
    {
        if (ShouldRotate())
        {
            tile.RotateExits(right);
            return;
        }

        foreach (GameObject go in objects)
            go.transform.rotation = Quaternion.identity;
    }

    internal bool HasTile()
    {
        return tile != null;
    }

}