﻿using UnityEngine;
using System.Collections;
using System;

public class Constants
{
	public const string VERSION_NUMBER = "v0.01";

    public static Color bggRed = Helpers.HexToColor("B53031");
    public static Color bggBlue = Helpers.HexToColor("304DB5");
    public static Color bggYellow = Helpers.HexToColor("9D9239");
    public static Color bggBlack = Helpers.HexToColor("222222");
    public static Color bggGreen = Helpers.HexToColor("00CC29");
    public static Color bggPurple = Helpers.HexToColor("923196");


    public static Color stoneShiftBlue = Helpers.HexToColor("7FBFBF");

    internal static Color takeoverGreen = Helpers.Color255(36, 122, 0);
	internal static Color takeoverRed = Helpers.Color255(189, 78, 78);
	internal static Color takeoverPurple = Helpers.Color255(102, 154, 80);
    
    internal static Color takeoverPuce = Helpers.Color255(128, 68, 68);
    internal static Color takeoverBlue = Helpers.Color255(76, 91, 161);
    internal static Color takeoverOrange = Helpers.Color255(203, 161, 109);
    public static Color takeoverLightGreen = Helpers.Color255(138, 145, 111);

    internal static Color commanderGrey = Color.grey;
	public static Color commanderCoral = Helpers.Color255(219, 77, 77);
	public static Color commanderBlue = Helpers.Color255(88, 158, 158);
	public static Color commanderOrange = Helpers.Color255(174, 98, 5);
	public static Color commanderPink = Helpers.Color255(229, 101, 225);
	public static Color commanderCobalt = Helpers.Color255(38, 38, 38);

	public static Color axesPlains = Helpers.Color255(209, 154, 63);
	//public static Color axesWater = Helpers.Color255(15, 25, 41);
	public static Color axesWater = Helpers.Color255(77, 112, 126);
	public static Color axesRoad = Helpers.Color255(74, 38, 20);
	public static Color axesRoadClear = Helpers.Color255(74, 38, 20, 0);
	public static Color axesGold = Helpers.Color255(209, 154, 63, 255);

	public static Color axesMeat = Helpers.Color255(152, 58, 58);

	public static Color axesStone = Helpers.Color255(89, 106, 98);

	public static Color houseColor = Helpers.Color255(85, 56, 48);

	public static Color axesPurple = Helpers.Color255(109, 26, 109);

	public static Vector3 FAR_POS = new Vector3(10000f, 10000f, 0f);

	public static Color playerColor = Helpers.Color255(51, 51, 34);

    public static int FIRST_SHRINE_NUM = 100;

    public static int PLACEMENT_MATCH_MAX_RANK = 25;

    public static int UI_LAYER = 5;
    public static int DEFAULT_LAYER = 0;

    public static float ENEMY_MOVE_TIME = 0.3f;

    public const float SLOW_DOWN_AMOUNT = 0.25f;
    public const float SLOW_DOWN_DURATION = 2f;

    public static int FINAL_FLOOR = 3;

    public static Color GetPlayerColor(Player player)
    {
        if (player == Player.You)
            return Color.blue;

        if (player == Player.Opponent)
            return Color.red;

        return Color.white;
    }

}
