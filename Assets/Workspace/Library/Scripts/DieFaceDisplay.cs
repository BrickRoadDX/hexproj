﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieFaceDisplay : MonoBehaviour
{
    public ObjectDisplay faceDisplay;

    public void Init(DiceType dieType)
    {
        List<DieFace> faces = DieDef.GetFacesForDiceType(dieType);
        foreach(DieFace face in faces)
        {
            Die d = DiceManager.Instance.CreateDie(dieType);
            d.SetDieFace(face, true);
            d.boxCollider.enabled = false;
            d.hoverZoom.Init(0.4f);
            faceDisplay.AddObject(d, true);
        }
    }
}
