﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use Helpers.MakeCooldown to initialize as a monobehavior on a GameObject
/// Can be used to track cooldowns. Trigger events every X seconds.
/// </summary>
public class Cooldown : MonoBehaviour
{
    public delegate bool BoolFunction();

    float timespan; //how long the cooldown is
    public Action timerEndCallback; //callback for when the timer hits zero
    public BoolFunction shouldTimerRun; //timer will only tick if this function evaluates to true

    float timer = 0f;

    public bool paused = true;

    public Cooldown(bool thisDoesntWorkUseHelperMethod)
    {
        Debug.LogError("use Helpers.Makecooldown to make a cooldown");
    }

    private void Update()
    {
        if (shouldTimerRun != null && !shouldTimerRun())
            return;

        if(!IsReady() && !paused)
        {
            timer -= Time.deltaTime;

            if (timerEndCallback != null && IsReady())
                timerEndCallback();
        }
    }

    internal void Pause()
    {
        paused = true;
    }

    public void Play()
    {
        paused = false;
    }

    public bool IsReady()
    {
        return timer <= 0f;
    }

    public void Reset()
    {
        timer = timespan;
    }

    public void StartCooldown(float newDuration, bool preFinishCooldown = false, bool startPaused = false)
    {
        this.timespan = newDuration;
        paused = startPaused;
        Reset();

        if (preFinishCooldown)
            timer = 0f;
    }

    public float GetTimespan()
    {
        return timespan;
    }
}
