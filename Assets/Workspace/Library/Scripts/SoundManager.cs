﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class SoundManager : Singleton<SoundManager>
{
	public static float soundVolume = 0f;

    public List<AudioSource> audioSources;

	public void PlaySound(string soundName)
	{
		AudioClip clip = Resources.Load(soundName) as AudioClip;

		if (clip != null)
		{
			Sound sound = Helpers.CreateInstance<Sound>("Sound", this.transform);
			sound.audioSource.clip = clip;
			sound.audioSource.volume = soundVolume;
			this.CallActionDelayed(sound.audioSource.Play);
			this.CallActionDelayed(delegate () { sound.gameObject.DestroySelf(); }, Helpers.AtLeast(clip.length, 0.3f));
		}
		else
		{
            Debug.Log(string.Format("Couldn't find sound: '{0}'", soundName));
        }
    }

    public void PlaySound(AudioClip clip)
    {
        if (clip != null)
        {
            Sound sound = Helpers.CreateInstance<Sound>("Sound", this.transform);
            sound.audioSource.clip = clip;
            sound.audioSource.volume = soundVolume;
            this.CallActionDelayed(sound.audioSource.Play);
            this.CallActionDelayed(delegate () { sound.gameObject.DestroySelf(); }, Helpers.AtLeast(clip.length, 0.3f));
        }
    }

    public static void SetSoundVolume(float value)
	{
		soundVolume = value;

        if (HasPrivateInstance())
            Instance.UpdateSoundVolumes();
	}

    void Start()
    {
        UpdateSoundVolumes();
    }

    void UpdateSoundVolumes()
    {
        foreach (AudioSource sound in Instance.audioSources)
        {
            sound.volume = soundVolume;
        }
    }

    List<Sound> loopingSounds = new List<Sound>();
    internal void PlayLoopSound(string soundName)
    {
        AudioClip clip = Resources.Load(soundName) as AudioClip;

        if (loopingSounds.Any(x => x.audioSource.clip.name.Equals(soundName)))
            return;

        if (clip != null)
        {
            Sound sound = Helpers.CreateInstance<Sound>("Sound", this.transform);
            sound.audioSource.clip = clip;
            sound.audioSource.volume = soundVolume;
            sound.audioSource.loop = true;
            sound.audioSource.Play();
            loopingSounds.Add(sound);
        }
        else
        {
            Debug.Log(string.Format("Couldn't find sound: '{0}'", soundName));
        }
    }

    public void StopLoopSound(string soundName)
    {
        if (!loopingSounds.Any(x => x.audioSource.clip.name.Equals(soundName)))
            return;

        int index = loopingSounds.FindIndex(x => x.audioSource.clip.name.Equals(soundName));
        loopingSounds[index].gameObject.DestroySelf();
        loopingSounds.RemoveAt(index);
    }
}
