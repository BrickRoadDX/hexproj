﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Cost
{
    public Dictionary<ResourceType, int> values = new Dictionary<ResourceType, int>();

	public Cost()
	{
        InitDict();
	}

    public Cost(int money = 0)
    {
        InitDict();

        Set(ResourceType.Money, money);
    }

    void InitDict()
    {
        foreach (ResourceType type in Helpers.GetAllEnumTypes<ResourceType>(true))
            values.Add(type, 0);
    }

    public void Set(ResourceType type, int value)
	{
		values[type] = value;
	}

	public int Get(ResourceType type)
	{
		return values[type];
    }

	public void Add(ResourceType type, int amount)
	{
		Set(type, Get(type) + amount);
	}

    internal bool HasCost()
    {
        foreach (ResourceType type in Helpers.GetAllEnumTypes<ResourceType>(true))
        {
            if (values.ContainsKey(type) && values[type] > 0)
                return true;
        }

        return false;
    }

    public string CostString()
    {
        if (!HasCost())
            return "";

        string costString = "Cost: ";

        costString += ResourceString();

        return costString;
    }
    
    public string ResourceString()
    {
        string resourceString = "";

        foreach (ResourceType type in Helpers.GetAllEnumTypes<ResourceType>(true))
        {
            if (values.ContainsKey(type) && values[type] > 0)
                resourceString += ResourceDef.GetTextCode(type) + " " + values[type];
        }

        return resourceString;
    }
}