﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AbilityButton : MonoBehaviour
{
    public SpriteRenderer abilitySprite;
    public ButtonColorSetter colorSetter;
    public tk2dUIItem uiItem;
    public BoxCollider myCollider;
    public TextMeshPro actionsRemainingText;

    [HideInInspector]
    public AbilityType abilityType;

    public void Init(AbilityType abilityType)
    {
        this.abilityType = abilityType;
        abilitySprite.sprite = Helpers.GetSprite(AbilityDef.GetAbilitySpriteName(abilityType));
        colorSetter.Init(AbilityDef.GetAbilityBackgroundColor(abilityType));
        abilitySprite.color = AbilityDef.GetAbilitySpriteColor(abilityType);
        actionsRemainingText.text = "";
    }

    private void OnEnable()
    {
        uiItem.OnClick += HandleClicked;
    }

    private void OnDisable()
    {
        uiItem.OnClick -= HandleClicked;
    }

    void HandleClicked()
    {
        AbilityManager.Instance.AbilityClicked(abilityType, this);
    }
}
