﻿using DG.Tweening;
using Gamelogic.Grids;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardBase : MonoBehaviour, SortingOrderObject, Draggable
{
	public CardID cardID;
	public TextMeshPro mainText;
    public TextMeshPro titleText;
    public TextMeshPro costText;

	public SpriteRenderer bgSprite;
    public Transform patternContainer;

    public HoverZoom hoverZoom;

	public tk2dUIItem uiItem;

    public BoxCollider cardCollider;

    [HideInInspector]
    public Direction currentDir = Direction.None;

    private bool extended = false;

    [HideInInspector]
    public List<Renderer> renderers = new List<Renderer>();

    List<int> sortingOrder = new List<int>();

    public SpriteRenderer hiddenSprite;
    bool hidden = false;

    public CardType cardType { get { return cardID == null ? CardType.None : cardID.cardType; } }

    public Transform hoverMoveTransform;

    [HideInInspector]
    public CellPattern cellPatternObject;

    void Awake()
    {
        currentDir = Direction.Up;

        if (!(this is Card))
            Debug.LogError("Whoa, why is this the base class CardBase");

        cachedBoxColliderSize = cardCollider.size;
    }

    void CacheSortingOrders()
    {
        renderers.Clear();
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        sortingOrder.Clear();
        foreach (Renderer r in this.renderers)
            sortingOrder.Add(r.sortingOrder);
    }

    public void ResetSortingOrders()
    {
        int index = 0;
        foreach (Renderer r in this.renderers)
        {
            r.sortingOrder = sortingOrder[index];
            index++;
        }
    }

    bool allowUIHover = true;
    public void HandleRemovedFromCardManager()
    {
        allowUIHover = false;

        if (posTweener != null)
            posTweener.Kill();

        hoverMoveTransform.localPosition = Vector3.zero;
    }

    bool hoveringIn = false;
    public void SetCardHovered(bool hovered)
    {
        if (!allowUIHover)
            return;

        hoveringIn = hovered;
        TweenToHoverInPos(new Vector3(hoverMoveTransform.transform.localPosition.x, hovered ? 2f : 0f, hoverMoveTransform.transform.localPosition.z));
    }

    Tweener posTweener;
    public void TweenToHoverInPos(Vector3 pos)
    {
        if (posTweener != null)
            posTweener.Kill();

        if (IsInPlayerHand())
        {
            posTweener = hoverMoveTransform.DOLocalMove(pos, 0.3f).OnComplete(TweenComplete);
        }
        else
        {
            TweenComplete();
        }

        if (!hoveringIn)
        {
            if (renderersForward)
            {
                renderersForward = false;
                Helpers.AdjustSortingOrders(gameObject, -1000);
            }
        }
    }

    bool renderersForward = false;
    void TweenComplete()
    {
        if (hoveringIn)
        {
            if(!renderersForward)
            {
                renderersForward = true;
                Helpers.AdjustSortingOrders(gameObject, 1000);    
            }
        }
    }

    string planetSpriteName = "";
	public void SetupCard(CardID card)
	{
        titleText.text = "";
        mainText.text = "";
        costText.text = "";
        patternContainer.gameObject.DestroyChildren();
        this.cardID = card;
        titleText.text = card.cardType.ToString();
        SetupCardPattern();

        SetColliderSize(false);

        gameObject.name = "Card: " + card.cardId + " " + card.GetCardString();

        UpdateDisplay();
        CacheSortingOrders();
    }

	void OnEnable()
	{
		uiItem.OnClick += HandleCardClicked;
	}

	void OnDisable()
	{
		uiItem.OnClick -= HandleCardClicked;
	}

    public bool IsCastingCard(Player player = Player.You)
    {
        return CardManager.Instance.IsCastingCard((Card)this);
    }

    public bool IsInPlayerHand()
    {
        return CardManager.Instance.GetHand(Player.You).Contains(this);
    }

    public void HandleCardClicked()
	{
		if (Helpers.GameActionsBlocked())
			return;

        if(IsInPlayerHand())
        {
            if (InputManager.Instance.CanPlayCards())
            {
                CardManager.Instance.PlayCard((Card)this);
            }
        }
        else if(PileMarket.CardIsInMarket((Card)this))
        {
            PileMarket.FindCardAndClick((Card)this);
        }
        else if (FlowMarket.CardIsInMarket((Card)this))
        {
            FlowMarket.FindCardAndClick((Card)this);
        }
    }

    public void CleanUp()
	{
        if(this != null && this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
	}

    public void UpdateDisplay()
    {
        mainText.text = CardDef.GetMiddleText(cardID.cardType);
    }

    internal void SetHidden(bool hidden)
    {
        this.hidden = hidden;
        hiddenSprite.gameObject.SetActive(hidden);
    }

    Vector3 cachedBoxColliderSize = Vector3.zero;
    internal void SetColliderSize(bool isExtended)
    {
        extended = isExtended;
        Vector3 sizeToUse = cachedBoxColliderSize;
        Vector3 centerToUse = new Vector3(0f, -3f, 0f);

        if (!isExtended)
        {
            centerToUse = Vector3.zero;
            sizeToUse.y = -6f;
        }

        cardCollider.center = centerToUse;
        cardCollider.size = new Vector3(sizeToUse.x, sizeToUse.y, cardCollider.size.z); 
        //don't mess with z cause we're gonna do that for the SortingOrderObject part
    }

    internal void SetupCardPattern()
    {
        cellPatternObject = CardDef.GetCellPatternForCardType(cardType);

        if (cellPatternObject == null)
            return;

        cellPatternObject.transform.parent = this.patternContainer;

        foreach (CellPatternSpace ps in cellPatternObject.patternSpaces.Values)
        {
            CreateRenderer(ps, Lookup.GetCardPatternSpaceSprite(ps.spaceType), patternContainer, 0.7f);
            CreateRenderer(ps, Helpers.GetSelectVisualSpriteName(), patternContainer);
        }
    }

    public SpriteRendererGo CreateRenderer(CellPatternSpace space, string spriteName, Transform parent, float localScale = 1f)
    {
        SpriteRendererGo srg = Helpers.CreateInstance<SpriteRendererGo>("SpriteRendererGo", parent);
        srg.gameObject.SetLayerRecursively("UI");
        srg.spriteRenderer.sprite = Helpers.GetSprite(spriteName);

        Vector3 localPosition = Helpers.GetVectorPositionOffsetFromOrigin(space.position);
        localPosition = localPosition * 0.18f;

        localPosition.x += Lookup.GetXOffsetForCardType(cardID.cardType);
        localPosition.y += Lookup.GetYOffsetForCardType(cardID.cardType);

        srg.transform.localPosition = localPosition;
        srg.transform.localScale = Vector3.one * 0.2f * localScale;
        srg.spriteRenderer.sortingOrder = mainText.sortingOrder;
        srg.spriteRenderer.color = Color.black;
        srg.gameObject.name = srg.spriteRenderer.sprite.name;

        return srg;
    }

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

    public BoxCollider GetCollider()
    {
        return cardCollider;
    }

    public void Rotate(bool right)
    {
        if (!CardManager.Instance.canRotateCards)
            return;

        Direction nextDirection = Helpers.MainDirections().GetNextElement(currentDir);
        if (right)
            nextDirection = Helpers.MainDirections().GetPrevElement(currentDir);

        SetRotation(nextDirection);
    }

    public void SetRotation(Direction newDir)
    {
        hoverMoveTransform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Helpers.GetAngleFromDirection(newDir)));
        currentDir = newDir;
    }

    public FlatHexPoint GetHoveredPoint()
    {
        Vector3 point = hoverMoveTransform.position;
        point += Vector3.left * bgSprite.bounds.extents.x;
        point += Vector3.up * (bgSprite.bounds.extents.y);

        //Helpers.ShowPointer(point, GameLayer.UI);

        return Helpers.GetHoveredPoint(point, gameObject.GetGameLayer());
    }

    public MonoBehaviour GetMonoBehaviour()
    {
        return this;
    }
}
