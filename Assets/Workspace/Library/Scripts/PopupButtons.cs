﻿using UnityEngine;
using System.Collections;
using System;
using TMPro;
using System.Collections.Generic;

public class PopupButtons : Popup
{
    public List<PopupButton> buttons;
    public TextMeshPro mainText;
    public Transform buttonContainer;
    const float BUTTON_SPACING = 6f;

    public void Init(string mainString, string buttonOneText, Action callback)
    {
        Init(mainString, new List<string>() { buttonOneText }, new List<Action>() { callback });
    }

    public void Init(string mainString, List<string> buttonTextList, List<Action> buttonCallbackList)
    {
        mainText.text = mainString;

        List<float> positions = Helpers.GetElementPositions(buttonTextList.Count, BUTTON_SPACING);
        for(int i = 0; i < buttonTextList.Count; i++)
        {
            PopupButton pb = Helpers.CreateInstance<PopupButton>("PopupButton", buttonContainer, true);
            pb.callback = buttonCallbackList[i];
            pb.text.text = buttonTextList[i];
            pb.transform.SetLocalPositionX(positions[i]);
            buttons.Add(pb);
        }
    }


}
