﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;
using DG.Tweening;
using System.Linq;
using TMPro;

public class CellBase : SpriteCell
{
    public TextMeshPro debugText;

    public ObjectDisplay actorDisplay;

    public ObjectDisplay tileDisplay;
    public Tile tile { get { return tileDisplay.Count < 1 ? null : tileDisplay.GetCastedList<Tile>()[0]; } }

    public ObjectDisplay itemDisplay;
    public int maxItems = 1;
    public int ItemCount { get { return itemDisplay.Count; } }

    public ObjectDisplay cardDisplay;
    public ObjectDisplay diceDisplay;

    [HideInInspector]
    public FlatHexPoint point;
    public bool isOccupied { get { return ActorCount > 0; } }
    public int ActorCount { get { return actorDisplay.Count; } }

    public SpriteRenderer backgroundSprite;
    public SpriteRenderer terrainSprite;

    public TerrainType terrainType = TerrainType.None;
    private void Start()
    {
        if (!(this is Cell))
            Debug.LogError("Whoa, why is this the base class CellBase");

        debugText.text = "";
    }

    public void SetDebugText(string text)
    {
        debugText.text = text;
    }

    public virtual void Init()
    {
        startingColorCache = Color;
        SetColor(startingColorCache);

        SetDebugText("");

        this.StopDelayedActions();

        actorDisplay.ClearAndDestroyAll();
        tileDisplay.ClearAndDestroyAll();
        itemDisplay.ClearAndDestroyAll();
        cardDisplay.ClearAndDestroyAll();
        diceDisplay.ClearAndDestroyAll();
    }

    public Actor SpawnActor(ActorType type, bool wasKilled = true)
    {
        if (type == ActorType.None)
            return null;

        if(!HasRoomForActor())
            DestroyActor(GetRandomActor(), DeathAnimation.Fly, Vector3.zero, wasKilled);

        Actor a = Actor.CreateActor(type);
        AddActor(a, true);
        Board.Instance.actorsOnBoard.Add(a);

        AbilityManager.Instance.UpdateAbilityVisuals();

        return a;
    }

    public void AddActor(Actor a, bool instant = false, bool animationHandled = false)
    {
        a.cell = (Cell)this;
        actorDisplay.AddObject(a, instant);
    }

    public void RemoveActor(Actor a)
    {
        if (actorDisplay.Contains(a))
            actorDisplay.RemoveObject(a);
    }

    public void SetColor(Color c)
    {
        backgroundSprite.color = c;
    }

    internal void HoverOut()
    {

    }


    internal void HoverOver()
    {

    }

    public void Init(FlatHexPoint point)
    {
        this.point = point;
        this.name = point.ToString();
    }

    internal bool IsTerrainWalkable()
    {
        return TerrainGroundWalkable(terrainType);
    }

    internal bool IsPlayerWalkable()
    {
        return IsTerrainWalkable() && HasRoomForActor();
    }

    internal bool IsEnemyWalkable()
    {
        return IsTerrainWalkable() && HasRoomForActor();
    }

    internal bool HasAdjacentActor(ActorType type)
    {
        foreach (Cell cell in Board.Instance.GetAdjacent((Cell)this))
        {
            if (cell.GetActorTypes().Any(x => x == type))
                return true;
        }
        return false;
    }

    internal bool IsPushable()
    {
        return true;
    }

    internal bool HasCellVisual()
    {
        return Board.Instance.cellHighlightCells.Contains((Cell)this);
    }

    internal bool HasEnemy()
    {
        return GetActorTypes().Any(x => ActorDef.IsEnemy(x));
    }

    internal bool IsTall()
    {
        return isOccupied;
    }

    internal bool HasPlayerUnit()
    {
        return GetActorTypes().Any(x => ActorDef.IsPlayerUnit(x));
    }

    internal bool HasReadyUnit()
    {
        return GetActors().Any(x => ActorDef.IsPlayerUnit(x.type) && x.isReady);
    }

    public bool HasTerrainType(TerrainType type)
    {
        return terrainType == type;
    }

    public static int counter = 0;
    internal void Attack(Actor attacker, int damage = 1)
    {
        ShowColorAnimation(Color.red * 0.75f, 1f);

        foreach(Actor a in GetActors())
            a.Attack(attacker, damage);
    }

    public List<Actor> GetActors()
    {
        return actorDisplay.GetCastedList<Actor>();
    }

    public void DestroyAllActors(DeathAnimation deathAnimType = DeathAnimation.None)
    {
        int numActors = ActorCount;
        for(int i = 0; i < numActors; i++)
            DestroyActor(GetFirstActor(), deathAnimType);
    }

    public void DestroyActor(Actor toDestroy, DeathAnimation deathAnimType = DeathAnimation.None, Vector3 direction = default(Vector3), bool wasKilled = true)
    {
        if (toDestroy == null)
            return;

        if (Board.Instance.actorsOnBoard.Contains(toDestroy))
            Board.Instance.actorsOnBoard.Remove(toDestroy);

        AbilityManager.Instance.UpdateAbilityVisuals();

        RemoveActor(toDestroy);

        float deathTime = 0f;

        switch (deathAnimType)
        {
            case DeathAnimation.None:
                toDestroy.gameObject.DestroySelf();
                break;

            case DeathAnimation.Fly:
                float tweenDuration = 4f;
                Vector3 pos = Helpers.CirclePoint(Helpers.RandomFloatFromRangeInclusive(0f, 1f), Board.Instance.GetBoardWidth() * 4f, toDestroy.transform.position);

                if (direction != default(Vector3) && direction != Vector3.zero)
                {
                    direction = direction.normalized;
                    pos = direction * Board.Instance.GetBoardWidth() * 7.5f;
                }

                toDestroy.transform.DOMove(pos, tweenDuration).SetEase(Ease.Linear).OnComplete(delegate () { Destroy(toDestroy.gameObject); });
                toDestroy.transform.DOScale(Vector3.one * 25f, tweenDuration);
                deathTime = 1f;
                break;
            case DeathAnimation.Fall:
                toDestroy.transform.DORotate(new Vector3(0f, 0f, 45f), 0.3f);
                toDestroy.transform.DOScale(0.01f, 2f);
                toDestroy.gameObject.AddComponent<Temporary>().mLifespan = 1f;
                deathTime = 1f;
                SoundManager.Instance.PlaySound("fall");
                this.CallActionDelayed(delegate ()
                {
                    SoundManager.Instance.PlaySound("splash");
                }, deathTime);

                break;
            case DeathAnimation.PositionFall:
                toDestroy.transform.DOMove(direction, 0.3f);
                toDestroy.transform.DORotate(new Vector3(0f, 0f, 45f), 0.3f);
                toDestroy.transform.DOScale(0.01f, 2f);
                toDestroy.gameObject.AddComponent<Temporary>().mLifespan = 1f;
                deathTime = 1f;
                SoundManager.Instance.PlaySound("fall");

                this.CallActionDelayed(delegate ()
                {
                    SpriteEffect spriteEffect = Helpers.MakeSpriteEffect("Splash", this.transform);
                    spriteEffect.gameObject.transform.position = direction;
                    SoundManager.Instance.PlaySound("splash");
                }, deathTime);
                break;
        }

        if (wasKilled)
        {
            toDestroy.HandleActorKilled();
        }
    }

    internal bool HasLivingActor()
    {
        return ActorCount > 0;
    }

    public bool HasRoomForActor()
    {
        return ActorCount < ActorDef.NumActorsPerCell();
    }

    internal void DestroyCell()
    {
        foreach(Actor a in GetActors())
            DestroyActor(a, DeathAnimation.None);

        this.gameObject.DestroySelf();
    }

    public ActorType GetActorType()
    {
        if (ActorCount > 0)
            return GetFirstActor().type;

        return ActorType.None;
    }
    
    public List<ActorType> GetActorTypes()
    {
        List<ActorType> types = new List<ActorType>();
        foreach (Actor a in GetActors())
            types.Add(a.type);

        return types;
    }

    internal bool HasActorType(ActorType type)
    {
        return GetActorTypes().Any(x => x == type);
    }

    public int GetPushDepth(FlatHexPoint dir)
    {
        int count = 0;

        Helpers.StartWhileLoopCounter();

        Cell currCell = (Cell)this;

        while (currCell != null && currCell.HasLivingActor())
        {
            if (currCell.HasLivingActor())
                count++;

            currCell = Board.Instance.GetCellIfAvailable(currCell.point + dir);
        }

        return count;
    }

    public Actor GetFirstActor()
    {
        if(actorDisplay.Count > 0)
            return GetActors().First();

        return null;
    }

    public Actor GetRandomActor()
    {
        if (actorDisplay.Count > 0)
            return GetActors().RandomElement();

        return null;
    }

    internal Actor GetNextActor(Actor selectedActor)
    {
        if(actorDisplay.Contains(selectedActor) && actorDisplay.Count > 0)
            return actorDisplay.GetCastedList<Actor>().GetNextElement(selectedActor);

        return null;
    }

    internal Actor GetPrevActor(Actor selectedActor)
    {
        if (actorDisplay.Contains(selectedActor) && actorDisplay.Count > 0)
            return actorDisplay.GetCastedList<Actor>().GetPrevElement(selectedActor);

        return null;
    }

    public bool HasActor(Actor a)
    {
        return actorDisplay.Contains(a);
    }

    [HideInInspector]
    public List<Cell> pushVisited = new List<Cell>();
    public void Push(FlatHexPoint dir, bool slipperyFeet = false, bool initialPush = true, bool terrainSliding = false)
    {
        if (initialPush)
            pushVisited.Clear();

        pushVisited.Add((Cell)this);

        List<Actor> pushedActors = GetActors();

        if (pushedActors.Count < 1)
            return;

        if (initialPush)
            SoundManager.Instance.PlaySound("whish");

        FlatHexPoint newPoint = this.point + dir;

        if (!Board.Instance.CellIsAvailalable(newPoint)) // board does not contain this space
        {
            foreach(Actor a in pushedActors)
            {
                a.transform.DOKill();
                a.transform.DOMove(Board.Instance.map[this.point + dir], 0.3f);
                this.DestroyActor(a, DeathAnimation.Fall, default(Vector3));            //off edge

                this.CallActionDelayed(delegate ()
                {
                    SpriteEffect spriteEffect = Helpers.MakeSpriteEffect("Splash", this.transform);
                    spriteEffect.gameObject.transform.position = Board.Instance.map[this.point + dir];

                }, 1f);
            }

            return;
        }

        Cell newCell = Board.Instance.grid[this.point + dir];

        if (newCell.terrainType == TerrainType.Hole)
        {
            foreach (Actor a in pushedActors)
                ActorFallInHole(a, newCell);

            return;
        }
        else if (!newCell.IsPushable())
        { 
            return; //don't push into doors or walls
        }

        if (newCell.HasLivingActor()) //continue to push next object if we're pushing a stack
        {                             //but if we're sliding ourselves, wait to convert until after sliding
            if(!slipperyFeet && !terrainSliding)
                newCell.Push(dir, false, false);
        }

        bool newCellBlocked = newCell.HasLivingActor();
        foreach(Actor a in pushedActors)
        {
            if (slipperyFeet || terrainType == TerrainType.Ice)
            {
                if(!newCellBlocked) //we only want to check whether it was blocked BEFORE we started pushin' stuff
                    a.Move(newCell.point);
            }
            else
            {
                a.Move(newCell.point);
            }
        }

        if (slipperyFeet || newCell.terrainType == TerrainType.Ice)
        {
            if(!newCellBlocked) //keep slipping and sliding
                newCell.Push(dir, slipperyFeet, false, newCell.terrainType == TerrainType.Ice);
            else if (initialPush) //we're pushing a stack, so convert this into a normal stack push
                this.Push(dir, false, initialPush, newCell.terrainType == TerrainType.Ice);
        }
    }

    public static void ActorFallInHole(Actor a, Cell holeCell)
    {
        if (a.dying)
            return;

        a.cell.RemoveActor(a);

        a.transform.DOKill();
        a.transform.DOMove(Board.Instance.map[holeCell.point], 0.3f);
        holeCell.DestroyActor(a, DeathAnimation.Fall, default(Vector3));
        holeCell.CallActionDelayed(holeCell.Splash, 1f);
        holeCell.terrainType = TerrainType.None;
        holeCell.CallActionDelayed(holeCell.SetTerrainNone, 1f);
    }

    public void Splash()
    {
        Helpers.MakeSpriteEffect("Splash", this.transform);
        SoundManager.Instance.PlaySound("splash");
    }

    internal void SpawnSpriteEffect(string spriteName)
    {
        Helpers.MakeSpriteEffect(spriteName, this.transform);
    }

    public static bool TerrainGroundWalkable(TerrainType type)
    {
        switch(type)
        {
            case TerrainType.Hole:
                return false;
        }

        return true;
    }

    internal void ShowColorAnimation(Color color, float duration = 0.3f)
    {
        CellVisual anim = Helpers.CreateInstance<CellVisual>("CellHighlight", this.transform, true);
        anim.sprite.color = color;

        anim.gameObject.AddComponent<Temporary>().mLifespan = duration;

        DOVirtual.Float(0f, 1f, duration, delegate (float fValue) {
            anim.sprite.color = Color.Lerp(color, Helpers.AlphaAdjustedColor(color, 0f), fValue);
        });
    }

    protected Color startingColorCache = Color.white;
    internal Color GetStartingColor()
    {
        return startingColorCache;
    }

    [HideInInspector]
    public Vector3 finalPos = Vector3.zero;

    public void TweenToPosition()
    {
        this.transform.DOLocalMove(finalPos, Board.BUILD_GRID_TRAVEL_TIME).SetEase(Ease.OutQuad);
    }

    public bool IsAdjacent(Cell other)
    {
        return Board.Instance.GetAdjacent(other).Contains(this);
    }

    public bool IsThreatened()
    {
        return ThreateningActors().Count > 0;
    }

    public List<Actor> ThreateningActors()
    {
        List<Actor> threateningActors = new List<Actor>();

        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (!a.dying)
            {
                if (a.EnemyValidMoves().Contains((Cell)this))
                    threateningActors.Add(a);
            }
        }

        return threateningActors;
    }


    public void ShowExplosiveCells()
    {
        List<Cell> cells = GetExplosiveCells();

        foreach (Cell c in cells)
            Board.Instance.CreateCellVisual(c, Color.red);
    }

    public static List<Cell> visitedExplosiveCells = new List<Cell>();
    internal List<Cell> GetExplosiveCells()
    {
        visitedExplosiveCells.Clear();

        VisitExplosiveCell((Cell)this);

        List<Cell> explosiveAffectCells = new List<Cell>();
        foreach(Cell c in visitedExplosiveCells)
        {
            foreach(Cell attackCell in Board.Instance.GetAdjacent(c))
            {
                if (!explosiveAffectCells.Contains(attackCell))
                    explosiveAffectCells.Add(attackCell);
            }
        }

        return explosiveAffectCells;
    }

    void VisitExplosiveCell(Cell cell)
    {
        if (visitedExplosiveCells.Contains(cell))
            return;

        visitedExplosiveCells.Add(cell);

        foreach(Cell c in Board.Instance.GetAdjacent(cell))
        {
            if (c.HasActorType(ActorType.Explosive))
                VisitExplosiveCell(c);
        }
    }

    internal void DetonateExplosive(Actor attacker)
    {
        List<Cell> cells = GetExplosiveCells();

        foreach(Cell cell in cells)
        {
            cell.Attack(attacker);
        }
    }

    public int NumAdjacentEnemies()
    {
        return Board.Instance.GetAdjacent((Cell)this).FindAll(x => x.HasEnemy()).Count;
    }

    public int ClosestPlayerUnit()
    {
        int distance = 99;

        foreach(Actor a in Board.Instance.GetPlayerUnits())
        {
            if (a.cell.point.DistanceFrom(this.point) < distance)
                distance = a.cell.point.DistanceFrom(this.point);
        }

        return distance;
    }

    public int NumEmptyAdjacent()
    {
        int count = 0;    
        foreach(Cell c in Board.Instance.GetAdjacent((Cell)this))
        {
            if (c.IsPlayerWalkable())
                count++;
        }
        return count;
    }

    public int ViabilityScore()
    {
        return 10 * NumEmptyAdjacent() - ClosestPlayerUnit();
    }

    public List<Cell> GetAdjacent()
    {
        return Board.Instance.GetAdjacent((Cell)this);
    }

    public void SetTerrainNone()
    {
        SetTerrainType(TerrainDef.GetDefaultTerrainType());
    }

    public virtual void SetTerrainType(TerrainType type)
    {
        this.terrainType = type;
        terrainSprite.transform.localScale = Vector3.one;

        terrainSprite.gameObject.SetActive(TerrainDef.HasTerrainSprite(type));
        backgroundSprite.color = TerrainDef.GetBackgroundSpriteColor(type, startingColorCache);

        backgroundSprite.sprite = Helpers.GetSprite(TerrainDef.GetBackgroundSpriteString(type));

        if (terrainSprite.gameObject.activeSelf)
        {
            terrainSprite.sprite = Helpers.GetSprite(TerrainDef.GetTerrainSpriteString(type));
            terrainSprite.color = TerrainDef.GetTerrainSpriteColor(type);
            terrainSprite.transform.localScale = TerrainDef.GetLocalScale(type);
        }
    }

    public bool HasCpo()
    {
        return Board.Instance.GetAllCellPatternBoardCells().Contains(this);
    }

    internal bool SpaceCanHoldCPO()
    {
        return CellPatternManager.Instance.SpaceIsDroppable((Cell)this);
    }

    public Tile SpawnTile(TileType type)
    {
        if (type == TileType.None)
            return null;

        if (HasTile())
            DestroyTile();

        Tile tile = Tile.CreateTile(type);
        AddTile(tile, true);
        Board.Instance.tilesOnBoard.Add(tile);

        return tile;
    }

    public TileType GetTileType()
    {
        if (tile == null)
            return TileType.None;

        return tile.tileType;
    }

    public void AddTile(Tile t, bool instant = false)
    {
        tileDisplay.AddObject(t, instant);
        t.cell = (Cell)this;
    }

    public bool HasTile()
    {
        return tile != null;
    }

    public void RemoveTile(Tile t)
    {
        if (tileDisplay.Contains(t))
            tileDisplay.RemoveObject(t);
    }

    public void DestroyTile()
    {
        if (tile == null)
            return;

        if (Board.Instance.tilesOnBoard.Contains(tile))
            Board.Instance.tilesOnBoard.Remove(tile);

        tile.gameObject.DestroySelf();
        RemoveTile(tile);
    }

    public List<Item> SpawnItems(ItemType type, int count, bool fast = true)
    {
        List<Item> spawned = new List<Item>();
        for (int i = 0; i < count; i++)
            SpawnItem(type, fast);

        return spawned;
    }

    public Item SpawnItem(ItemType type, bool fast = true)
    {
        if (type == ItemType.None)
            return null;

        if (!HasRoomForItem())
            DestroyItem(GetRandomItem());

        Item item = Item.CreateItem(type);
        AddItem(item, fast);
        Board.Instance.itemsOnBoard.Add(item);

        return item;
    }

    public void AddItem(Item toAdd, bool instant = false)
    {
        itemDisplay.AddObject(toAdd, instant);
        toAdd.cell = (Cell)this;
    }

    public bool HasAnItem()
    {
        return ItemCount > 0;
    }

    public bool HasRoomForItem()
    {
        return ItemCount < maxItems;
    }

    public List<Item> GetItems()
    {
        return itemDisplay.GetCastedList<Item>();
    }

    public Item GetFirstItem()
    {
        if (itemDisplay.Count > 0)
            return GetItems().First();

        return null;
    }

    public Item GetRandomItem()
    {
        if (itemDisplay.Count > 0)
            return GetItems().RandomElement();

        return null;
    }

    public void RemoveItem(Item toRemove)
    {
        if (itemDisplay.Contains(toRemove))
            itemDisplay.RemoveObject(toRemove);
    }

    public void DestroyItem(Item toDestroy)
    {
        if (toDestroy == null)
            return;

        if (Board.Instance.itemsOnBoard.Contains(toDestroy))
            Board.Instance.itemsOnBoard.Remove(toDestroy);

        RemoveItem(toDestroy);
        toDestroy.gameObject.DestroySelf();
    }

    public void DestroyItemsOfType(ItemType type)
    {
        foreach(Item i in GetItemsOfType(type))
            DestroyItem(i);
    }

    public List<Item> GetItemsOfType(ItemType type)
    {
        List<Item> list = new List<Item>();
        foreach(Item i in GetItems())
        {
            if (i.itemType == type)
                list.Add(i);
        }
        return list;
    }

    public int GetItemTypeCount(ItemType type)
    {
        return GetItemsOfType(type).Count;
    }
}

class CellComparer : IComparer<Cell>
{
    public int Compare(Cell x, Cell y)
    {
        return y.ViabilityScore() - x.ViabilityScore();
    }
}
