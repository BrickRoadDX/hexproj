﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoPane : Singleton<InfoPane>
{
    public Transform middleTransform;
    public Transform displayContainer;

    public TextMeshPro titleText;
    public TextMeshPro descText;
    public SpriteRenderer customSprite;

    public override void Awake()
    {
        base.Awake();
        CleanUpInfo();
    }

    public void CleanUpInfo()
    {
        middleTransform.gameObject.DestroyChildren();
        SetInfoShown(false);
        titleText.text = "";
        descText.text = "";
        customSprite.sprite = Helpers.GetSprite("clear");
    }

    void SetInfoShown(bool shown)
    {
        displayContainer.gameObject.SetActive(shown);
    }

    public void ShowCard(CardType cardType)
    {
        CleanUpInfo();
        if (cardType == CardType.None)
            return;

        SetInfoShown(true);

        Card card = CardManager.Instance.CreateCard(cardType);
        card.transform.parent = middleTransform;
        card.transform.localPosition = Vector3.zero;
        card.hoverZoom.Init(1.5f);
        card.hoverZoom.Disable();
        Helpers.SetSortingOrders(card.gameObject, 82);
    }

    public void ShowActor(ActorType actorType)
    {
        CleanUpInfo();
        if (actorType == ActorType.None)
            return;

        SetInfoShown(true);

        titleText.text = ActorDef.GetActorName(actorType);
        descText.text = ActorDef.GetActorDesc(actorType);
        customSprite.sprite = Helpers.GetSprite(ActorDef.GetSpriteName(actorType));
        customSprite.color = ActorDef.GetActorColor(actorType);
    }

    public void ShowDie(DiceType diceType, DieFace dieFace = DieFace.None)
    {
        CleanUpInfo();
        if (diceType == DiceType.None)
            return;

        SetInfoShown(true);

        DieFaceDisplay dfd = Helpers.CreateInstance<DieFaceDisplay>("DieFaceDisplay", middleTransform, true);
        dfd.Init(diceType);
        Helpers.SetSortingOrders(dfd.gameObject, 800);
        titleText.text = DieDef.GetDieName(diceType);
        dfd.transform.SetLocalPositionY(-4f);

        Die d = DiceManager.Instance.CreateDie(diceType);
        Helpers.SetSortingOrders(d.gameObject, 82);
        d.transform.parent = middleTransform;
        d.hoverZoom.Init(0.7f);
        d.transform.position = customSprite.transform.position;
        d.boxCollider.enabled = false;

        string descString = DieDef.GetDieDesc(diceType);

        if(dieFace != DieFace.None)
        {
            d.SetDieFace(dieFace, true);
            if (descString.Length > 0)
                descString += "\n\n";

            descString += DieDef.GetFaceDesc(dieFace);
        }

        descText.text = descString;
    }
}
