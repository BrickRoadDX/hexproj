﻿using DG.Tweening;
using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InputManagerBase : Singleton<InputManagerBase>
{
    internal InputState inputState;

    [HideInInspector]
    public ScrollwheelHandler scrollwheelHandler;

    [HideInInspector]
    public Actor selectedActor = null;

    [HideInInspector]
    public Cell rewindMoveCell = null;

    public TextMeshPro inputStateReminderText;

    public virtual void Init()
    {
        scrollwheelHandler = Helpers.MakeScrollWheelHandler(this.transform, RightScroll, LeftScroll);

        GoToState(InputState.Playing);
    }

    public void HandleAbiliityClicked(AbilityType type)
    {

    }

    internal void StartPlayingCard(Card cardToCast)
    {

    }

    public virtual void Update()
    {
        DebugInput();
        if(Input.GetMouseButtonUp(1))
            ProcessRightClick();
    }

    public virtual void ProcessRightClick()
    {
        if (CardManager.Instance.CardDisplayUp())
            return; //we might right click or something, don't try to undo actions

        if (CardManager.Instance.TryUndo())
            return; //if cardmanager has an undo (or discard hovered card) do that

        if (DiceManager.Instance.TryUndo())
            return;

        if (AbilityManager.Instance.TryUndo())
            return;

        if (TryUndoMove())
            return;

        if (TryUndoSelect())
            return;

        if (Board.Instance.TryRightClick())
            return; //do this last because we still want to undo if the mouse is over the board

        if (DiceManager.Instance.TryRightClick())
            return;
    }

    public virtual bool TryUndoSelect()
    {
        if (selectedActor != null)
        {
            GoToBaseState();
            return true;
        }
        return false;
    }

    public virtual bool TryUndoMove()
    {
        if(selectedActor != null)
        {
            if(rewindMoveCell != null)
            {
                selectedActor.Move(rewindMoveCell.point);
                GoToBaseState();
                return true;
            }
        }
        return false;
    }

    public virtual void DebugInput()
    {
 
    }

    public virtual void DelayHoverCell (Cell cell)
    {

    }

    public virtual void LeftScroll()
    {

    }

    public virtual void RightScroll()
    {

    }

    public virtual void HandleCellClicked(Cell cell)
    {

    }

    public virtual void HandleCellRightClicked(Cell cell)
    {

    }

    public virtual void StartTurn()
    {
        
    }

    public virtual bool CanPlayCards()
    {
        return !CardManager.Instance.HasCastingCard();
    }

    public virtual bool CanUndoCards()
    {
        return CardManager.Instance.HasCastingCard();
    }

    public void GoToBaseState()
    {
        GoToState(InputState.Playing);
    }

    public virtual void GoToState(InputState newState)
    {
        inputState = newState;
        Debug.Log(newState);

        switch(newState)
        {
            case InputState.Playing:
                Board.Instance.RemoveCellHighlights();
                Board.Instance.RemoveCellVisuals();
                UnselectActor();
                rewindMoveCell = null;
                break;
        }

        string reminderText = AbilityDef.GetReminderText(AbilityManager.Instance.CurrentAbility(), inputState);
        inputStateReminderText.text = reminderText;
    }

    public virtual void SelectActor(Actor a)
    {
        if (selectedActor != null)
            UnselectActor();

        selectedActor = a;
        selectedActor.Zoom(true);

        selectedActor.SetSelected(true);

        InfoPane.Instance.ShowActor(selectedActor.type);
    }

    public virtual void UnselectActor()
    {
        Board.Instance.RemoveCellHighlights();

        if (selectedActor != null)
        {
            selectedActor.Zoom(false);
            selectedActor.SetSelected(false);
        }

        selectedActor = null;

        Board.Instance.ClearDebugText();
    }

    public virtual void HoverCell(Cell cell)
    {

    }

    public virtual void HandleDieClicked(Die clicked)
    {
        switch (inputState)
        {
            case InputState.Playing:
                clicked.ToggleSelection();
                break;
        }
    }

    public virtual void HandleDieRightClicked(Die clicked)
    {
        if (DieDef.TryDoRightClickAbility(clicked))
            DiceManager.Instance.SpendDie(clicked);
        else
            Helpers.ShowReminderText("No valid targets");
    }

    public virtual void HandleCardUndo(Card card)
    {
        Board.Instance.RemoveCellHighlights();
        GoToBaseState();
    }
}

