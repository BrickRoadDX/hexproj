﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;

public class TooltipObject : MonoBehaviour {

	public TextMeshPro tooltipText;

	public int width;
	public int height;
    public float xOffset = 0f;
    public float yOffset = 0f;

    public tk2dUIItem uiItem;

    Vector3 defaultPos = Vector3.zero;
    bool inDefaultPosition = true;
    public bool onLeftSide = false;

    private void Awake()
    {
        if(width == 0)
        {
            SpriteRenderer sr = this.GetComponentInChildren<SpriteRenderer>();
            width = Mathf.FloorToInt(sr.sprite.bounds.extents.x * 2f);
            height = Mathf.FloorToInt(sr.sprite.bounds.extents.y * 2f);
        }
    }

    private void OnEnable()
    {
        uiItem.OnHoverOver += HandleHover;
    }

    private void OnDisable()
    {
        uiItem.OnHoverOver -= HandleHover;
    }

    public void SetDefaultPosition(Vector3 position)
    {
        defaultPos = position;
        inDefaultPosition = true;
        this.transform.position = position;
    }

    private void HandleHover()
    {
        inDefaultPosition = !inDefaultPosition;
        if(inDefaultPosition)
        {
            this.transform.position = defaultPos;
        }
        else
        {
            this.transform.position = defaultPos + (onLeftSide ? Vector3.right : Vector3.left) * width * 2f;
        }
    }
}
