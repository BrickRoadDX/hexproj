﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interfaces 
{

}

interface BrainGoodActor
{
    bool HasPreMove();
    void CacheEnemyMoves();
    void DoEnemyMove();
    void DoPreMove(Action unpauseEnemyTurn);
    bool TakeDamage(int damage); //return true if "survived" false if died
    void HandleMoved();
    bool IsEnemy();
    bool IsPlayerUnit();
}

interface BrainGoodInputManager
{
    void HoverCell(Cell cell);
    void LeftScroll();
    void RightScroll();
    void GoToBaseState();
    void GoToState(InputState newState);
    void DebugInput();
    void Init();
    void StartTurn();
    bool CanPlayCards();
    bool CanUndoCards();
    void HandleCellClicked(Cell cell);
    void HandleCellRightClicked(Cell cell);
    void HandleDieClicked(Die die);
    void HandleDieRightClicked(Die die);
    void HandleCardUndo(Card card);
    void HandleAbiliityClicked(AbilityType abilityType);
}
