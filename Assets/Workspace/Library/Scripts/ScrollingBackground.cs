﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScrollingBackground : MonoBehaviour {

	public List<SpriteRenderer> spriteRenderers;
	public List<int> spriteRendererWidths;

	public float MOVE_SPEED = 10f;

	// Use this for initialization
	void Start ()
	{
		float posX = -450;

		for(int i = 0; i < spriteRenderers.Count; i++)
		{
			spriteRenderers[i].transform.SetLocalPositionX(posX);
			posX += spriteRendererWidths[i];
		}
	}


	// Update is called once per frame
	void Update ()
	{
		float amountToMove = Time.deltaTime * MOVE_SPEED;

		for (int i = 0; i < spriteRenderers.Count; i++)
		{
			spriteRenderers[i].transform.SetLocalPositionX(spriteRenderers[i].transform.localPosition.x - amountToMove);

			if(spriteRenderers[i].transform.localPosition.x < -1.3f * spriteRendererWidths[i])
			{
				spriteRenderers[i].transform.SetLocalPositionX(NextRenderer(i).transform.localPosition.x + TotalWidth() - spriteRendererWidths[i]);
			}
		}
	}

	SpriteRenderer NextRenderer(int index)
	{
		index++;
		if (index > spriteRenderers.Count - 1)
			index = 0;

		return spriteRenderers[index];
	}

	float TotalWidth()
	{
		float total = 0; 
		for(int i = 0; i < spriteRendererWidths.Count; i++)
		{
			total += spriteRendererWidths[i];
		}
		return total;
	}
}
