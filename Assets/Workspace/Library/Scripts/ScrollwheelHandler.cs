﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Init with Helpers.MakeScrollwheelHandler
/// </summary>
public class ScrollwheelHandler : MonoBehaviour
{
    public Action rightScrollAction;
    public Action leftScrollAction;

    float scrollWheelDelay = 0f;
    public void Update()
    {
        if (scrollWheelDelay < 0.1f)
        {
            scrollWheelDelay += Time.deltaTime;
        }
        else
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0 || Input.GetMouseButtonUp(2) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X))
            {
                if (Input.GetAxis("Mouse ScrollWheel") == 0)
                    scrollWheelDelay = 0f;

                bool right = Input.GetAxis("Mouse ScrollWheel") > 0;

                if (Input.GetKey(KeyCode.Z))
                    right = true;

                if (Input.GetKey(KeyCode.X))
                    right = false;

                //do some kind of scroll wheel stuff

                if(right)
                {
                    rightScrollAction();
                }
                else
                {
                    leftScrollAction();
                }
            }
        }
    }
}
