﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//add ItemBag to scene
//add reference to game or something similar
//call additem, pull next item

public class ItemBag : MonoBehaviour
{
    public SpriteRenderer bagSprite;
    public ObjectDisplay display;
    public tk2dUIItem uiItem;
    public TextMeshPro itemCountText;

    private void Awake()
    {
        UpdateItemCount();
    }

    private void OnEnable()
    {
        uiItem.OnClick += HandleClicked;
    }

    private void OnDisable()
    {
        uiItem.OnClick -= HandleClicked;
    }

    private void UpdateItemCount()
    {
        itemCountText.text = display.Count.ToString();
    }

    public void AddItem(Item item)
    {
        item.transform.position = this.transform.position + (Vector3.up * 20f);
        item.gameObject.SetLayerRecursively(GameLayer.UI);
        display.AddObject(item);
        UpdateItemCount();
    }

    public bool HasItem()
    {
        return display.Count > 0;
    }

    public Item PullNextItem(RemoveAnim anim = RemoveAnim.None)
    {
        if (!HasItem())
            return null;

        Item i = display.GetCastedList<Item>().RandomElement();
        display.RemoveObject(i, anim);
        UpdateItemCount();
        return i;
    }

    public void HandleClicked()
    {
        bagSprite.gameObject.SetActive(!bagSprite.gameObject.activeSelf);
    }
}
