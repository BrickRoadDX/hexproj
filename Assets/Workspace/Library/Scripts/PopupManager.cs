﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public enum PopupType
{
    None = 0,
    UseOrSettle = 1,
}

public class PopupManager : Singleton<PopupManager>
{
    public bool menuPopup = false;
	public Popup mCurrentPopup;
    public PopupType mCurrentPopupType = PopupType.None;
	public Queue<Action> mPopupsQueue = new Queue<Action>();
	public BoxCollider popupBlocker;

	void Start()
	{
		popupBlocker.enabled = false;
	}
    
    public void ClearQueue()
    {
        mPopupsQueue.Clear();
    }


    public void ShowPopupOneButton(string mainString, string buttonOneText, Action buttonOneCallback, string prefabName = "PopupButtons")
    {
        Action showPopup = delegate ()
        {
            PopupButtons popup = Helpers.CreateInstance<PopupButtons>(prefabName, this.transform);

            popup.Init(mainString, new List<string>() { buttonOneText}, new List<Action>() { buttonOneCallback});

            mCurrentPopup = popup;
            mCurrentPopupType = PopupType.None;
        };

        ShowOrQueue(showPopup);
    }

    public void ShowPopupTwoButtons(string mainString, string buttonOneText, Action buttonOneCallback, string buttonTwoText, Action buttonTwoCallback, string prefabName = "PopupButtons")
    {
        Action showPopup = delegate ()
        {
            PopupButtons popup = Helpers.CreateInstance<PopupButtons>(prefabName, this.transform);

            popup.Init(mainString, new List<string>() { buttonOneText, buttonTwoText }, new List<Action>() { buttonOneCallback, buttonTwoCallback });

            mCurrentPopup = popup;
            mCurrentPopupType = PopupType.None;
        };

        ShowOrQueue(showPopup);
    }

    public void ShowPopupThreeButtons(string mainString, string buttonOneText, Action buttonOneCallback, string buttonTwoText, Action buttonTwoCallback, string buttonThreeText, Action buttonThreeCallback, string prefabName = "PopupButtons")
	{
		Action showPopup = delegate ()
		{
			PopupButtons popup = Helpers.CreateInstance<PopupButtons>(prefabName, this.transform);

			popup.Init(mainString, new List<string>() { buttonOneText, buttonTwoText, buttonThreeText}, new List<Action>() { buttonOneCallback, buttonTwoCallback, buttonThreeCallback });

			mCurrentPopup = popup;
            mCurrentPopupType = PopupType.None;
		};

        ShowOrQueue(showPopup);
    }

    public void ShowOrQueue(Action showPopup)
	{
		if (mCurrentPopup == null)
		{
			showPopup();
		}
		else
		{
			mPopupsQueue.Enqueue(showPopup);
		}
	}

	public void DismissPopup()
	{
        mCurrentPopupType = PopupType.None;
        menuPopup = false;

        if (Application.isLoadingLevel)
            return;

		if (popupBlocker != null)
			popupBlocker.enabled = false;

		if (mCurrentPopup != null)
        {
            //if (!(mCurrentPopup is PopupTutorial) && TutorialManager.HasPrivateInstance())
            //    TutorialManager.Instance.Resume();

            GameObject.Destroy(mCurrentPopup.gameObject);
            SoundManager.Instance.PlaySound("ClickHigh");
        }

		if (mPopupsQueue.Count > 0)
		{
			this.CallAction(mPopupsQueue.Dequeue());
		}
	}

	internal bool IsShowingPopup()
	{
		return mCurrentPopup != null;
	}

    public bool BlockGameActions()
    {
        if (Game.Instance.IsGameOver())
            return true;

        //if (mCurrentPopup is PopupTutorial && !TutorialManager.TutorialStepHasButton())
        //    return false;

        //if (TutorialManager.Instance.InTutorial() && TutorialManager.TutorialStepHasButton())
        //    return true;

        return IsShowingPopup(); //most of the time block if we're showing a popup
    }


    internal bool DismissablePopup()
    {
        return mCurrentPopupType == PopupType.UseOrSettle;
    }


    void OnDestroy()
    {
        ClearQueue();
    }

}
