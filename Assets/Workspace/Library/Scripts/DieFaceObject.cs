﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DieFaceObject : MonoBehaviour {
	public SpriteRenderer dieBackground;
	public SpriteRenderer dieSprite;
    public SpriteRenderer highlightSprite;
}
