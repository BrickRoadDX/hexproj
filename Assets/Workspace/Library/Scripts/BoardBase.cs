﻿#define HEX
//#define RECT

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;
using System.Linq;
using DG.Tweening;


public enum BoardType
{
    None = 0,
    Hex = 1,
    Rect = 2,
}

public class BoardBase : Singleton<Board>
{
    public bool gameHasBoard = true;
    public bool playBoardAnimation = true;

#if(RECT)
    public RectGrid<Cell> grid;
#else
    public FlatHexGrid<Cell> grid;
#endif

    public IMap3D<FlatHexPoint> map;
	public Camera boardCamera;

	public List<GameObject> cellHighlights = new List<GameObject>();
	public List<Cell> cellHighlightCells = new List<Cell>();

    public List<GameObject> cellVisuals = new List<GameObject>();
    public List<Cell> cellVisualCells = new List<Cell>();

    public List<Actor> actorsOnBoard = new List<Actor>();
    public List<Tile> tilesOnBoard = new List<Tile>();
    public List<Item> itemsOnBoard = new List<Item>();

    public List<CellPattern> cellPatternObjectsOnBoard = new List<CellPattern>();

    public static BoardType GetBoardType()
    {
#if (HEX)
        return BoardType.Hex;
#else
        return BoardType.Rect;
#endif
    }

    public override void Awake()
	{
        BoardBuildMap();

        if (!gameHasBoard)
            return;

		base.Awake();
		BoardBuildGrid();
        Init();
	}

    public virtual void Init()
    {
        if (!gameHasBoard)
            return;

        ClearBoard();

        BuildGridAnimation();
    }

    public void ClearBoard()
    {
        foreach (Cell r in AllCellsDontModify())
            r.Init();

        actorsOnBoard.Clear();
        tilesOnBoard.Clear();
        itemsOnBoard.Clear();
        cellPatternObjectsOnBoard.Clear();

        RemoveCellHighlights();
    }

    public void SpawnAppropriateNumber(ActorType type)
    {
        for (int i = 0; i < GenerationValues.NumToSpawn(type); i++)
        {
            List<Cell> candiates = Board.Instance.GetUnoccupiedWalkableCells();

            if(candiates.Count > 0)
                Board.Instance.GetRandomCellFromList(candiates).SpawnActor(type);
        }
    }

    internal bool CellIsAvailalable(FlatHexPoint FlatHexPoint)
    {
        return grid.Contains(FlatHexPoint) && grid[FlatHexPoint].gameObject.activeSelf;
    }

    public void ResetHover()
    {
        if (oldCell != null)
        {
            HoverOut();
            oldCell.HoverOut();
        }

        oldCell = null;
    }

    Cell oldCell;
	float mTimeOnCell = 0f;
	Vector3 worldPosition;
	FlatHexPoint point;
	Cell cell;
    bool hoveredOnNewCell = false;
    bool hoveredOnNewCellTwo = false;
    void Update()
	{
        if (!gameHasBoard)
            return;

		Rect screenRect = new Rect(0, 0, Screen.width, Screen.height);
		if (!screenRect.Contains(Input.mousePosition))
			return;

		worldPosition = GridBuilderUtils.ScreenToWorld(this.gameObject, Input.mousePosition);
		point = map[worldPosition];
		cell = !grid.Contains(point) ? null : grid[point];

        if (cell == null && oldCell != null)
        {
            HoverOut();
            oldCell.HoverOut();
        }

        if (Input.GetMouseButtonUp(0))
		{
            if (cell != null && !Helpers.GameActionsBlocked() && Helpers.HeldUiItem() == null)
				LeftClickCell(cell);
		}
    }

    public virtual bool TryRightClick()
    {
        if (cell != null && !Helpers.GameActionsBlocked() && Helpers.HeldUiItem() == null)
        {
            RightClickCell(cell);
            return true;
        }
        return false;
    }

    public void CacheEnemyMoves()
    {
        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (!a.dying)
                a.CacheEnemyMoves();
        }
    }

    public bool cacheAllEnemyMoves = false;
    int numCaches = 0;
    private void LateUpdate()
    {
        if(cacheAllEnemyMoves)
        {
            CacheEnemyMoves();

            cacheAllEnemyMoves = false;

            numCaches++;
            //Debug.Log("Cached enemy moves, {0}", numCaches);
        }

        if (cell != null && !Helpers.GameActionsBlocked())
        {
            bool newCell = oldCell != cell;

            if (newCell)
            {
                if (oldCell != null)
                {
                    oldCell.HoverOut();
                    HoverOut();
                }

                hoveredOnNewCell = false;
                oldCell = cell;
                mTimeOnCell = 0f;
            }
            else
            {
                mTimeOnCell += Time.deltaTime;
            }

            if (cell != null)
            {
                if (mTimeOnCell > 0f && !hoveredOnNewCell)
                {
                    hoveredOnNewCell = true;

                    HoverCell(cell);
                    cell.HoverOver();
                }

                if (mTimeOnCell > 1f)
                {
                    DelayHoverCell(cell);
                }
            }
        }
    }

    public List<Actor> GetPlayerUnits()
    {
        return actorsOnBoard.FindAll(x => x.IsPlayerUnit());
    }

    public List<Actor> GetEnemyUnits()
    {
        return actorsOnBoard.FindAll(x => x.IsEnemy());
    }

    internal int NumPlayerUnits()
    {
        return GetPlayerUnits().Count;
    }

    internal Actor SpawnOnClosestEmptySpace(Cell cell, ActorType capturedUnitType)
    {
        for(int i = 0; i < 8; i++)
        {
            foreach(FlatHexPoint p in PointsAtDistance(cell.point, i))
            {
                if (p.HasCell() && p.ToCell().IsPlayerWalkable() && !p.ToCell().HasPlayerUnit())
                {
                    Actor a = p.ToCell().SpawnActor(capturedUnitType);
                    return a;
                }
            }
        }
        return null;
    }

    internal Cell GetCellIfAvailable(FlatHexPoint FlatHexPoint)
    {
        if (CellIsAvailalable(FlatHexPoint))
            return grid[FlatHexPoint];

        return null;
    }

    void HoverCell(Cell cell)
    {
        InputManager.Instance.HoverCell(cell);

        switch(InputManager.Instance.inputState)
        {
            case InputState.ChoosingSpace:
                RemoveCellVisuals();
                if (cellHighlightCells.Contains(cell))
                    CreateCellVisual(cell);
                break;
        }
    }

    void DelayHoverCell(Cell cell)
    {
        InputManager.Instance.DelayHoverCell(cell);
    }

    void HoverOut()
    {
        //if (boardTooltipShower.IsShowingTooltip())
        //    TooltipShower.DestroyCurrentTooltip();
    }

    public bool CanUndo()
    {
        if (Helpers.GameActionsBlocked())
            return false;

        return true;
    }

	public List<Cell> GetUnoccupiedCells()
	{
        List<Cell> cells = new List<Cell>(AllCells());

		cells.RemoveAll(x => x.isOccupied);

		return cells;
	}

    public List<Cell> GetUnoccupiedWalkableCells()
    {
        List<Cell> cells = GetUnoccupiedCells();
        cells.RemoveAll(x => !x.IsTerrainWalkable());

        return cells;
    }

    public Cell GetRandomEmptyAdjacent(Cell cell)
    {
        List<Cell> list = GetAdjacent(cell);
        list.RemoveAll(x => x.isOccupied);
        list.Shuffle();

        if(list.Count > 0)
            return list[0];

        return null;
    }

	public List<Cell> GetAdjacent(Cell cell)
	{
		List<Cell> adjSpaces = new List<Cell>();
		foreach (FlatHexPoint dir in FlatHexPoint.MainDirections)
		{
			FlatHexPoint adj = cell.point + dir;
			if (grid.Contains(adj))
				adjSpaces.Add(grid[adj]);
		}
        adjSpaces.RemoveAll(x => !CellIsAvailalable(x.point));

		return adjSpaces;
	}

    public List<FlatHexPoint> ShortestPath(FlatHexPoint one, FlatHexPoint two, Actor mover, bool removeStart = false)
	{
        IEnumerable<FlatHexPoint> pointsArray = Algorithms.AStar(
        grid,
        one,
        two,
        (p, q) => p.DistanceFrom(q),
        c => true,
        (p, q) => TerrainDef.GetTerrainCost(q.ToCell(), mover));

        if (pointsArray == null)
            return new List<FlatHexPoint>();

        List<FlatHexPoint> points = pointsArray.ToList();

        if(removeStart)
        {
            points.Remove(one);
        }

		return points;
	}

    public Actor ClosestActor(Actor origin, List<Actor> targets)
    {
        int lowDist = 99;
        Actor result = null;

        foreach(Actor a in targets)
        {
            if(a.cell.point.DistanceFrom(origin.cell.point) < lowDist)
            {
                lowDist = a.cell.point.DistanceFrom(origin.cell.point);
                result = a;
            }
        }
        return result;
    }



    bool IsEnemywalkable(Cell cell)
    {
        return cell.IsEnemyWalkable();
    }

    public FlatHexPoint RandomPointAtDistance(FlatHexPoint origin, int distance, List<Cell> cells = null)
	{
		List<FlatHexPoint> points = PointsAtDistance(origin, distance, cells);

		if (points.Count == 0) //return origin to indicate not found
			return origin;

		points.Shuffle();

		return points.First();
	}

	public List<FlatHexPoint> PointsAtDistance(FlatHexPoint origin, int distance, List<Cell> cells = null)
	{
		List<FlatHexPoint> points = grid.ToList();

		if (cells != null)
		{
			points.Clear();
			foreach (Cell c in cells)
			{
				points.Add(c.point);
			}
		}

		points.RemoveAll(x => x.DistanceFrom(origin) != distance);

        points.Shuffle();

		return points;
	}

    public List<FlatHexPoint> PointsWithinDistance(FlatHexPoint origin, int distance, List<Cell> cells = null)
    {
        List<FlatHexPoint> points = GridHelpers.GetNewFlatHexPointGrid().ToPointList().ToList();

        points.RemoveAll(x => x.DistanceFrom(origin) > distance);

        return points;
    }

    bool SpaceAvailable(FlatHexPoint p)
	{
		return grid.Contains(p) && !grid[p].isOccupied;
	}

	public void HighlightPoints(List<FlatHexPoint> points)
	{
		List<Cell> cells = new List<Cell>();

		foreach (FlatHexPoint p in points)
		{
			if (grid.Contains(p))
				cells.Add(grid[p]);
		}

		HighlightCells(cells);
	}

	public void HighlightCells(List<Cell> cells, Color color = default(Color))
	{
		RemoveCellHighlights();

		foreach (Cell c in cells)
		{
			CreateCellHighlight(c, color);
		}
	}

    public Cell lastSelectedCell;
	public void LeftClickCell(Cell cell)
	{
        lastSelectedCell = cell;

        switch(InputManager.Instance.inputState)
        {
            case InputState.ChoosingSpace:
                Card placingCard = CardManager.Instance.GetLastCastCard();
                if (placingCard != null && CardDef.MovesToBoard(placingCard.cardType))
                {
                    CardManager.Instance.MoveCardToBoard(cell);
                }
                else
                {
                    InputManager.Instance.HandleCellClicked(cell);
                }
                break;
            default:
                InputManager.Instance.HandleCellClicked(cell);
                break;
        }
    }

    public void RightClickCell(Cell cell)
    {
        InputManager.Instance.HandleCellRightClicked(cell);
    }

    public void CreateCellVisuals(List<Cell> cells, Color color = default(Color), string visualName = "")
	{
		foreach(Cell c in cells)
		{
			CreateCellVisual(c, color, visualName);
		}
	}

	public CellVisual CreateCellVisual(Cell cell, Color color = default(Color), string visualName = "", bool doubleVisual = false, float sizeMultiplier = 1f, bool addToList = true)
	{
        if (cellVisualCells.Contains(cell) && !doubleVisual)
            return null;

        if (visualName.Length < 1)
            visualName = Helpers.GetSelectVisualSpriteName();

        cellVisualCells.Add(cell);

        CellVisual cv = Helpers.CreateInstance<CellVisual>("CellVisual");

        if(addToList)
            cellVisuals.Add(cv.gameObject);

        cv.transform.parent = cell.transform;
        cv.transform.localPosition = Vector3.zero;
        cv.transform.localScale = cv.transform.localScale * sizeMultiplier;

        cv.sprite.sprite = Helpers.GetSprite(visualName);

        if (color != default(Color))
            cv.sprite.color = color;

        return cv;
    }

	public void RemoveCellVisuals()
	{
		foreach (GameObject go in cellVisuals)
		{
            go.DestroySelf();
        }

        cellVisuals.Clear();
        cellVisualCells.Clear();
	}

    public void CreateCellHighlight(List<Cell> cells, Color color = default(Color))
    {
        foreach(Cell c in cells)
        {
            CreateCellHighlight(c, color);
        }
    }

    public void CreateCellHighlight(FlatHexPoint point, Color color = default(Color))
    {
        Cell cell = GetCellIfAvailable(point);
        if (cell != null)
            CreateCellHighlight(cell, color);
    }

	public void CreateCellHighlight(Cell cell, Color color = default(Color))
	{
		if (cellHighlightCells.Contains(cell))
			return;

        CellVisual cv = Helpers.CreateInstance<CellVisual>("CellHighlight");
        cellHighlights.Add(cv.gameObject);
		cv.transform.parent = cell.transform;
		cv.transform.localPosition = Vector3.zero;
        if (color != default(Color))
            cv.sprite.color = color;

        if (GetBoardType() == BoardType.Rect)
            cv.sprite.sprite = Helpers.GetSprite("512white");

		cellHighlightCells.Add(cell);
	}

    public void RemoveCellHighlights()
    {
        foreach (GameObject cellHighlight in cellHighlights)
        {
            cellHighlight.DestroySelf();
        }

        cellHighlightCells.Clear();
        cellHighlights.Clear();
    }

    void BoardBuildMap()
    {
        grid = GridHelpers.GetNewFlatHexPointGrid();

        float xDist = 110f;
        float yDist = GetBoardType() == BoardType.Hex ? 95f : 110f;

#if (HEX)
        map = new FlatHexMap(new Vector2(xDist, yDist) * 0.048f)
        .WithWindow(boardCamera.rect)
            .AlignMiddleCenter(grid)
            .Translate(new Vector2(0f, -4.5f))
            .To3DXY();
#else
        map = new RectMap(new Vector2(xDist, yDist) * 0.048f)
        .WithWindow(boardCamera.rect)
            .AlignMiddleCenter(grid)
            .Translate(new Vector2(0f, -4.5f))
            .To3DXY();
#endif
    }

    void BoardBuildGrid()
	{
        foreach (FlatHexPoint point in grid)
        {
            CreateCell(point);
        }
    }

    public static float BUILD_GRID_TRAVEL_TIME
    {
        get
        {
            if (Instance.playBoardAnimation)
                return 1f;

            return 0f;
        }
    }

    public static float BUILD_GRID_INTERVAL
    {
        get
        {
            if (Instance.playBoardAnimation)
                return 0.01f;

            return 0f;
        }
    }

    public string GetCellPrefabName(BoardType type)
    {
        return "Cell";
    }

    public Cell CreateCell(FlatHexPoint point)
    {
        Cell cell = Helpers.CreateInstance<Cell>(GetCellPrefabName(GetBoardType()));

        cell.transform.parent = this.transform;

        //float tweenDuration = 1f;
        cell.transform.localScale = Vector3.one;

        cell.SetColor(cell.GetStartingColor());

        cell.Init(point);
        grid[point] = cell;

        return cell;
    }

    private List<Cell> allCellsCache = new List<Cell>();
    internal List<Cell> AllCellsDontModify()
    {
        if (allCellsCache.Count > 0)
            return allCellsCache;

        foreach (FlatHexPoint point in grid)
        {
            if (grid.Contains(point) && grid[point] != null)
                allCellsCache.Add(grid[point]);
        }
        return allCellsCache;
    }

    internal List<Cell> AllCells()
	{
        List<Cell> list = new List<Cell>();
        foreach(FlatHexPoint point in grid)
        {
            if (grid.Contains(point) && grid[point] != null)
                list.Add(grid[point]);
        }
        return list;
	}

	public List<Cell> GetRowColCross(Cell cell)
	{
		List<Cell> cells = new List<Cell>();
		foreach (FlatHexPoint p in grid)
		{
			if (p.Y == cell.point.Y || p.X == cell.point.X)
				cells.Add(grid[p]);
		}
		cells.Remove(cell);
		return cells;
	}

	internal List<Cell> GetRow(int rowIndex)
	{
		List<Cell> cells = new List<Cell>();
		foreach (FlatHexPoint p in grid)
		{
			if (p.Y == rowIndex)
				cells.Add(grid[p]);
		}
		return cells;
	}

    internal List<Cell> GetCol(int colIndex)
	{
		List<Cell> cells = new List<Cell>();
		foreach (FlatHexPoint p in grid)
		{
			if (p.X == colIndex)
				cells.Add(grid[p]);
		}
		return cells;
	}

	public Cell GetRandomCellFromList(List<Cell> cells)
	{
		return cells[Helpers.RandomIntFromRange(0, cells.Count)];
	}

    public Cell GetCell(int row, int col)
	{
		if (grid.Contains(new FlatHexPoint(row, col)))
			return grid[new FlatHexPoint(row, col)];

		Debug.Log(string.Format("Row {0}, Col {1} does not exist", row, col));
		return grid[new FlatHexPoint(0, 0)];
	}

    public List<FlatHexPoint> GetAdjacentPattern(Cell origin)
    {
        List<FlatHexPoint> points = new List<FlatHexPoint>();
        points.Add(origin.point);
        foreach (FlatHexPoint dir in FlatHexPoint.MainDirections)
        {
            points.Add(origin.point + dir);
        }

        return points;
    }

    public List<Actor> ActorsOfType(ActorType a)
    {
        return actorsOnBoard.FindAll(x => x.type == a);
    }

    public ActorType ActorTypeInDirection(Cell cell, Direction d)
    {
        Cell newCell = (cell.point + GridHelpers.GetFlatHexPointFromDirection(d)).ToCell();
        if (newCell != null)
            return newCell.GetActorType();

        return ActorType.None;
    }

    internal bool FirstObjectIsEnemy(Cell cell, Direction d)
    {
        Cell curr = cell;
        FlatHexPoint dir = GridHelpers.GetFlatHexPointFromDirection(d);

        while(curr != null)
        {
            curr = GetCellIfAvailable(curr.point + dir);
            if(curr != null)
            {
                if (curr.HasEnemy())
                    return true;

                if (curr.isOccupied)
                    return false;
            }
        }

        return false;
    }

    internal bool FirstObjectIsPlayer(Cell cell, Direction d)
    {
        Cell curr = cell;
        FlatHexPoint dir = GridHelpers.GetFlatHexPointFromDirection(d);

        while (curr != null)
        {
            curr = GetCellIfAvailable(curr.point + dir);
            if (curr != null)
            {
                if (curr.HasPlayerUnit())
                    return true;

                if (curr.isOccupied)
                    return false;
            }
        }

        return false;
    }

    public FlatHexPoint FirstFlatHexPointToAttack(Cell cell, Direction d, bool stayOnBoard = false)
    {
        FlatHexPoint curr = cell.point;
        FlatHexPoint dir = GridHelpers.GetFlatHexPointFromDirection(d);

        FlatHexPoint lastOnBoard = cell.point;

        while (grid.Contains(curr))
        {
            lastOnBoard = curr;
            curr = curr + dir;
            if (curr.ToCell())
            {
                if (curr.ToCell().isOccupied)
                    return curr;
            }
        }

        if (!curr.HasCell() && stayOnBoard)
            return lastOnBoard;

        return curr;
    }

    public int FirstFlatHexPointDistance(Cell cell, Direction d)
    {
        FlatHexPoint dest = FirstFlatHexPointToAttack(cell, d, true);
        return cell.point.DistanceFrom(dest);
    }

    Sequence buildGrid;
    public void BuildGridAnimation()
    {
        buildGrid = DOTween.Sequence();

        foreach (FlatHexPoint point in grid)
        {
            float circleDistance = GetBoardWidth() * 2f;
            Cell cell = grid[point];
            Vector3 pos = Helpers.CirclePoint(Helpers.RandomFloatFromRangeInclusive(0f, 1f), circleDistance, cell.finalPos);
            cell.transform.DOKill();
            cell.finalPos = map[point];
            cell.transform.position = pos;
            buildGrid.AppendCallback(cell.TweenToPosition);
            buildGrid.AppendInterval(BUILD_GRID_INTERVAL);
        }

        buildGrid.Play();
    }

    public float CellDistance()
    {
        return Mathf.Abs(map[new FlatHexPoint(0, 0)].x - map[new FlatHexPoint(1, 0)].x);
    }

    public int GetNumCellsAcross()
    {
        int minX = 99;
        int maxX = 0;
        foreach(FlatHexPoint p in grid)
        {
            if (p.X > maxX)
                maxX = p.X;

            if (p.X < minX)
                minX = p.X;
        }

        return maxX - minX;
    }

    public float GetBoardWidth()
    {
        return CellDistance() * GetNumCellsAcross() * 1.5f;
    }

    public Cell HoveredCell()
    {
        Vector3 worldPosition = GridBuilderUtils.ScreenToWorld(this.gameObject, Input.mousePosition);
        FlatHexPoint point = map[worldPosition];
        Cell hovered = !grid.Contains(point) ? null : grid[point];

        return hovered;
    }

    public List<FlatHexPoint> GetSpearPoints(FlatHexPoint origin, FlatHexPoint target, int distance = 2)
    {
        List<FlatHexPoint> points = new List<FlatHexPoint>();

        points.Add(target);
        FlatHexPoint dir = target - origin;
        points.Add(origin + dir + dir);

        if (distance > 2)
            points.Add(origin + dir + dir + dir);

        return points;
    }

    internal int NumReadyUnits()
    {
        int count = 0;
        foreach (Actor a in GetPlayerUnits())
            if(a.isReady)
                count++;

        return count;
    }

    public int CalculatePathMoveCost(List<Cell> path, Actor mover)
    {
        //path.RemoveAt(0);
        int cost = 0;
        foreach(Cell cell in path)
        {
            cost += TerrainDef.GetTerrainCost(cell, mover);
        }

        return cost;
    }

    public List<Cell> GetSpacedCells(int walkingDistance, int numTiles)
    {
        List<Cell> cells = new List<Cell>();

        cells.Add(GetUnoccupiedWalkableCells().RandomElement());

        Helpers.StartWhileLoopCounter();
        while (cells.Count < numTiles && Helpers.WhileLoopTick())
        {
            Cell spacedCell = FindSpacedCell(cells, walkingDistance);

            if (spacedCell != null) //we found one
            {
                cells.Add(spacedCell);
            }
            else if (cells.Count < numTiles) // didn't find one so have to reduce strength
            {
                walkingDistance--;
                Debug.LogWarning(string.Format("Reduced walking distance to {0}", walkingDistance));
            }

            if (walkingDistance < 1)
            {
                Debug.LogError("Couldn't find any cells");
                return cells;
            }
        }

        return cells;
    }

    public Cell FindSpacedCell(List<Cell> otherCells, int walkingDistance)
    {
        List<Cell> candidates = GetUnoccupiedWalkableCells();

        foreach (Cell other in otherCells)
        {
            foreach (FlatHexPoint point in GetMovesFromMovespeed(other, walkingDistance, null))
                candidates.Remove(point.ToCell());
        }

        if (candidates.Count < 1)
            return null;

        return candidates.RandomElement();
    }

    public List<FlatHexPoint> GetMovesFromMovespeed(Cell origin, int moveSpeed, Actor mover)
    {
        Dictionary<FlatHexPoint, int> moves = Algorithms.GetPointsInRangeCost<Cell, FlatHexPoint>(
            grid,
            origin.point,
            c => c.IsPlayerWalkable(),
            (p, q) => TerrainDef.GetTerrainCost(q.ToCell(), mover),
            moveSpeed);

        foreach (FlatHexPoint point in moves.Keys)
            point.ToCell().SetDebugText(moves[point].ToString());

        return moves.Keys.ToList();
    }

    public List<Cell> GetTerrainCells(TerrainType type)
    {
        List<Cell> cells = AllCells();
        cells.RemoveAll(x => x.terrainType != type);
        return cells;
    }

    public Cell ClosestCellInCellList(Cell origin, List<Cell> targets)
    {
        int lowDist = 99;
        Cell result = null;

        foreach (Cell c in targets)
        {
            if (c.point.DistanceFrom(origin.point) < lowDist)
            {
                lowDist = c.point.DistanceFrom(origin.point);
                result = c;
            }
        }
        return result;
    }

    public Cell GetRandomDefaultTerrainCell()
    {
        return GetDefaulTerrainCells().RandomElement();
    }

    public List<Cell> GetDefaulTerrainCells()
    {
        List<Cell> defaultTerrainCells = AllCells();
        defaultTerrainCells.RemoveAll(x => x.terrainType != TerrainDef.GetDefaultTerrainType());
        return defaultTerrainCells;  
    }

    public Cell GetRandomNoTileCell()
    {
        return GetNoTileCells().RandomElement();
    }

    public List<Cell> GetNoTileCells()
    {
        List<Cell> noTileCells = AllCells();
        noTileCells.RemoveAll(x => x.HasTile());
        return noTileCells;
    }

    public Cell GetRandomUnoccupiedCell()
    {
        return GetUnoccupiedCells().RandomElement();
    }

    public Cell GetRandomUnoccupiedWalkableCell()
    {
        return GetUnoccupiedWalkableCells().RandomElement();
    }

    public void HighlightAllCpoBoardSpaces()
    {
        RemoveCellHighlights();

        foreach(CellPattern cpo in cellPatternObjectsOnBoard)
            HighlightCpoBoardSpaces(cpo, false);
    }

    public void HighlightCpoBoardSpaces(CellPattern cpo, bool removeHighlights = true)
    {
        if (removeHighlights)
            RemoveCellHighlights();

        if (cpo == null)
            return;

        Color color = CellPatternDef.placedHighlightColor;
        foreach (Cell c in GetCellPatternBoardSpaces(cpo))
        {
            if (!cpo.IsPlaced())
                color = c.SpaceCanHoldCPO() ? CellPatternDef.validPlaceColor : CellPatternDef.invalidPlaceColor;

            CreateCellHighlight(c.point, color);
        }
    }

    public List<Cell> GetCellPatternBoardSpaces(CellPattern cpo)
    {
        return GetCellPatternBoardPoints(cpo).ToCells();
    }

    public List<FlatHexPoint> GetCellPatternBoardPoints(CellPattern cpo)
    {
        List<FlatHexPoint> points = new List<FlatHexPoint>();
        foreach (CellPatternSpace space in cpo.patternSpaces.Values)
            points.Add(GetBoardPointUnderCellPatternSpace(space));

        return points;
    }

    public FlatHexPoint GetBoardPointUnderCellPatternSpace(CellPatternSpace cps)
    {
        return map[cps.transform.position];
    }

    public void DropCellPattern(CellPattern cpoToDrop)
    {
        if (cpoToDrop == null)
            return;

        CellPatternSpace reference = cpoToDrop.cellPatternSpaces.First();
        FlatHexPoint p = GetBoardPointUnderCellPatternSpace(reference);
        Vector3 properPos = map[p];
        Vector3 offset = properPos - reference.transform.position;
        cpoToDrop.transform.position += offset;

        cellPatternObjectsOnBoard.Add(cpoToDrop);
    }

    public void PickupCellPattern(CellPattern cpo)
    {
        if (cellPatternObjectsOnBoard.Contains(cpo))
            cellPatternObjectsOnBoard.Remove(cpo);
    }

    public List<Cell> GetAllCellPatternBoardCells()
    {
        List<Cell> spaces = new List<Cell>();
        foreach(CellPattern cpo in cellPatternObjectsOnBoard)
            spaces.AddRange(cpo.GetBoardSpaces());

        return spaces;
    }

    public List<Cell> setupCells = new List<Cell>();
    internal void ResetSetupCells()
    {
        setupCells = AllCells();
    }

    public Cell NextSetupCell()
    {
        return setupCells.RandomElement(true);
    }

    internal void ShowReadyPlayerUnits()
    {
        foreach (Actor a in GetPlayerUnits())
            if(a.isReady)
                CreateCellHighlight(a.cell);

        return;
    }

    internal void ShowCells(List<Cell> validPlacements)
    {
        HighlightCells(validPlacements);
    }

    internal void ShowCardPlacements(Card dragCard)
    {
        RemoveCellHighlights();
        foreach (Cell cell in Board.Instance.AllCells())
            if (CardDef.ValidCardPlacement(dragCard, cell))
                CreateCellHighlight(cell);
    }

    internal void ShowDicePlacements(Die dragDie)
    {
        RemoveCellHighlights();
        foreach (Cell cell in Board.Instance.AllCells())
            if (DieDef.ValidDiePlacement(dragDie, cell))
                CreateCellHighlight(cell);
    }

    internal void ClearDebugText()
    {
        foreach (Cell c in AllCells())
            c.SetDebugText("");
    }
}

public enum Quadrant
{
    None = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
}
