﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using DG.Tweening;
using Gamelogic.Grids;
using UnityEngine.SceneManagement;

public enum Direction
{
    None = 0,
    Down = 1,
    Up = 3,
    DownRight = 5,
    DownLeft = 6,
    UpLeft = 7,
    UpRight = 8,

    Left = 10,
    Right = 11,
}

public enum GameSpeed
{
    None = 0,
    Slow = 1,
    Fast = 2,
}

public enum GameLayer
{
    None = 0,
    Default = 1,
    UI = 2,
}

public interface SortingOrderObject
{
    void ResetSortingOrders();
    GameObject GetGameObject();
    BoxCollider GetCollider();
}

public static class Helpers
{
	private static StringBuilder s_stringBuilder = new StringBuilder(20);

    public static System.Random rng = new System.Random();

	public const int WHILE_LOOP_INSANITY = 10000;
	public static int whileLoopCounter = 0;

    public static bool isShuttingDown = false;

    static Collider hitCollider;
    public static T RayCastForObjectType<T>(Camera camera) where T : MonoBehaviour
    {
        hitCollider = null;
        T raycastTarget = null;

        hitCollider = RaycastFromMouse(camera);

        raycastTarget = null;
        if (hitCollider != null)
            raycastTarget = hitCollider.GetComponent<T>();

        return raycastTarget;
    }

    static SpriteRendererGo pointer;
    public static void ShowPointer(Vector3 pos, GameLayer layer)
    {
        if (pointer == null)
            pointer = MakeSpriteRenderer("arrow-cursor", null, Color.green, 9999);

        pointer.transform.localScale = Vector3.one * 0.1f;
        pointer.transform.position = pos;
        pointer.transform.gameObject.SetLayerRecursively(layer);
    }

    public static FlatHexPoint GetHoveredPoint(Vector3 pos, GameLayer layer)
    {
        if (layer == GameLayer.UI)
            return Board.Instance.map[Helpers.TranslateFromTk2dWorldtoMainWorld(pos)];

        return Board.Instance.map[pos];
    }

    public static Color RandomColor()
    {
        return new Color(RandomFloatFromRangeInclusive(0f, 1f), RandomFloatFromRangeInclusive(0f, 1f), RandomFloatFromRangeInclusive(0f, 1f));
    }

    internal static ScrollwheelHandler MakeScrollWheelHandler(Transform transform, Action rightScroll, Action leftScroll)
    {
        ScrollwheelHandler swh = transform.gameObject.AddComponent<ScrollwheelHandler>();
        swh.rightScrollAction = rightScroll;
        swh.leftScrollAction = leftScroll;
        return swh;
    }

    public static Camera CameraForGameLayer(GameLayer layer)
    {
        if (layer == GameLayer.UI)
            return tk2dCamera;

        return mainCamera;
    }

    static Bounds adjustedZ;
    public static bool IsMouseWithinBounds2d(Bounds bounds, GameLayer gameLayer = GameLayer.UI)
    {
        adjustedZ = new Bounds(bounds.center, new Vector3(bounds.size.x, bounds.size.y, 1000f));
        return adjustedZ.Contains(CameraForGameLayer(gameLayer).ScreenToWorldPoint(Input.mousePosition));
    }

    public static bool GameActionsBlocked()
    {
        return PopupManager.Instance.BlockGameActions() || CardManager.Instance.CardDisplayUp();
    }

    //pressedUIItem, hitUIitem  (pressed is when mouse down, hit is when mouse up)
    public static tk2dUIItem HeldUiItem() //when clicked
    {
        return tk2dUIManager.Instance.pressedUIItem;
    }

    public static tk2dUIItem HoveredUiItem() //when not clicked
    {
        return tk2dUIManager.Instance.hitUIItem;
    }

    public static bool IsHoveredOrHeld(tk2dUIItem uiItem)
    {
        if (HoveredUiItem() == uiItem)
            return true;

        if (HeldUiItem() == uiItem)
            return true;

        return false;
    }

    static List<GameSpeed> gameSpeeds = Helpers.GetAllEnumTypes<GameSpeed>();
    static GameSpeed currentSpeed = GameSpeed.None;
    public static void RotateGameSpeed()
    {
        currentSpeed = gameSpeeds.GetNextElement(currentSpeed);

        switch(currentSpeed)
        {
            case GameSpeed.None:
                Time.timeScale = 1f;            
                break;
            case GameSpeed.Slow:
                Time.timeScale = 0.1f;
                break;
            case GameSpeed.Fast:
                Time.timeScale = 3f;
                break;
        }
        Debug.Log(string.Format("Game Speed {0}", currentSpeed));
    }

    internal static void ShowCellReminder(Cell cell, string text)
    {
        ShowReminderText(text, 0.5f, cell.transform.position, Color.white, false);
    }

    public static void UpdateSortingOrderObjects(List<MonoBehaviour> monoBehaviours)
    {
        for (int i = 0; i < monoBehaviours.Count; i++)
        {
            if(monoBehaviours[i] is SortingOrderObject)
                UpdateSortingOrderObject((SortingOrderObject)monoBehaviours[i], i);
        }
    }

    public static void UpdateSortingOrderObject(SortingOrderObject slo, int index)
    {
        slo.ResetSortingOrders();
        AdjustSortingOrders(slo.GetGameObject(), index * 20);
        BoxCollider bc = slo.GetCollider();

        if(bc != null)
            bc.size = new Vector3(bc.size.x, bc.size.y, 1f + index * 0.5f);
    }

    public static void SetSortingOrders(GameObject go, int baseValue = 20, List<Renderer> dontAdjust = null)
    {
        List<Renderer> renderers = new List<Renderer>();
        foreach (Renderer r in go.GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        if (dontAdjust != null)
            renderers.RemoveAll(x => dontAdjust.Contains(x));

        renderers.Sort((Renderer x, Renderer y) => x.sortingOrder - y.sortingOrder); //takes an int
        int lowest = renderers[0].sortingOrder;

        foreach (Renderer r in renderers)
            r.sortingOrder = baseValue + (r.sortingOrder - lowest);
    }

    public static void AdjustSortingOrders(GameObject go, int amount = 20, List<Renderer> dontAdjust = null)
    {
        List<Renderer> renderers = new List<Renderer>();
        foreach (Renderer r in go.GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        if (dontAdjust != null)
            renderers.RemoveAll(x => dontAdjust.Contains(x));

        foreach (Renderer r in renderers)
            r.sortingOrder = r.sortingOrder + amount;
    }

    internal static void MakeTemporary(GameObject gameObject, float duration)
    {
        gameObject.AddComponent<Temporary>().mLifespan = duration;
    }

    public static Vector3 GetUICameraCenterPosition()
    {
        return GetCameraCenterPosition(Helpers.tk2dCamera);
    }

    public static Vector3 GetCameraCenterPosition(Camera cam)
    {
        return cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 10f));
    }

    public static Cooldown MakeCooldown(float duration, Transform parent, bool startImmediately = false)
    {
        Cooldown newCooldown = Helpers.MakeGameObjectWithComponent<Cooldown>(parent);
        newCooldown.StartCooldown(duration);
        if(!startImmediately)
            newCooldown.Pause();
        return newCooldown;
    }

    public static SpriteEffect MakeSpriteEffect(string animationName, Transform parent, bool permanent = false, int sortingOrder = 28, float scaleMultiplier = 1f, Vector3 offset = default(Vector3))
    {
        SpriteEffect effect = Helpers.CreateInstance<SpriteEffect>("SpriteEffect", parent, true);
        effect.transform.parent = parent;
        effect.transform.localPosition = Vector3.zero;

        effect.transform.localScale = Vector3.one * 0.1f * scaleMultiplier;
        effect.transform.localPosition += offset;

        foreach (Renderer r in effect.GetComponentsInChildren<Renderer>())
            r.sortingOrder = sortingOrder;

        effect.animator.Play(animationName);

        if (!permanent)
            effect.temporary.mLifespan = (1f / effect.animator.CurrentClip.fps) * effect.animator.CurrentClip.frames.Length;


        return effect;
    }

    public static List<float> GetElementPositions(int numElements, float elementSpacing)
    {
        List<float> elementPositions = new List<float>();
        if (numElements % 2 != 0)
        {
            elementPositions.Add(0f);
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numElements / (float)2);
            for (int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i + 1) * elementSpacing;
                elementPositions.Insert(0, -position);
                elementPositions.Add(+position);
            }
        }
        else
        {
            int elementsOutFromMiddle = Mathf.FloorToInt((float)numElements / (float)2);
            for (int i = 0; i < elementsOutFromMiddle; i++)
            {
                float position = (i + 1) * elementSpacing;
                position -= elementSpacing / 2f;
                elementPositions.Insert(0, -position);
                elementPositions.Add(+position);
            }
        }
        return elementPositions;
    }

    private static Camera _tk2dCamera;
    public static Camera tk2dCamera
    {
        get {
            if (_tk2dCamera == null)
                _tk2dCamera = Game.Instance.tk2dCamera;

            return _tk2dCamera;
        }
    }

    private static Camera _mainCamera;
    public static Camera mainCamera
    {
        get
        {
            if (_mainCamera == null)
                _mainCamera = Game.Instance.mainCamera;

            return _mainCamera;
        }
    }

    public static RadialProgressBar MakeRadialProgressBar(float fullValue, Transform parent, Vector3 localOffset)
    {
        RadialProgressBar rpb = CreateInstance<RadialProgressBar>("RadialProgressBar", parent);
        rpb.Init(fullValue);
        rpb.transform.parent = parent;
        rpb.transform.localPosition = localOffset;
        return rpb;
    }

    public static ReminderText currentReminderText;
    public static ReminderText ShowReminderText(string text, float scale = 1f, Vector3 origin = default(Vector3), Color textColor = default(Color), bool uiText = true, bool centerText = true)
    {
        if (currentReminderText != null && centerText)
            currentReminderText.gameObject.DestroySelf();

        ReminderText reminderText = Helpers.CreateInstance<ReminderText>("ReminderText");
        reminderText.transform.localScale = Vector3.one * scale;

        reminderText.mover.enabled = false;
        reminderText.fader.enabled = false;

        if (textColor == default(Color))
            textColor = reminderText.text.color;

        reminderText.text.color = textColor;

        Vector3 defaultPos = Game.Instance.tk2dCamera.transform.position + Vector3.down * 7f;

        if (origin != default(Vector3))
            reminderText.transform.position = origin;
        else
            reminderText.transform.position = defaultPos;

        reminderText.fader.endColor = AlphaAdjustedColor(textColor, 0f);

        reminderText.mover.enabled = true;
        reminderText.fader.enabled = true;

        reminderText.text.text = text;

        if (!uiText)
        {
            reminderText.text.gameObject.layer = 1;
            reminderText.text.sortingOrder = 100;
        }

        if (centerText)
            currentReminderText = reminderText;

        Debug.Log("Reminder Text: " + text);

        return reminderText;
    }



    public static Vector3 TranslateFromMainWorldToTk2dWorld(Vector3 mainWorldPos)
    {
        Vector3 point = Game.Instance.mainCamera.WorldToScreenPoint(mainWorldPos);
        point = Game.Instance.tk2dCamera.ScreenToWorldPoint(point);
        return point;
    }

    internal static string GetSelectVisualSpriteName()
    {
        return Board.GetBoardType() == BoardType.Hex ? "hex-select" : "box-select";
    }

    internal static string GetBasicShapeSpriteName()
    {
        return Board.GetBoardType() == BoardType.Hex ? "512hex" : "512white";
    }


    public static Vector3 TranslateFromTk2dWorldtoMainWorld(Vector3 uiPos)
    {
        Vector3 point = Game.Instance.tk2dCamera.WorldToScreenPoint(uiPos);
        point = Game.Instance.mainCamera.ScreenToWorldPoint(point);
        return point;
    }

    public static TextMeshObject MakeTextMesh(string text, Transform parent, int sortingOrder = 0, Vector3 localPosition = default(Vector3), Color color = default(Color))
    {
        TextMeshObject effect = CreateInstance<TextMeshObject>("TextMeshObject", parent, true);
        effect.transform.localPosition = localPosition;

        effect.textMesh.text = text;
        effect.textMesh.sortingOrder = sortingOrder;

        if (color != default(Color))
            effect.textMesh.color = color;
        else
            effect.textMesh.color = Color.white;

        return effect;
    }


    public static SpriteRendererGo MakeSpriteRenderer(string spriteName, Transform parent, Color color = default(Color), int sortingOrder = 0, Vector3 localPosition = default(Vector3))
    {
        SpriteRendererGo effect = CreateInstance<SpriteRendererGo>("SpriteRendererGo", parent, true);
        effect.transform.localPosition = localPosition;

        effect.spriteRenderer.sprite = Helpers.GetSprite(spriteName);
        effect.spriteRenderer.color = color;
        effect.spriteRenderer.sortingOrder = sortingOrder;

        if (color != default(Color))
            effect.spriteRenderer.color = color;
        else
            effect.spriteRenderer.color = Color.white;

        return effect;
    }

    internal static void SetLayer(GameObject go, GameLayer layer)
    {
        go.SetLayerRecursively(layer == GameLayer.Default ? 0 : 5);
    }

    public static TemporarySpriteRenderer MakeTempSprite(string spriteName, Transform parent, Vector3 position, float duration = 1f, float scaleMultiplier = 1f, bool permanent = false, int sortingOrder = 28)
    {
        TemporarySpriteRenderer effect = Helpers.CreateInstance<TemporarySpriteRenderer>("TemporarySpriteRenderer", parent, true);
        effect.transform.parent = parent;
        effect.transform.position = Vector3.zero;

        effect.transform.localScale = Vector3.one * scaleMultiplier;
        effect.transform.position = position;

        effect.spriteRenderer.sprite = Helpers.GetSprite(spriteName);

        foreach (Renderer r in effect.GetComponentsInChildren<Renderer>())
            r.sortingOrder = sortingOrder;

        if (!permanent)
            effect.temporary.mLifespan = duration;

        return effect;
    }

    public static void LoadSoundValues()
    {
        AudioListener.volume = 0.4f;

        if (!SaveManager.HasKey("soundVolume"))
        {
            SaveManager.SetInt("soundVolume", 40);
        }
        float volume = (float)SaveManager.GetInt("soundVolume") / 100f;

        SoundManager.SetSoundVolume(volume);

        if (!SaveManager.HasKey("musicVolume"))
        {
            SaveManager.SetInt("musicVolume", 40);
        }
        float musicVolume = (float)SaveManager.GetInt("musicVolume") / 100f;

        MusicManager.SetMusicVolume(musicVolume);
    }

    public static int RotateIndex<T>(int index, int increment, List<T> list)
    {
        index += increment;
        index = (list.Count + (index % list.Count)) % list.Count;

        return index;
    }

    public static string AddIcons(string input)
    {
        input = input.Replace("{R}", "<sprite=\"icons\" name=refresh>");
        input = input.Replace("{C}", "<sprite=\"icons\" name=star>");
        input = input.Replace("{S}", "<sprite=\"icons\" name=shield>");
        input = input.Replace("{H}", "<sprite=\"icons\" name=hunt>");
        input = input.Replace("{P}", "<sprite=\"icons\" name=person>");
        input = input.Replace("{A}", "<sprite=\"icons\" name=animal>");
        input = input.Replace("{V}", "<sprite=\"icons\" name=vine>");

        input = input.Replace("{Male}", "<sprite=\"icons\" name=male>");
        input = input.Replace("{Female}", "<sprite=\"icons\" name=female>");

        return input;
    }

    public static List<Direction> MainDirections()
    {
        if (Board.GetBoardType() == BoardType.Hex)
            return GridHelpers.HexMainDirections();

        return GridHelpers.RectMainDirections();
    }

    public static Direction GetOppositeDirection(Direction d)
    {
        FlatHexPoint p = GridHelpers.GetFlatHexPointFromDirection(d);
        return GridHelpers.GetDirectionFromFlatHexPoint(p.Rotate180());
    }

    public static bool IsTransformInFrontOfCamera(Camera c, Transform t)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(t.position);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f) && viewportPoint.z > 0f;
    }

    public static bool TransformInFrontOfCamera(this Camera c, Transform t)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(t.position);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f);
    }

    public static bool PositionInFrontOfCamera(this Camera c, Vector3 v3)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(v3);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f);
    }

    public static float GetAngleFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return 0f;
            case Direction.Down:
                return 180f;
            case Direction.DownLeft:
                return 120f;
            case Direction.DownRight:
                return 240f;
            case Direction.UpLeft:
                return 60f;
            case Direction.UpRight:
                return 300f;
            case Direction.Right:
                return 270f;
            case Direction.Left:
                return 90f;
        }

        return 180f;
    }

    public static Vector3 GetVectorDirFromDirection(Direction dir)
    {
        FlatHexPoint p = GridHelpers.GetFlatHexPointFromDirection(dir);
        Vector3 pos1 = Board.Instance.map[new FlatHexPoint(0, 0)];
        Vector3 pos2 = Board.Instance.map[p];
        Vector3 vectorDir = pos2 - pos1;

        return vectorDir.normalized;
    }

    public static Quaternion GetRotationFromDirection(Direction dir)
    {
        return Quaternion.Euler(new Vector3(0f, 0f, GetAngleFromDirection(dir)));
    }

    internal static Cell GetCellInDirection(Cell cell, Direction d)
    {
        FlatHexPoint dir = GridHelpers.GetFlatHexPointFromDirection(d);
        FlatHexPoint other = cell.point + dir;
        if (other.HasCell())
            return other.ToCell();

        return null;
    }

    public static bool IsMainMenuScene()
    {
        return SceneManager.GetActiveScene().name.Contains("mainmenu");
    }

    public static bool IsGameScene()
    {
        return SceneManager.GetActiveScene().name.Contains("game");
    }

    public static Vector3 GetVectorPositionOffsetFromOrigin(FlatHexPoint p)
    {
        return (Board.Instance.map[p] - Board.Instance.map[new FlatHexPoint(0, 0)]);
    }



    public static GameObject MakeEffect(string resourceName, Transform parent, float duration = 1f, int sortingOrder = 20, float scaleMultiplier = 1f, Vector3 offset = default(Vector3))
    { 
        GameObject effect = Create(resourceName);
        effect.transform.parent = parent;
        effect.transform.localPosition = Vector3.zero;
        effect.gameObject.AddComponent<Temporary>().mLifespan = duration;

        effect.transform.localScale = Vector3.one * 100f * scaleMultiplier;
        effect.transform.localPosition += offset;

        foreach (ParticleSystemRenderer ps in effect.GetComponentsInChildren<ParticleSystemRenderer>())
        {
            ps.sortingOrder = sortingOrder;
        }

        return effect;
    }

    public static GameObject MakeEffect(string resourceName, Vector3 position, float duration = 1f, int sortingOrder = 20, float scaleMultiplier = 1f)
    {
        GameObject effect = Create(resourceName);
        effect.transform.localPosition = Vector3.zero;
        effect.gameObject.AddComponent<Temporary>().mLifespan = duration;

        effect.transform.localScale = Vector3.one * scaleMultiplier;
        effect.transform.position = position;

        foreach (ParticleSystemRenderer ps in effect.GetComponentsInChildren<ParticleSystemRenderer>())
        {
            ps.sortingOrder = sortingOrder;
        }

        return effect;
    }

    public static HealthBar MakeHealthBar(Transform parent, int maxHp, string prefabName = "HealthBarPip", Vector3 localPos = default(Vector3), Color color = default(Color))
    {
        HealthBar hb = CreateInstance<HealthBar>("HealthBar");
        Color c = color;

        hb.Init(maxHp, prefabName, c);
        hb.transform.parent = parent;
        hb.transform.localPosition = localPos;
        return hb;
    }

    public static ActorStats MakeActorStats(Transform parent, Vector3 localPos, Dictionary<ActorStatType, int> startStats)
    {
        ActorStats actorStats = CreateInstance<ActorStats>("ActorStats");
        actorStats.transform.parent = parent;
        actorStats.transform.localPosition = localPos;

        foreach(ActorStatType type in startStats.Keys)
            actorStats.AddStat(type, startStats[type]);

        return actorStats;
    }

    public static Ray vRay;
	public static RaycastHit hit;
	public static Collider RaycastFromMouse(Camera c)
	{
		vRay = c.ScreenPointToRay(Input.mousePosition);
		Physics.Raycast(vRay, out hit);
        Debug.DrawRay(vRay.origin, vRay.direction * 100f);

        return hit.collider;
	}

	public static Color AlphaAdjustedColor(Color c, float newAlpha) // 1 to 0
	{
		return new Color(c.r, c.g, c.b, newAlpha);
	}

	public static string LowercaseFirst(string s)
	{
		return Char.ToLowerInvariant(s[0]) + s.Substring(1);
	}

	public static Sprite GetSprite(string spriteName)
	{
		return Resources.Load<Sprite>(spriteName);
	}

	public static int mod(int x, int m)
	{
		return (x % m + m) % m;
	}

	public static Vector3 GetGroundMousePosition()
	{
		Plane ground = new Plane(Vector3.up, Vector3.zero);
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		float rayDistance;
		if (ground.Raycast(ray, out rayDistance))
		{
			return ray.GetPoint(rayDistance);
		}
		return new Vector3(0, 0, 0);
	}


	public static int AtLeast(int num, int atLeast)
	{
		return Mathf.Max(num, atLeast);
	}

	public static float AtLeast(float num, float atLeast)
	{
		return Mathf.Max(num, atLeast);
	}

	public static int AtMost(int num, int atMost)
	{
		return Mathf.Min(num, atMost);
	}

	public static float AtMost(float num, float atMost)
	{
		return Mathf.Min(num, atMost);
	}



	public static Vector3 CirclePoint(float percent, float radius, Vector3 center)
	{
		var i = percent;
		// get the angle for this step (in radians, not degrees)
		var angle = i * Mathf.PI * 2;
		// the X &amp; Y position for this angle are calculated using Sin &amp; Cos
		var x = Mathf.Sin(angle) * radius;
		var y = Mathf.Cos(angle) * radius;
		var pos = new Vector3(x, y, 0) + center;

		return pos;
	}

    static Dictionary<string, Color> colorDict = new Dictionary<string, Color>();
    public static Color HexToColor(string hex)
    {
        if (colorDict.ContainsKey(hex))
            return colorDict[hex];

        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = 255;
        if (hex.Length > 6)
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        colorDict.Add(hex, new Color32(r, g, b, a));

        return colorDict[hex];
    }

    public static Color Color255(int r = 255, int g = 255, int b = 255, int a = 255)
	{
		return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
	}

	public static GameObject Create(string resourceName)
	{
		return (GameObject.Instantiate(Resources.Load(resourceName))) as GameObject;
	}

	public static GameObject Create(string resourceName, Transform parent, bool zeroTransform = false)
	{
		GameObject go = Create(resourceName);
		go.transform.parent = parent;

        if (zeroTransform)
            go.transform.localPosition = Vector3.zero;

		return go;
	}

	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

    public static void ClearAndDestroyList<T>(this IList<T> list) where T : MonoBehaviour
	{
		foreach (T go in list)
		{
			if (go != null && go.gameObject != null)
				go.gameObject.DestroySelf();
		}

		list.Clear();
	}

    public static void ShuffleNoSeed<T>(this IList<T> list)
	{
		if (list == null)
			return;

		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = UnityEngine.Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	/// <summary>
	/// Pass in an amount in pennies, and will return the value in dollars with a currency symbol.
	/// Example: pass in 325, will return $3.25
	/// </summary>
	/// <param name="amount"></param>
	/// <returns></returns>
	/// 
	static string output;
	public static string FormatCurrency(int amount)
	{
		output = string.Format("${0}.{1}", (amount / 100), (amount % 100).ToString("00")).Replace("-", "");
		if (amount < 0)
		{
			output = "-" + output;
		}
		return output;
	}

	public static float SignedAngle(Vector3 a, Vector3 b)
	{
		var angle = Vector3.Angle(a, b); // calculate angle
										 // assume the sign of the cross product's Y component:
		return angle * Mathf.Sign(Vector3.Cross(a, b).y);
	}

	public static float SignedAngle(Vector2 a, Vector2 b)
	{
		float ang = Vector2.Angle(a, b);
		Vector3 cross = Vector3.Cross(a, b);

		if (cross.z > 0)
			ang = 360 - ang;

		return ang;
	}


	public static bool Between(this float f, float min, float max)
	{
		return f >= min && f <= max;
	}

	/// <summary>
	/// Returns a random int in the range [minValue, maxValue) (ie: exclusive on the upper bound)
	/// </summary>
	public static int RandomIntFromRange(int minValue, int maxValue)
	{
		if (maxValue <= minValue)
		{
			return minValue;
		}



		return rng.Next(minValue, maxValue);
	}

	public static int RandomIntFromRangeInclusive(int minValue, int maxValue)
	{
		return RandomIntFromRange(minValue, maxValue + 1);
	}

	/// <summary>
	/// Returns a random float in the range [minValue, maxValue].
	/// </summary>
	/// 
	///
	static float next;
	public static float RandomFloatFromRangeInclusive(float minValue, float maxValue)
	{
		next = (float)rng.NextDouble();
		return (next * (maxValue - minValue)) + minValue;
	}

	/// <summary>
	/// Same as ChanceRoll, except takes a number from [0f, 1f] instead of [0, 100]
	/// </summary>
	public static bool ChanceRoll(float chance = 0.5f)
	{
		if (chance <= 0f)
		{
			return false;
		}
		else if (chance >= 1f)
		{
			return true;
		}
		else
		{
			if (RandomFloatFromRangeInclusive(0f, 1f) >= chance)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

    public static T MakeGameObjectWithComponent<T>(Transform parent) where T : MonoBehaviour
    {
        GameObject go = new GameObject(typeof(T).FullName);
        go.transform.parent = parent;
        
        return go.AddComponent<T>();
    }

    public static T CreateInstance<T>(string prefabName) where T : MonoBehaviour
	{
		// get the prefab
		GameObject prefab = Resources.Load(prefabName) as GameObject;

		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	public static T CreateInstance<T>(string prefabName, Transform parent, bool zeroLocalPosition = false) where T : MonoBehaviour
	{
		// get the prefab
		GameObject prefab = Resources.Load(prefabName) as GameObject;

		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;
		instance.transform.SetParent(parent);

		// get the component
		T component = instance.GetComponent<T>();

		if (zeroLocalPosition)
			instance.transform.localPosition = Vector3.zero;

		return component;
	}

	public static T CreateInstance<T>(GameObject prefab) where T : MonoBehaviour
	{
		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	private static T CreateInstance<T>(GameObject prefab, Transform parent) where T : MonoBehaviour
	{
		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;
		instance.transform.SetParent(parent);

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	public static void StartWhileLoopCounter()
	{
		whileLoopCounter = 0;
	}

	public static bool WhileLoopTick(bool errorOnInsane = true)
	{
		if (Debug.isDebugBuild)
			errorOnInsane = false;

		whileLoopCounter++;
		if (whileLoopCounter >= WHILE_LOOP_INSANITY)
		{
			if (errorOnInsane)
			{
				Debug.LogError("Insanity in while loop");
			}
			else
			{
				Debug.LogWarning("Insanity in while loop");
			}
			return false;
		}
		return true;
	}

	public static void SetTimeScale(float t)
	{
		Time.timeScale = t;
	}

	//	public static void ShowPopup(string text, bool pause = false, Action callback = null)
	//	{
	//		Action modalCallback = null;
	//		if(pause)
	//		{
	//			modalCallback = TimeScaleOne;
	//		}
	//
	//		PopupDataStoryInfo data = new PopupDataStoryInfo("title", L.Get(text), callback, modalCallback);
	//
	//		//PopupStoryInfo popup = PopupManager.Instance.ShowPopupOnStack<PopupStoryInfo>(data);
	//		PopupManager.Instance.Enqueue(data);
	//
	//		//popup.transform.SetPositionX(700f); //HACK
	//
	//		//return popup.transform;
	//	}

	//	public static void ShowPopupTwoButtons(string text, string buttonOneText, string buttonTwoText, bool pause, Action callback = null, Action callbackTwo = null)
	//	{
	//		Action modalCallback = null;
	//		if (pause)
	//		{
	//			TimeScaleZero();
	//			modalCallback = TimeScaleOne;
	//		}
	//
	//		PopupDataStoryInfoTwoButtons data = new PopupDataStoryInfoTwoButtons(text, buttonOneText, buttonTwoText, callback, callbackTwo, modalCallback);
	//
	//		//PopupStoryInfoTwoButtons popup = PopupManager.Instance.ShowPopupOnStack<PopupStoryInfoTwoButtons>(data);
	//		PopupManager.Instance.Enqueue(data);
	//
	//		//popup.transform.SetPositionX(700f); //HACK
	//	}

	public static void ShakeTransform(Transform t, float force = 2f, float duration = 0.3f)
	{
		t.DOGoto(10f);
		t.DOShakePosition(duration, force).SetUpdate(true);
	}

	public static void TimeScaleOne()
	{
		Time.timeScale = 1f;
	}

	public static void TimeScaleZero()
	{
		Time.timeScale = 0f;
	}

	public static void ShuffleFisherYates<T>(List<T> list)
	{
		int n = list.Count;
		for (int i = 0; i < n; i++)
		{
			int r = i + RandomIntFromRange(0, n - i);
			T t = list[r];
			list[r] = list[i];
			list[i] = t;
		}
	}

	// NOTE: this code might seem more complicated than necessary, but using a StringBuilder is the most efficient way to handle this,
	// as it allocates very little memory compared to doing normal string concatenation and string.format calls, which are expensive and allocate a lot of memory
	public static string FormatTimeHoursMinutes(int timeInMinutes, bool regularTime = true)
	{
		// clear the StringBuilder
		s_stringBuilder.Length = 0;

		int hours = ((timeInMinutes % 1440) / 60); // keep in the range of [0,23]
		int minutes = timeInMinutes % 60;

		string postFix = "";

		if (hours == 12) // special case, if hours are exactly 12, is 12 PM
		{
			s_stringBuilder.Append("12");
			postFix = regularTime ? "PM" : "AM";
		}
		else if (hours == 0) // special case, if hours are exactly 0, is 12 AM
		{
			s_stringBuilder.Append("12");
			postFix = regularTime ? "AM" : "PM";
		}
		else if (hours > 11) // anything over 12 and we need to subtract 12 from the hours, and mark as PM
		{
			s_stringBuilder.Append((hours - 12).ToString());
			postFix = regularTime ? "PM" : "AM";
		}
		else // all other cases are AM
		{
			s_stringBuilder.Append(hours.ToString());
			postFix = regularTime ? "AM" : "PM";
		}

		s_stringBuilder.Append(":");

		if (minutes < 10)
		{
			s_stringBuilder.Append("0");
		}

		s_stringBuilder.Append(minutes.ToString());

		s_stringBuilder.Append(" ");
		s_stringBuilder.Append(postFix);

		return s_stringBuilder.ToString();
	}

    internal static string FormatTimeMinutesSeconds(int time)
    {
        return string.Format("{0}:{1}", Mathf.FloorToInt(time / 60), (time % 60).ToString("00"));
    }

    internal static bool IsOdd(int count)
    {
        return (count % 2) != 0;
    }

    public static T GetRandomEnumType<T>(bool removeNone = true) where T : System.Enum
    {
        return GetAllEnumTypes<T>(removeNone).RandomElement();
    }

    public static List<T> GetAllEnumTypes<T>(bool removeNone = false) where T : System.Enum
    {
        List<T> types = new List<T>();

        var values = Enum.GetValues(typeof(T));
        foreach (T type in values)
            types.Add(type);

        if (removeNone)
            types.Remove((T)values.GetValue(0));

        return types;
    }
}