﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonColorSetter : MonoBehaviour
{
    public Color baseColor = Color.grey;
    public tk2dUIUpDownHoverButton tk2DUIUpDownHoverButton;

    void Start()
    {
        tk2DUIUpDownHoverButton.uiItem.isHoverEnabled = true;

        SpriteRenderer sr = tk2DUIUpDownHoverButton.upStateGO.GetComponent<SpriteRenderer>();

        Init(baseColor);
    }

    public void Init(Color baseColor)
    {
        this.baseColor = baseColor;

        SetColor(tk2DUIUpDownHoverButton.upStateGO, baseColor);
        SetColor(tk2DUIUpDownHoverButton.hoverOverStateGO, baseColor + (Color.white * 0.1f));
        SetColor(tk2DUIUpDownHoverButton.downStateGO, baseColor + (Color.white * 0.2f));
    }

    void SetColor(GameObject go, Color color)
    {
        SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
        if (sr != null)
            sr.color = color;

        tk2dBaseSprite bs = go.GetComponent<tk2dBaseSprite>();
        if (bs != null)
            bs.color = color;
    }
}
