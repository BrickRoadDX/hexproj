﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;

public enum DisplayAllignment
{
    None = 0,
    Grid = 1, //moves from top left to bottom right in rows, like reading, no centering
    CenteredRowAndBelow = 2, //just moves down yOffset, uses cols
    CenteredRowsAndCols = 3, //tries to center yOffsets, uses Cols
    SpecialCenter = 4, //defines Cols automatically to make the smallest possible grid/most centered
    FixedWidth = 5, //xOffset defines the total distance between the first and last items
}

public enum RemoveAnim
{
    None = 0,
    Destroy = 1,
    UpAnimAndDestroy = 2,
    GoToCenterScreenAndDestroy = 3,
    MoveToTransformAndDestroy = 4,
}

/// <summary>
/// Add this monobehavior to the scene to group objects either in a grid or in a centered row
/// </summary>
public class ObjectDisplay : MonoBehaviour
{
    public Transform objectContainer;

    [HideInInspector]
    public List<MonoBehaviour> displayObjects = new List<MonoBehaviour>();

    [HideInInspector]
    public List<Vector3> storePositions = new List<Vector3>();

    public int cols = 3;
    public float xOffset = 5f;
    public float yOffset = 6f;

    public float timeToUpdatePositions = 0.3f;

    public DisplayAllignment displayType = DisplayAllignment.Grid;

    public bool sorted = false;

    public int Count { get { return NumItems(); } }

    private Dictionary<MonoBehaviour, Tweener> moveTweens = new Dictionary<MonoBehaviour, Tweener>();

    void CalculatePositions()
    {
        storePositions.Clear();

        float posX = 0f;
        float posY = 0f;

        switch (displayType)
        {
            case DisplayAllignment.Grid:
                for (int i = 0; i < 100; i++)
                {
                    posX = 0f;
                    for (int j = 0; j < cols; j++)
                    {
                        storePositions.Add(new Vector3(posX, posY, 0f));
                        posX += xOffset;
                    }
                    posY -= yOffset;
                }
                break;
            case DisplayAllignment.CenteredRowAndBelow: //uses defined # of cols
                int numRows = Mathf.CeilToInt((float)Count / cols);
                storePositions = GetCenteredGridPositions(numRows, cols, false);
                break;
            case DisplayAllignment.CenteredRowsAndCols:
                numRows = Mathf.CeilToInt((float)Count / cols);
                storePositions = GetCenteredGridPositions(numRows, cols);
                break;
            case DisplayAllignment.FixedWidth:
                float spacing = xOffset / (float)Count;
                List<float> xPositions = Helpers.GetElementPositions(Count, spacing);
                for(int i = 0; i < Count; i++)
                    storePositions.Add(new Vector3(xPositions[i], 0f, 0f));
                break;
            case DisplayAllignment.SpecialCenter: //creates square grid
                float halfXOffset = xOffset * 0.5f;
                float halfYOffset = yOffset * 0.5f;
                switch(Count)
                {
                    case 1:
                        storePositions.Add(Vector3.zero);
                        break;
                    case 2:
                        storePositions.Add(Vector3.zero + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfXOffset * Vector3.right));
                        break;
                    case 3:
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.right));
                        break;
                    case 4:
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up) + (halfXOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.right));
                        break;
                    case 5:
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up) + (xOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down) + (halfXOffset * Vector3.right));
                        break;
                    case 6:
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (halfXOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfXOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (halfXOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (halfXOffset * Vector3.right));
                        break;
                    case 7:
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.left));
                        storePositions.Add(Vector3.zero);
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.right));
                        break;
                    case 8:
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.up));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (halfYOffset * Vector3.down));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.right));
                        break;
                    case 9:
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.up) + (xOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.left));
                        storePositions.Add(Vector3.zero);
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.right));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.left));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down));
                        storePositions.Add(Vector3.zero + (yOffset * Vector3.down) + (xOffset * Vector3.right));
                        break;
                    default:
                        int numCols = Mathf.CeilToInt(Mathf.Sqrt((float)Count));
                        numRows = Mathf.CeilToInt((float)Count / (float)numCols);
                        storePositions = GetCenteredGridPositions(numRows, numCols);
                        break;
                }
                break;
        }
    }

    public List<Vector3> GetCenteredGridPositions(int numRows, int numCols, bool centerVertically = true)
    {
        int numRemaining = Count;
        List<Vector3> storePositions = new List<Vector3>();
        List<float> xPositions = Helpers.GetElementPositions(numCols, xOffset);
        List<float> yPositions = Helpers.GetElementPositions(numRows, -yOffset);
        float posY = 0f;
        for (int i = 0; i < numRows; i++)
        {
            if (numRemaining < numCols)
            {
                xPositions = Helpers.GetElementPositions(numRemaining, xOffset);
                numRemaining = 9999;
            }

            for (int j = 0; j < numCols; j++)
            {
                if (centerVertically)
                    posY = yPositions[i];

                if (xPositions.HasIndex(j))
                    storePositions.Add(new Vector3(xPositions[j], posY, 0f));

                numRemaining--;
            }

            posY -= yOffset;
        }
        return storePositions;
    }

    public void AnimationUnpaused()
    {
        UpdateObjectPositions(false);
    }

    public void UpdateObjectPositions(bool updateFast = false)
    {
        if (sorted)
            displayObjects.Sort((MonoBehaviour x, MonoBehaviour y) => ((Sortable)x).GetSortValue() - ((Sortable)y).GetSortValue()); //takes an int

        CalculatePositions();

        float timeToUse = timeToUpdatePositions;
        if (updateFast)
            timeToUse = 0f;

        foreach (Tweener tweener in moveTweens.Values)
            if(tweener != null)
                tweener.Kill();

        for (int i = 0; i < displayObjects.Count; i++)
        {
            displayObjects[i].transform.parent = objectContainer;

            bool paused = (displayObjects[i] is AnimationPausable) && ((AnimationPausable)displayObjects[i]).IsAnimationPaused();

            if(!paused)
            {
                if(timeToUse < 0.0001f)
                {
                    displayObjects[i].transform.localPosition = storePositions[i];
                }
                else
                {
                    moveTweens[displayObjects[i]] = displayObjects[i].transform.DOLocalMove(storePositions[i], timeToUse);
                }
            }
        }

        Helpers.UpdateSortingOrderObjects(displayObjects);
    }

    public void AddObject(MonoBehaviour go, bool updateFast = false)
    {
        if (!moveTweens.ContainsKey(go))
            moveTweens.Add(go, null);

        displayObjects.Add(go);
        UpdateObjectPositions(updateFast);
    }

    public List<T> GetCastedList<T>() where T : MonoBehaviour
    {
        List<T> list = new List<T>();
        foreach(T t in displayObjects)
        {
            list.Add(t);
        }

        return list;
    }

    public void RemoveObject(MonoBehaviour mono, RemoveAnim removeProcess = RemoveAnim.None, float animDuration = 1f, Vector3 goToPosition = default(Vector3))
    {
        if (!displayObjects.Contains(mono))
            return;

        if (moveTweens[mono] != null)
            moveTweens[mono].Kill();

        displayObjects.Remove(mono);

        switch(removeProcess)
        {
            case RemoveAnim.None:
                break;
            case RemoveAnim.Destroy:
                mono.gameObject.DestroySelf();
                break;
            case RemoveAnim.UpAnimAndDestroy:
                mono.transform.DOLocalMoveY(mono.transform.localPosition.y + 5f, animDuration);
                mono.gameObject.DestroySelf(animDuration);
                break;
            case RemoveAnim.GoToCenterScreenAndDestroy:
                Helpers.AdjustSortingOrders(mono.gameObject, 50 * GetNextCenterAnimSortingIndex());
                mono.transform.DOMove(Helpers.GetUICameraCenterPosition(), animDuration * 0.5f);
                mono.transform.DOScale(mono.transform.localScale.x * 2f, animDuration * 0.5f);
                mono.CallActionDelayed(delegate ()
                {
                    mono.transform.DOShakeRotation(0.5f, new Vector3(0f, 0f, 5f), 45);
                }, animDuration * 0.5f);
                mono.gameObject.DestroySelf(animDuration);
                break;
            case RemoveAnim.MoveToTransformAndDestroy:
                mono.transform.DOMove(goToPosition, animDuration);
                mono.gameObject.DestroySelf(animDuration);
                break;
        }

        UpdateObjectPositions();
    }

    internal bool Contains(MonoBehaviour mono)
    {
        return displayObjects.Contains(mono);
    }

    int currIndex = 1;
    public int GetNextCenterAnimSortingIndex()
    {
        currIndex++;
        if (currIndex > 10)
            currIndex = 0;
        return currIndex;
    }

    internal void ClearAndDestroyAll()
    {
        foreach (MonoBehaviour go in displayObjects)
        {
            if (go != null)
                go.gameObject.DestroySelf();
        }
        displayObjects.Clear();
    }

    public int NumItems()
    {
        return displayObjects.Count;
    }

    internal void RemoveOldestObject(RemoveAnim anim)
    {
        if(displayObjects.Count > 0)
            RemoveObject(displayObjects[0], anim);
    }

    internal void RemoveRandomObject()
    {
        MonoBehaviour obj = displayObjects.RandomElement();
        RemoveObject(obj);
    }
}

public interface KillTweens
{
    void KillTweens();
}

public interface Sortable 
{
    int GetSortValue();
}

public interface AnimationPausable
{
    bool IsAnimationPaused();
}