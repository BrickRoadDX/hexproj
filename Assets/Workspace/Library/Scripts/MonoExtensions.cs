﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Gamelogic.Grids;

public static class MonoExtensions
{
    public static List<T> GetFirst<T>(this List<T> source, int count)
    {
        return source.GetRange(0, count);
    }

    public static List<T> GetLast<T>(this List<T> source, int count)
    {
        return source.GetRange(source.Count - (count), count);
    }

    public static T GetNextElement<T>(this List<T> source, T current)
    {
        if(source.Contains(current))
        {
            int x = source.IndexOf(current);
            x++;
            x = Helpers.mod(x, source.Count);
            return source[x];
        }

        return source[0];
    }

    public static T GetPrevElement<T>(this List<T> source, T current)
    {
        if (source.Contains(current))
        {
            int x = source.IndexOf(current);
            x--;
            x = Helpers.mod(x, source.Count);
            return source[x];
        }

        return source[0];
    }

    public static T PopFirst<T>(this List<T> source, bool shuffle = false)
    {
        if (shuffle)
            source.Shuffle();
        T item = source[0];
        source.RemoveAt(0);
        return item;
    }

    public static T RandomElement<T>(this List<T> source)
    {
        return source[Helpers.RandomIntFromRange(0, source.Count)];
    }

    public static T RandomElement<T>(this List<T> source, bool remove = false)
    {
        T element = RandomElement(source);
        if (remove)
            source.Remove(element);

        return element;
    }

    public static bool HasElement<T>(this List<T> source, int index)
    {
        return source.Count > index;
    }

    public static bool HasIndex<T>(this List<T> source, int index)
    {
        return source.Count > index;
    }

    public static string ListString<T>(this List<T> source)
    {
        string listString = "";
        foreach (T t in source)
            listString += t.ToString() + ",";
        listString = listString.Trim(',');
        return listString;
    }

    public static int RandomIndex<T>(this List<T> source)
    {
        return Helpers.RandomIntFromRange(0, source.Count);
    }

    public static Vector3 RandomShifted(this Vector3 v3, float offset = 50f)
	{
		return new Vector3(v3.x + Helpers.RandomFloatFromRangeInclusive(-offset, +offset), v3.y + Helpers.RandomFloatFromRangeInclusive(-offset, +offset), v3.z);
	}


	public static void TryStopCoroutine(this MonoBehaviour mono, IEnumerator coroutine)
	{
		if (coroutine != null)
		{
			mono.StopCoroutine(coroutine);
		}
	}

	// Extension.
	public static void CallAction(this MonoBehaviour mono, Action action)
	{
		callAction (mono, action);
	}
	
	static void callAction(MonoBehaviour mono, Action method)
	{
		if (mono.enabled
		    && method != null)
		{
			method();
		}
	}

	// Extension
	public static void CallActionDelayed(this MonoBehaviour mono, Action action)
	{
		mono.StartCoroutine (actionDelayed (mono, action, 0));
	}
	
	// Extension
	public static void CallActionDelayed(this MonoBehaviour mono, Action action, float delay)
	{
		mono.StartCoroutine (actionDelayed (mono, action, delay));
	}
	
	static IEnumerator actionDelayed(MonoBehaviour mono, Action method, float delay)
	{
		// Wait.
		if (delay == 0)
		{
			yield return null;
		}
		else
		{
			yield return new WaitForSeconds (delay);
		}
		
		// Call.
		callAction (mono, method);
	}
	
	public static void StopDelayedActions(this MonoBehaviour mono)
	{
		mono.StopAllCoroutines();
	}
	
	// Extension
	public static void CallActionRepeat(this MonoBehaviour mono, Action method, float delay)
	{
		mono.StartCoroutine (actionRepeat (mono, method, delay));
	}
	
	static IEnumerator actionRepeat(MonoBehaviour mono, Action method, float delay)
	{
		// Call.
		callAction(mono, method);
		
		// Wait.
		yield return new WaitForSeconds (delay);
		
		// Re-call.
		mono.StartCoroutine (actionRepeat (mono, method, delay));
	}
	
	// Extension
	public static Renderer[] GetRenderersInChildren(this MonoBehaviour mono)
	{
		return mono.GetComponentsInChildren<Renderer> (true) as Renderer[];
	}
	
	// Extension
	public static List<Material> GetMaterialsInChildren(this MonoBehaviour mono)
	{
		List<Material> mAllMaterials = new List<Material>();
		
		// Iterate renderers.
		foreach(var render in mono.GetRenderersInChildren ())
		{
			// Add materials to list.
			foreach(var mat in render.materials)
			{
				mAllMaterials.Add (mat);
			}
		}
		
		return mAllMaterials;
	}

    public static GameLayer GetGameLayer(this GameObject go)
    {
        if (go.layer == 5)
            return GameLayer.UI;

        return GameLayer.Default;
    }

    public static FlatHexPoint GetHoveredPoint(this GameObject go)
    {
        if (go.layer == 5)
            return Board.Instance.map[Helpers.TranslateFromTk2dWorldtoMainWorld(go.transform.position)];

        return Board.Instance.map[go.transform.position];
    }

    public static void SetLayerRecursively(this GameObject obj, GameLayer layer)
    {
        switch(layer)
        {
            case GameLayer.Default:
                SetLayerRecursively(obj, 0);
                return;
            case GameLayer.UI:
                SetLayerRecursively(obj, 5);
                return;
        }

        Debug.LogError(string.Format("We don't have this layer!, {0}", layer.ToString()));
    }

    public static void SetLayerRecursively(this GameObject obj, int layer) {
		obj.layer = layer;
		
		foreach (Transform child in obj.transform) {
			child.gameObject.SetLayerRecursively(layer);
		}
	}


	public static void SetLayerRecursively(this GameObject obj, string layer)
	{
		int layerInt = LayerMask.NameToLayer(layer);
		obj.SetLayerRecursively(layerInt);
	}

	public static void DestroySelf(this GameObject obj, float delay = -1f)
	{
        if(obj != null && delay <= 0f)
		    GameObject.Destroy(obj.gameObject);

        if (obj != null && delay > 0f)
            obj.gameObject.AddComponent<Temporary>().mLifespan = delay;
	}

    public static void DestroySelf(this MonoBehaviour obj)
    {
        if (obj != null)
            GameObject.Destroy(obj);
    }

    public static void DestroyChildren(this GameObject obj)
    {
        foreach(Transform t in obj.transform)
        {
            t.gameObject.DestroySelf();
        }
    }

    public static GameObject Instantiate(GameObject prefab) 
	{
		return (GameObject.Instantiate(prefab) as GameObject);
	}

	public static void SetAlpha (this Material material, float value) 
	{ 
		Color color = material.color; color.a = value; material.color = color; 
	}

    public static bool HasCell(this FlatHexPoint point)
    {
        return Board.Instance.grid.Contains(point);
    }

    public static Cell ToCell(this FlatHexPoint point)
    {
        if (Board.Instance.grid.Contains(point))
            return Board.Instance.grid[point];

        return null;
    }

    public static Vector3 PointPos(this FlatHexPoint point)
    {
        return Board.Instance.map[point];
    }

    public static List<Cell> ToCells(this List<FlatHexPoint> points)
    {
        List<Cell> list = new List<Cell>();

        foreach (FlatHexPoint p in points)
        {
            if (Board.Instance.grid.Contains(p))
            {
                list.Add(Board.Instance.grid[p]);
            }
        }
        return list;
    }

    public static List<FlatHexPoint> ToPoints(this List<Cell> cells)
    {
        List<FlatHexPoint> list = new List<FlatHexPoint>();

        foreach (Cell c in cells)
        {
            list.Add(c.point);
        }
        return list;
    }
}