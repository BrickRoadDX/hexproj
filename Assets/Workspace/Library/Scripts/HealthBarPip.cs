﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarPip : MonoBehaviour {

    public SpriteRenderer healthBarOutline;
    public SpriteRenderer healthBarFill;
    public float X_OFFSET = 5f;

}
