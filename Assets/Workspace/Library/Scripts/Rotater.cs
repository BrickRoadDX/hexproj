﻿using UnityEngine;
using System.Collections;

public class Rotater : MonoBehaviour {
	public float xRotationSpeed = 0;
	public float yRotationSpeed = 0;
	public float zRotationSpeed = 0;

	public bool randomStartOffset = false;

	void Start()
	{
		if(randomStartOffset)
		{
			float randomOffset = Random.Range (0f, 360f);

			if(xRotationSpeed != 0)
			{
				gameObject.transform.Rotate(new Vector3(randomOffset, 0f, 0f));
			}

			if(yRotationSpeed != 0)
			{
				gameObject.transform.Rotate(new Vector3(0f, randomOffset, 0f));
			}

			if(zRotationSpeed != 0)
			{
				gameObject.transform.Rotate(new Vector3(0f, 0f, randomOffset));
			}
		}	
	}

	void Update()
	{
		gameObject.transform.rotation *= Quaternion.Euler (xRotationSpeed * Time.deltaTime,yRotationSpeed * Time.deltaTime,zRotationSpeed * Time.deltaTime);
	}

}
