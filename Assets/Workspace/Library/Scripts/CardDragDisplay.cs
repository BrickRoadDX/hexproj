﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardDragDisplay : DragDisplay
{
    public TextMeshPro cardCountText;

    public override void Init()
    {
        base.Init();
        cardCountText.text = "";
    }

    public override bool DraggingDroppableObject()
    {
        return CardManager.Instance.dragCard != null;
    }

    public override void Drop(Draggable toDrop)
    {
        base.Drop(toDrop);

        cardCountText.text = "Cards: " + displayObjects.Count.ToString();
    }
}
