﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;


// 1) add pilemarket to gameobject in scene
// 2) setup its object display
// 3) add reference to game or somewhere
// 4) call init, or addnewcard

public class PileMarket : MonoBehaviour
{
    public ObjectDisplay objectDisplay;
    public Dictionary<Card, NumberText> pileCounts = new Dictionary<Card, NumberText>();
    public Dictionary<Card, NumberText> cardCosts = new Dictionary<Card, NumberText>();

    public bool cardPiles = true;
    public int pileStartSize = 10; //-1 for random test values

    static List<PileMarket> pileMarkets = new List<PileMarket>();

    public void Init(int numCards = 10)
    {
        objectDisplay.ClearAndDestroyAll();
        pileCounts.Clear();
        cardCosts.Clear();

        FillMarket(numCards);

        pileMarkets.Add(this);
    }

    public void FillMarket(int numCards)
    {
        int currentCount = objectDisplay.Count;
        for (int i = 0; i < numCards - currentCount; i++)
        {
            int pileSize = pileStartSize;
            if (pileSize < 0)
                pileSize = Helpers.RandomIntFromRangeInclusive(0, 3);

            if (!cardPiles)
                pileSize = 1;

            AddNewCard(CardType.None, pileSize, false);
        }

        UpdateCardPiles();
    }

    public void AddNewCard(CardType cardType = CardType.None, int cardsInPile = 1, bool updatePileCounts = true)
    {
        Card card = CardManager.Instance.CreateCard(cardType);
        objectDisplay.AddObject(card);

        TextMeshObject pileCount = Helpers.MakeTextMesh("", card.transform, card.mainText.sortingOrder + 2);
        pileCounts.Add(card, new NumberText(pileCount, cardsInPile));
        pileCount.transform.localPosition = new Vector3(1.3f, -3f, 0f);
        if (!cardPiles)
            pileCount.gameObject.SetActive(false);

        TextMeshObject cardCost = Helpers.MakeTextMesh("", card.transform, card.mainText.sortingOrder + 2, Vector3.zero, ResourceDef.GetResourceColor(ResourceType.Money));
        cardCosts.Add(card, new NumberText(cardCost, CardDef.GetCost(card.cardType).Get(ResourceType.Money)));
        cardCost.transform.localPosition = new Vector3(0f, 3f, 0f);

        card.GetComponent<HoverZoom>().Init(1.05f);

        if(updatePileCounts)
            UpdateCardPiles();
    }

    public void HandleClicked(Card card)
    {
        if (!HaveEnough(card))
        {
            Helpers.ShowReminderText("None of that card left!");
            return;
        }

        if(ResourceManager.Instance.CanAfford(CardDef.GetCost(card.cardType)))
        {
            Debug.Log(card.name);
            ResourceManager.Instance.Spend(ResourceType.Money, cardCosts[card].number);
            CardManager.Instance.AddNewCardToDiscard(card.cardType);
            DecrementPile(card);
        }
        else
        {
            Helpers.ShowReminderText("Can't afford that card!");
            Helpers.ShakeTransform(card.transform, 0.1f);
        }
    }

    private void DecrementPile(Card card)
    {
        if(cardPiles)
        {
            pileCounts[card].SetNumber(pileCounts[card].number - 1);
            UpdateCardPiles();
        }
        else
        {
            Vector3 pos = CardManager.Instance.cardPiles.First(x => x.cardPileType == CardPileType.Discard).transform.position;
            objectDisplay.RemoveObject(card, RemoveAnim.MoveToTransformAndDestroy, 1f, pos);
            card.transform.DOScale(0.3f, 1f);
        }
    }

    void UpdateCardPiles()
    {
        foreach(Card card in pileCounts.Keys)
        {
            if (pileCounts[card].number < 1)
                card.bgSprite.color = Color.grey;
        }
    }

    private bool HaveEnough(Card card)
    {
        return pileCounts[card].number > 0;
    }

    internal bool HasCard(Card card)
    {
        return objectDisplay.Contains(card);
    }

    public static bool CardIsInMarket(Card card)
    {
        foreach(PileMarket market in pileMarkets)
        {
            if (market.HasCard(card))
                return true;
        }
        return false;
    }

    public static void FindCardAndClick(Card card)
    {
        foreach (PileMarket market in pileMarkets)
        {
            if (market.HasCard(card))
                market.HandleClicked(card);
        }
    }
}

public class NumberText
{
    TextMeshObject textMeshPro;
    public int number;

    public NumberText(TextMeshObject tmp, int number)
    {
        textMeshPro = tmp;
        this.number = number;
        UpdateText();
    }

    public void SetNumber(int newNum)
    {
        number = newNum;
        UpdateText();
    }

    void UpdateText()
    {
        textMeshPro.textMesh.text = number.ToString();
    }

    public void SetText(string text)
    {
        textMeshPro.textMesh.text = text;
    }
}
