﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum CardPileType
{
    None  = 0,
    Discard = 1,
    Deck = 2,
}

//Add to gameobject in scene
//add uiitem/boxcollider/hoverzoom
//if set to discard pile will track that pile from cardmanager
//only currently works for deck and discard piles

public class CardPile : MonoBehaviour
{
    public tk2dUIItem uiItem;
    public CardPileType cardPileType = CardPileType.Discard;
    public TextMeshPro numCardsText;
    public ObjectDisplay cardDisplay;

    public SpriteRenderer fadeSprite;

    bool displayIsUp = false;

    private void Awake()
    {
        displayIsUp = false;
        CardManager.Instance.cardPiles.Add(this);
        fadeSprite.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        uiItem.OnClick += HandleClicked;
    }

    private void OnDisable()
    {
        uiItem.OnClick -= HandleClicked;
    }

    private void HandleClicked()
    {
        if (displayIsUp)
            return;

        List<CardID> cardList = new List<CardID>(GetCardList());

        if(cardList.Count < 1)
        {
            Helpers.ShowReminderText(string.Format("No cards in {0}", GetPileName()));
            return;
        }

        if (cardPileType == CardPileType.Deck)
            cardList.Shuffle();

        foreach(CardID card in cardList)
        {
            CardBase co = CardManager.Instance.CreateCard(card);
            cardDisplay.AddObject(co);
        }

        foreach(CardBase co in cardDisplay.GetCastedList<CardBase>())
        {
            Helpers.AdjustSortingOrders(co.gameObject, 300);
        }

        SetDisplayUp(true);
    }

    string GetPileName()
    {
        switch(cardPileType)
        {
            case CardPileType.None:
                return "Card Pile";
        }
        return cardPileType.ToString();
    }

    void SetDisplayNotUp()
    {
        SetDisplayUp(false);
    }

    void SetDisplayUp(bool displayUp)
    {
        fadeSprite.gameObject.SetActive(displayUp);
        displayIsUp = displayUp;
    }

    void ClearCardDisplay()
    {
        cardDisplay.ClearAndDestroyAll();
    }

    private void Update()
    {
        if(displayIsUp)
        {
            if(Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
            {
                ClearCardDisplay();
                this.CallActionDelayed(SetDisplayNotUp);
            }
        }
    }

    public List<CardID> GetCardList()
    {
        switch(cardPileType)
        {
            case CardPileType.Discard:
                return CardManager.Instance.discard;
            case CardPileType.Deck:
                return CardManager.Instance.playerDeck;
        }

        return new List<CardID>();
    }

    public void UpdateCardPile()
    {
        numCardsText.text = GetCardList().Count.ToString();
    }

    internal bool DisplayUp()
    {
        return displayIsUp;
    }
}
