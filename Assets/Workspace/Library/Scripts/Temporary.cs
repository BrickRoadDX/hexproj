﻿using UnityEngine;
using System.Collections;

public class Temporary : MonoBehaviour {
	
	public float mLifespan = 1f;
	
	void Update()
	{
		mLifespan -= Time.deltaTime;
		if(mLifespan <= 0f)
		{
			Destroy (this.gameObject);
		}
	}
}
