﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainedResourceObject : MonoBehaviour {

    public SpriteRenderer sprite;
    public Temporary temporary;
    public ResourceType resourceType;

    public void OnDestroy()
    {
        if (Helpers.isShuttingDown)
            return;

        if(resourceType != ResourceType.None)
            ResourceManager.Instance.ProcessNextResource(resourceType);
    }
}
