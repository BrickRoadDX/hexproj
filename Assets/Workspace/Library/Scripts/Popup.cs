﻿using UnityEngine;
using System.Collections;
using TMPro;

public class Popup : MonoBehaviour {
    public BoxCollider blockingCollider;

    public tk2dUIItem dragUiItem;

    public bool draggable = true;

    public tk2dUIItem showHideButton;
    public TextMeshPro showHideButtonText;

    protected virtual string soundString { get { return "draw"; } }

    public virtual void Init()
    {
        if (PopupManager.HasPrivateInstance())
            PopupManager.Instance.popupBlocker.enabled = true;

        SoundManager.Instance.PlaySound(soundString);
    }

    Vector3 lastMousePos = Vector3.zero;
    public virtual void Update()
    {
        if (draggable && dragUiItem != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePos = Game.Instance.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);
            }

            if (Input.GetMouseButton(0) && (tk2dUIManager.Instance.pressedUIItem == dragUiItem || tk2dUIManager.Instance.pressedUIItem == showHideButton))
            {
                this.transform.position = this.transform.position - (lastMousePos - Game.Instance.tk2dCamera.ScreenToWorldPoint(Input.mousePosition));
                KeepOnScreen();
                lastMousePos = Game.Instance.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);
            }
        }
    }

    void KeepOnScreen()
    {
        transform.SetPositionX(Mathf.Clamp(transform.position.x, -130f, -72f));
        transform.SetPositionY(Mathf.Clamp(transform.position.y, -16f, 16f));
    }


    protected virtual void OnEnable()
    {
        if (showHideButton != null)
            showHideButton.OnClick += ShowHideClicked;
    }

    protected virtual void OnDisable()
    {
        if (showHideButton != null)
            showHideButton.OnClick -= ShowHideClicked;
    }

    void ShowHideClicked()
    {
        dragUiItem.gameObject.SetActive(!dragUiItem.gameObject.activeSelf);
        showHideButtonText.text = dragUiItem.gameObject.activeSelf ? "Hide" : "Show";
    }
}
