﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;


// 1) add flowmarket to gameobject in scene
// 2) setup its object display
// 3) add reference to game or somewhere
// 4) call init, rotate, hascard, clicked

public class FlowMarket : MonoBehaviour
{
    public ObjectDisplay objectDisplay;
    public Dictionary<Card, NumberText> cardCosts = new Dictionary<Card, NumberText>();

    static List<FlowMarket> flowMarkets = new List<FlowMarket>();

    public int marketSize = 5;

    public void Init(int marketSize = 5)
    {
        objectDisplay.ClearAndDestroyAll();
        cardCosts.Clear();

        this.marketSize = marketSize;
        FillMarket(marketSize);

        flowMarkets.Add(this);
    }

    public void FillMarket(int numCards)
    {
        int currentCount = objectDisplay.Count;
        for (int i = 0; i < numCards - currentCount; i++)
            AddNewCard(CardType.None);

        UpdateCardCosts();
    }

    public void AddNewCard(CardType cardType = CardType.None)
    {
        Card card = CardManager.Instance.CreateCard(cardType);
        objectDisplay.AddObject(card);

        TextMeshObject cardCost = Helpers.MakeTextMesh("", card.transform, card.mainText.sortingOrder + 2, Vector3.zero, ResourceDef.GetResourceColor(ResourceType.Money));
        cardCosts.Add(card, new NumberText(cardCost, GetMarketPosition(card)));
        cardCost.transform.localPosition = new Vector3(0f, 3f, 0f);

        card.GetComponent<HoverZoom>().Init(1.05f);
    }

    void UpdateCardCosts()
    {
        foreach(Card card in cardCosts.Keys)
        {
            cardCosts[card].SetNumber(GetMarketPosition(card) + 1);
        }
    }

    public int GetMarketPosition(Card card)
    {
        return objectDisplay.displayObjects.IndexOf(card);
    }

    public void HandleClicked(Card card)
    {
        if (ResourceManager.Instance.CanAfford(CardDef.GetCost(card.cardType)))
        {
            Debug.Log(card.name);
            ResourceManager.Instance.Spend(ResourceType.Money, cardCosts[card].number);
            CardManager.Instance.AddNewCardToDiscard(card.cardType);
            RemoveAndFill(card);
        }
        else
        {
            Helpers.ShowReminderText("Can't afford that card!");
            Helpers.ShakeTransform(card.transform, 0.1f);
        }
    }

    private void RemoveAndFill(Card card, bool purchased = true)
    {
        cardCosts[card].SetText("");
        cardCosts.Remove(card);

        Helpers.AdjustSortingOrders(card.gameObject, -1000);
        if(purchased)
        {
            Vector3 pos = CardManager.Instance.cardPiles.First(x => x.cardPileType == CardPileType.Discard).transform.position;
            objectDisplay.RemoveObject(card, RemoveAnim.MoveToTransformAndDestroy, 1f, pos);
            card.transform.DOScale(0.3f, 1f);
        }
        else
        {
            objectDisplay.RemoveObject(card, RemoveAnim.UpAnimAndDestroy, 1f);
        }

        FillMarket(marketSize);
    }

    internal bool HasCard(Card card)
    {
        return objectDisplay.Contains(card);
    }

    public static bool CardIsInMarket(Card card)
    {
        foreach (FlowMarket market in flowMarkets)
        {
            if (market.HasCard(card))
                return true;
        }
        return false;
    }

    public static void FindCardAndClick(Card card)
    {
        foreach (FlowMarket market in flowMarkets)
        {
            if (market.HasCard(card))
                market.HandleClicked(card);
        }
    }

    internal void RotateCards(int num)
    {
        for(int i = 0; i < num; i++)
        {
            if (objectDisplay.Count > 0)
                RemoveAndFill(objectDisplay.GetCastedList<Card>()[0], false);
        }
    }
}
