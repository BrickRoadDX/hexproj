﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverZoom : MonoBehaviour
{
    public tk2dUIItem uiItem;

    Tweener zoomTweener;
    Tweener verticalTweener;

    public float scaleFactor = 1.2f;
    public float scaleDuration = 0.3f;
    public Ease ease = Ease.OutQuad;
    public bool fasterHoverIn = true;
    public bool twoD = true;

    private bool hoverEventsEnabled = true;

    Vector3 startScale = Vector3.one;
    Vector3 zoomedInScale = Vector3.one;

    private void Awake()
    {
        startScale = this.transform.localScale;
        Init();
    }

    public void Init(float adjustedStartScale = 1f)
    {
        if (zoomTweener != null)
            zoomTweener.Kill();

        Vector3 zoomedOutScale = startScale * adjustedStartScale;
        this.transform.localScale = zoomedOutScale;
        zoomedInScale = zoomedOutScale * scaleFactor;
        if (twoD)
            zoomedInScale.z = zoomedOutScale.z;

        SetupTweener();
    }

    void SetupTweener()
    {
        zoomTweener = this.transform.DOScale(zoomedInScale, scaleDuration).SetEase(ease).SetAutoKill(false).Pause();
    }

    bool isHovered = false;
    private void Update()
    {
        //Debug.Log(Helpers.HoveredUiItem());

        if (!hoverEventsEnabled)
            return;

        if(!isHovered)
        {
            if (Helpers.IsHoveredOrHeld(uiItem))
            {
                isHovered = true;
                HandleHover();
            }
        }
        else
        {
            if (!Helpers.IsHoveredOrHeld(uiItem))
            {
                isHovered = false;
                HandleHoverOut();
            }
        }
    }

    private void HandleHover()
    {
        if(fasterHoverIn)
            zoomTweener.timeScale = 4f;

        zoomTweener.PlayForward();
    }

    private void HandleHoverOut()
    {
        zoomTweener.timeScale = 1f;
        zoomTweener.PlayBackwards();
    }

    public void Disable()
    {
        SetHoverEventsEnabled(false);
    }

    public void SetHoverEventsEnabled(bool enabled)
    {
        hoverEventsEnabled = enabled;
        if(!enabled)
            HandleHoverOut();     
    }
}
