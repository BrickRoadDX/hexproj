﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using System.Linq;
using Gamelogic.Grids;

public class Die : MonoBehaviour, Sortable, Draggable
{
    public static int nextDieId = 1;
    [HideInInspector]
    public int dieId = 0;

    public tk2dUIItem uiItem;

    [HideInInspector]
	public DiceType diceType;

    [HideInInspector]
    public int dieFaceIndex = 0;

    [HideInInspector]
	public DieFace dieFaceType = DieFace.None;

    [HideInInspector]
	public bool selected = false;

	public SpriteRenderer selectedSprite;
    public SpriteRenderer dieFaceRenderer;
	public SpriteRenderer borderRenderer;
	public SpriteRenderer bgRenderer;
    public RollingTransform rollingAnimationTransform;
    public HoverZoom hoverZoom;
    public BoxCollider boxCollider;

    [HideInInspector]
	public bool justRolled = false;

    [HideInInspector]
	public bool rollingAnimation = true;


    [HideInInspector]
	private float nextFaceCounter = 0f;

	public DieTooltipShower dieTooltipShower;

    public Transform plusMinusButtonsContainer;
    public tk2dUIItem plusButton;
    public tk2dUIItem minusButton;

    [HideInInspector]
    public DieFace previousDieFace = DieFace.None;

    public List<Renderer> dontAdjustRenderers;

    [HideInInspector]
    public bool allowUIHover = true;

    public void Init(DiceType type)
    {
        rollingAnimationTransform.die = this;

        dieId = Die.nextDieId;
        nextDieId++;

        this.diceType = type;

        if (dieTooltipShower != null)
            dieTooltipShower.SetDie(this);

        dieFaceRenderer.color = Color.black;
        //borderRenderer.color = DieDef.GetDieColor(type);
        borderRenderer.color = Color.clear;
        //bgRenderer.color = DieDef.GetDieColor(type);

        currentFaces = new List<DieFace>(DieDef.GetFacesForDiceType(type));

        plusMinusButtonsContainer.gameObject.SetActive(false);
        SetSelected(false);
    }

    private void OnEnable()
    {
        plusButton.OnClick += NextDieFace;
        minusButton.OnClick += PrevDieFace;
        uiItem.OnClick += HandleClicked;
    }

    private void OnDisable()
    {
        plusButton.OnClick -= NextDieFace;
        minusButton.OnClick -= PrevDieFace;
        uiItem.OnClick -= HandleClicked;
    }

    void Start()
	{
        SetSelected(false);
	}

	public void SetDieFace(DieFace newType, bool stopRollingAnimation = false)
	{
        if (stopRollingAnimation)
            rollingAnimation = false;

        dieFaceType = newType;
        SetSprite(dieFaceType);
	}

    void Update()
	{
		if(rollingAnimation == true)
		{
			nextFaceCounter += Time.deltaTime;
			timeSinceRolled += Time.deltaTime;
			if (nextFaceCounter > 0.1f)
			{
                if(timeSinceRolled < (ROLL_TIME * 0.75f))
				{
					nextFaceCounter -= 0.1f;
					List<DieFace> faces = GetFaces();

					int index = Helpers.RandomIntFromRange(0, faces.Count);

					SetSprite(faces[index]);
				}
				else
				{
					SetSprite(dieFaceType);
				}
			}
		}
	}

    internal void ToggleSelection()
    {
        SetSelected(!selected);
    }

    void HandleClicked()
	{
        if (PopupManager.Instance.IsShowingPopup() || rollingAnimation || Game.Instance.IsGameOver())
			return;

        if (DiceManager.Instance.refreshBox.Contains(this))
            return;

        if(DiceManager.Instance.diceBox.Contains(this))
            InputManager.Instance.HandleDieClicked(this);
	}

    public static DieFace GetDieFaceForValue(DiceType type, int value)
    {
        foreach(DieFace face in DieDef.GetFacesForDiceType(type))
        {
            if (DieDef.DiceFaceValue(face) == value)
                return face;
        }

        return (DieFace)value;
    }

    public void NextDieFace()
    {
        this.previousDieFace = this.dieFaceType;

        SoundManager.Instance.PlaySound("pop");

        if (GetFaces().Contains(dieFaceType) && GetFaces()[dieFaceIndex] != dieFaceType)
            dieFaceIndex = GetFaces().IndexOf(dieFaceType);

        dieFaceIndex++;

        if (dieFaceIndex > GetFaces().Count - 1)
            dieFaceIndex = 0;

        SetDieFace(GetFaces()[dieFaceIndex]);

        DiceManager.Instance.lastChangedDie = this;
        DiceManager.Instance.lastDiscardedDie = null;
    }

    public void PrevDieFace()
    {
        this.previousDieFace = this.dieFaceType;

        SoundManager.Instance.PlaySound("pop");

        if (GetFaces().Contains(dieFaceType) && GetFaces()[dieFaceIndex] != dieFaceType)
            dieFaceIndex = GetFaces().IndexOf(dieFaceType);

        dieFaceIndex--;

        if (dieFaceIndex < 0)
            dieFaceIndex = GetFaces().Count - 1;

        SetDieFace(GetFaces()[dieFaceIndex]);

        DiceManager.Instance.lastChangedDie = this;
        DiceManager.Instance.lastDiscardedDie = null;
    }

	public void SetSelected(bool isSelected)
	{
		bool wasSelected = selected;

		selected = isSelected;

        selectedSprite.gameObject.SetActive(isSelected);

		if (!wasSelected && selected)
		{
            if (DiceManager.Instance.soundDelay <= 0f)
            {
                SoundManager.Instance.PlaySound("splish");
                DiceManager.Instance.soundDelay = 0.1f;
            }
		}

		if (!selected && wasSelected)
		{
            if (DiceManager.Instance.soundDelay <= 0f)
            {
                SoundManager.Instance.PlaySound("tiny splish");
                DiceManager.Instance.soundDelay = 0.1f;
            }
		}

        DiceManager.Instance.UpdatePlusMinusButtons();
    }

    public List<DieFace> currentFaces = new List<DieFace>();
	public List<DieFace> GetFaces()
	{
        return currentFaces;
	}

    public void DelayedRoll()
	{
		this.CallActionDelayed(RegularRoll, 0.1f);
	}

    void RegularRoll()
    {
        Roll(true);
    }

	public void Roll(bool ensureDifferent = false, bool doRollingAnimation = true)
	{
        if (DiceManager.Instance.clatterSoundDelay <= 0f)
        {
            SoundManager.Instance.PlaySound("diceclatter");
            DiceManager.Instance.clatterSoundDelay = 0.15f;
        }

        List<DieFace> faces = new List<DieFace>(GetFaces());

		if(ensureDifferent)
			faces.RemoveAll(x => x == dieFaceType);

		dieFaceIndex = Helpers.RandomIntFromRange(0, faces.Count);

		dieFaceType = faces[dieFaceIndex];

		SetSprite(dieFaceType);

        if(doRollingAnimation)
            DoRollingAnimation();

        DiceManager.Instance.diceBox.UpdateObjectPositions();
	}

	public void SetSprite(DieFace type)
	{
        if (DieDef.GetDieFaceColor(type) != default(Color))
            dieFaceRenderer.color = DieDef.GetDieFaceColor(type);

        dieFaceRenderer.color = DieDef.GetDieColor(diceType);

		SetSprite(DieDef.GetDieFaceSpriteName(type));
	}

    public void SetSprite(string spriteName)
    {
        dieFaceRenderer.sprite = Helpers.GetSprite(spriteName);
    }

    internal static List<DieFace> AllFaces()
    {
        List<DieFace> allFaces = new List<DieFace>();

        foreach (DieFace face in Enum.GetValues(typeof(DieFace)))
            allFaces.Add(face);

        return allFaces;
    }

    Vector3 endPos;
	float timeSinceRolled = 0f;
	public const float ROLL_TIME = 1f;
	public Tweener rollingRotationTweener;
    internal void DoRollingAnimation()
	{
		if(rollingRotationTweener != null)
			rollingRotationTweener.Kill();

		endPos = Vector3.zero; //tweenerendpos
        float scaleFactor = 0.1f;

		Vector3[] path = { new Vector3(endPos.x + 100f * scaleFactor, endPos.y + 100f * scaleFactor, endPos.z),
				 new Vector3(endPos.x + 66f * scaleFactor, endPos.y + 0f * scaleFactor, endPos.z),
				 new Vector3(endPos.x + 33f * scaleFactor, endPos.y + 33f * scaleFactor, endPos.z), endPos };

        rollingAnimationTransform.transform.localPosition = path[0];

        rollingAnimationTransform.transform.rotation = Quaternion.identity;
		rollingAnimation = true;
		timeSinceRolled = 0f;

		rollingRotationTweener = rollingAnimationTransform.transform.DORotate(new Vector3(0f,0f,360f), ROLL_TIME, RotateMode.LocalAxisAdd);
        rollingAnimationTransform.transform.DOLocalPath(path, ROLL_TIME, PathType.CatmullRom).OnComplete(EndRolling).Play();

        Helpers.AdjustSortingOrders(this.gameObject, 20);
    }

    void EndRolling()
	{
		rollingAnimation = false;

		SetSprite(dieFaceType);
        Helpers.AdjustSortingOrders(this.gameObject, -20);
	}

    public int DieValue()
    {
        return DieDef.DiceFaceValue(dieFaceType);
    }

    public bool MatchesCostRequirement(DieFace costDieFace)
    {
        if (costDieFace == DieFace.None)
            return false;

        if (costDieFace == DieFace.Any || this.dieFaceType == DieFace.Any)
            return true;

        return DieDef.DiceFaceValue(costDieFace) == DieDef.DiceFaceValue(dieFaceType);
    }

    public bool IsPlayersDie()
    {
        return true;
    }

    public int GetSortValue()
    {
        return DieValue() * 1000 + dieId;
    }

    public FlatHexPoint GetHoveredPoint()
    {
        Vector3 point = this.transform.position;
        point += Vector3.left * bgRenderer.bounds.extents.x;
        point += Vector3.up * (bgRenderer.bounds.extents.y);

        //Helpers.ShowPointer(point, GameLayer.UI);

        return Helpers.GetHoveredPoint(point, gameObject.GetGameLayer());
    }

    public MonoBehaviour GetMonoBehaviour()
    {
        return this;
    }
}
