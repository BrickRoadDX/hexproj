﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBase : MonoBehaviour
{
    public SpriteRenderer backgroundRenderer;
    public SpriteRenderer spriteRenderer;

    [HideInInspector]
    public TileType tileType = TileType.None;

    [HideInInspector]
    public Cell cell;

    [HideInInspector]
    public int tileId = 0;

    public static int nextTileId = 1;

    public Dictionary<Direction, ExitType> exits = new Dictionary<Direction, ExitType>();
    public Dictionary<Direction, SpriteRendererGo> exitVisuals = new Dictionary<Direction, SpriteRendererGo>();
    public Transform exitContainer;

    public virtual void Init(TileType tType)
    {
        tileType = tType;

        if (!(this is Tile))
            Debug.LogError("Whoa, why is this the base class TileBase");

        this.spriteRenderer.transform.localScale = TileDef.GetTileScale(tType);

        tileId = nextTileId;
        nextTileId++;
        gameObject.name = tileType.ToString() + " " + tileId.ToString();

        spriteRenderer.sprite = Helpers.GetSprite(TileDef.GetSpriteName(tType));
        spriteRenderer.color = TileDef.GetSpriteColor(tType);
        backgroundRenderer.color = TileDef.GetBackroundColor(tType);
        backgroundRenderer.sprite = Helpers.GetSprite(Helpers.GetBasicShapeSpriteName());

        exits = TileDef.GetExits(tileType);

        UpdateExits();
    }

    internal static Tile CreateTile(TileType type)
    {
        Tile t = Helpers.CreateInstance<Tile>("Tile", null);
        t.Init(type);

        return t;
    }

    public void SetExits(Dictionary<Direction, ExitType> exits)
    {
        this.exits = exits;
        UpdateExits();
    }

    void UpdateExits()
    {
        exitContainer.gameObject.DestroyChildren();
        foreach(Direction d in exits.Keys)
        {
            ExitType type = exits[d];

            if(type != ExitType.None)
            {
                SpriteRendererGo srg = Helpers.MakeSpriteRenderer(TileDef.GetExitSpriteName(type), exitContainer, TileDef.GetExitColor(type), spriteRenderer.sortingOrder + 1);
                srg.transform.localScale = TileDef.GetExitScale(type);
                srg.transform.localPosition = backgroundRenderer.bounds.extents.x *  0.8f * Helpers.GetVectorDirFromDirection(d);
                exitVisuals[d] = srg;
            }
        }
        if (cell != null)
            CheckConnections();
    }

    public void RotateExits(bool right)
    {
        Dictionary<Direction, ExitType> cache = new Dictionary<Direction, ExitType>();
        foreach (Direction d in exits.Keys)
            cache.Add(d, exits[d]);

        foreach(Direction d in Helpers.MainDirections())
        {
            Direction next = Helpers.MainDirections().GetNextElement(d);
            if (right)
                next = Helpers.MainDirections().GetPrevElement(d);

            exits[next] = cache[d];
        }
    }

    public void CheckConnections()
    {
        foreach(Direction d in exits.Keys)
        {
            Tile match = CheckMatchInDirection(ExitType.Road, d);
            if(match != null)
            {
                exitVisuals[d].spriteRenderer.color = Color.cyan;
                match.exitVisuals[Helpers.GetOppositeDirection(d)].spriteRenderer.color = Color.cyan;
            }
        }
    }

    private Tile CheckMatchInDirection(ExitType type, Direction d)
    {
        if (exits[d] == type)
        {
            Cell other = Helpers.GetCellInDirection(cell, d);
            if (other != null && other.HasTile())
            {
                Direction opposite = Helpers.GetOppositeDirection(d);
                if (other.tile.exits[opposite] == type)
                    return other.tile;
            }
        }
        return null;
    }
}
