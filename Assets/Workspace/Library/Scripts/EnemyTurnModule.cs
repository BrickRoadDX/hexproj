﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Add to Game.cs as a field and instantiate inline
///     public EnemyTurnModule enemyTurn = new EnemyTurnModule();
///     then just call enemyTurn.StartEnemyTurn();
/// </summary>
public class EnemyTurnModule
{
    bool preMovesDone = true;
    bool isEnemyTurn = false;
    Sequence enemyTurn;
    List<Actor> enemiesToMove = null;
    List<Actor> preMoveActors = null;
    public void StartEnemyTurn()
    {
        if (isEnemyTurn)
            return;

        if (Game.Instance.IsGameOver() || InputManager.Instance.inputState == InputState.Ending)
            return;

        preMovesDone = false;

        InputManager.Instance.GoToState(InputState.EnemyTurn);

        enemyTurn = DOTween.Sequence();

        enemyTurn.AppendInterval(Constants.ENEMY_MOVE_TIME);

        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (a.HasPreMove())
            {
                PreMove(a);
            }
        }

        foreach (Actor a in Board.Instance.GetEnemyUnits())
            a.CacheEnemyMoves();

        enemiesToMove = Board.Instance.GetEnemyUnits();

        enemiesToMove.Sort((Actor x, Actor y) => ((int)(y.type) - (int)(x.type))); //takes an int

        foreach (Actor actor in enemiesToMove)
            MoveEnemy(actor);

        enemyTurn.AppendInterval(0.1f);

        enemyTurn.AppendCallback(EnemyTurnOver);
        if (!Game.Instance.IsGameOver())
            enemyTurn.Play();
    }

    void PreMove(Actor actor)
    {
        enemyTurn.AppendCallback(delegate () {
            if (!actor.dying)
            {
                enemyTurn.Pause();
                actor.DoPreMove(UnpauseEnemyTurn);
            }
        });
    }

    public void PauseEnemyTurn()
    {
        if (enemyTurn != null)
            enemyTurn.Pause();
    }

    void UnpauseEnemyTurn()
    {
        if (enemyTurn != null)
            enemyTurn.Play();
    }

    void MoveEnemy(Actor actor)
    {
        enemyTurn.AppendCallback(delegate ()
        {
            actor.cell.ShowColorAnimation(Color.white * 0.6f, 1f);
        });

        enemyTurn.AppendInterval(Constants.ENEMY_MOVE_TIME);

        enemyTurn.AppendCallback(delegate () {
            if (!enemiesToMove.First().dying)
            {
                actor.DoEnemyMove();
            }
            enemiesToMove.RemoveAt(0);
        });
    }

    void EnemyTurnOver()
    {
        Board.Instance.cacheAllEnemyMoves = true;

        InputManager.Instance.StartTurn();
    }
}
