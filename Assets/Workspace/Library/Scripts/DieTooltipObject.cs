﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class DieTooltipObject : TooltipObject
{
	public Transform dieTransform;

	public void Init(DiceType dieType, DieFace face = DieFace.None)
	{
        float xOffset = 0f;

        List<DieFace> faces = DieDef.GetFacesForDiceType(dieType);

        if (faces.Count > 6)
            xOffset -= 15f;

        tooltipText.text = DieDef.GetFaceDesc(face);

        if (tooltipText.text.Length > 0)
            dieTransform.SetLocalPositionY(95f);

        for (int i = 0; i < faces.Count; i++)
        {
            DieFace currFace = faces[i];
            DieFaceObject dfo = Helpers.CreateInstance<DieFaceObject>("DieFaceObject", dieTransform, true);
            dfo.transform.SetLocalPositionX(xOffset);

            dfo.dieBackground.color = DieDef.GetDieColor(dieType);
            dfo.dieSprite.sprite = Helpers.GetSprite(DieDef.GetDieFaceSpriteName(currFace));
            dfo.dieSprite.color = DieDef.GetDieFaceColor(currFace);

            dfo.highlightSprite.gameObject.SetActive(false);

            if (Helpers.IsMainMenuScene())
            {
                dfo.dieBackground.sortingOrder = 202;
                dfo.dieSprite.sortingOrder = 203;
            }

            xOffset += 25f;
        }
    }
}