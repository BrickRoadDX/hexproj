﻿using DG.Tweening;
using Gamelogic.Grids;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum DeathAnimation
{
    None = 0,
    Fly = 1,
    Fall = 2,
    PositionFall = 3,
    CustomDeathAnim = 4,
}

public class ActorBase : MonoBehaviour, AnimationPausable, BrainGoodActor, Sortable, SortingOrderObject
{
    public static int nextActorId = 1;

    [HideInInspector]
    public int actorId = 0;

    [HideInInspector]
    public Cell cell;

    [HideInInspector]
    public ActorType type;

    public TextMeshPro textMesh;
    public SpriteRenderer spriteRenderer;
    public SpriteOutline outline;

    [HideInInspector]
    public bool dying = false;

    [HideInInspector]
    public Cell previousPosition;

    [HideInInspector]
    public Direction movedDirection = Direction.None;

    public bool isReady = true;

    [HideInInspector]
    public Vector3 defaultScale = Vector3.one;

    public virtual void Init(ActorType aType)
    {
        if (!(this is Actor))
            Debug.LogError("Whoa, why is this the base class ActorBase");

        type = aType;

        textMesh.text = "";

        SetLocalScale(ActorDef.GetActorScale(aType));

        actorId = Actor.nextActorId;
        Actor.nextActorId++;
        gameObject.name = type.ToString() + " " + actorId.ToString();

        spriteRenderer.sprite = Helpers.GetSprite(ActorDef.GetSpriteName(type));
        spriteRenderer.color = ActorDef.GetActorColor(type);

        CacheSortingOrders();

        SetSelected(false);
        SetReady(true);
    }

    void SetLocalScale(Vector3 scale)
    {
        this.transform.localScale = scale;
        defaultScale = scale;
    }

    public void SetReady(bool ready)
    {
        isReady = ready;
        this.spriteRenderer.color = ActorDef.GetActorColor(type);

        if (!isReady)
            this.spriteRenderer.color = ActorDef.GetActorColor(type) * 0.5f;

        AbilityManager.Instance.UpdateAbilityVisuals();
    }

    internal void Attack(Actor attacker, int damage = 1)
    {
        bool playerCaused = false;

        if (attacker != null)
        {
            if (attacker.IsPlayerUnit())
                playerCaused = true;
        }

        if(!((Actor)this).TakeDamage(damage))
            cell.DestroyActor((Actor)this, DeathAnimation.Fly, Board.Instance.map[attacker.cell.point + GridHelpers.GetFlatHexPointFromDirection(attacker.movedDirection)] - Board.Instance.map[attacker.cell.point]);
    }

    public virtual void HandleActorKilled()
    {
        dying = true;

        Board.Instance.cacheAllEnemyMoves = true;
    }

    void SetMovedDirection(FlatHexPoint offset)
    {
        SetMovedDirection(GridHelpers.GetDirectionFromFlatHexPoint(offset));
    }

    void SetMovedDirection(Direction dir)
    {
        movedDirection = dir;
    }

    public void Move(FlatHexPoint newPoint)
    {
        if (Board.Instance.CellIsAvailalable(newPoint))
            Move(cell, newPoint.ToCell());
    }

    public void Move(Cell oldCell, Cell newCell)
    {
        if (newCell == oldCell)
            return;

        InputManager.Instance.rewindMoveCell = oldCell;

        SetMovedDirection(newCell.point - oldCell.point);

        Actor actorToAttack;

        actorToAttack = null;
        if (!newCell.HasRoomForActor())
            actorToAttack = newCell.GetRandomActor();

        bool attacked = false;

        if (actorToAttack != null)
        {
            actorToAttack.Attack((Actor)this);
            attacked = true;

            if (actorToAttack != null)
                actorToAttack.cell.DestroyActor(actorToAttack, DeathAnimation.Fly);

            InputManager.Instance.rewindMoveCell = null;
        }
        else
        {
            if (this.IsPlayerUnit())
                SoundManager.Instance.PlaySound("step");
        }

        if (!newCell.HasRoomForActor())
        {
            InputManager.Instance.rewindMoveCell = null;
            return;
        }

        if (oldCell.HasActor((Actor)this))
            oldCell.RemoveActor((Actor)this);

        previousPosition = cell;

        cell = newCell;

        bool animationHandled = HandleMoveAnimation(newCell);
        newCell.AddActor((Actor)this, false);

        if (this.IsPlayerUnit())
            Board.Instance.cacheAllEnemyMoves = true;

        ((Actor)this).HandleMoved();
    }

    public bool IsEnemy()
    {
        return ActorDef.IsEnemy(type);
    }

    public bool IsPlayerUnit()
    {
        return ActorDef.IsPlayerUnit(type);
    }

    public void EscapeAnim()
    {
        float duration = 1f;
        this.transform.DORotate(new Vector3(0f, 0f, 360f), duration, RotateMode.LocalAxisAdd).Play();
        this.transform.DOScale(2f, duration).Play();

        Helpers.MakeTemporary(this.gameObject, duration);
    }

    public virtual void CacheEnemyMoves()
    {
        Debug.Log("Enemy cached its moves");
    }

    public virtual void DoEnemyMove()
    {
        Debug.Log("Do enemy move");
    }

    public virtual void DoPreMove(Action unpauseEnemyTurn)
    {
        unpauseEnemyTurn();
    }

    public virtual bool HasPreMove()
    {
        return false;
    }

    public virtual bool TakeDamage(int damage = 1)
    {
        return false; //just die
    }

    public virtual void HandleMoved()
    {
        Debug.Log("Actor moved");
    }

    public List<Cell> validMoves = new List<Cell>();
    internal List<Cell> EnemyValidMoves()
    {
        return validMoves;
    }

    internal static List<ActorType> GetEnemyTypes()
    {
        List<ActorType> enemies = Helpers.GetAllEnumTypes<ActorType>(true);
        enemies.RemoveAll(x => !ActorDef.IsEnemy(x));
        return enemies;
    }

    internal static List<ActorType> GetPlayerUnitTypes()
    {
        List<ActorType> playerUnits = Helpers.GetAllEnumTypes<ActorType>(true);
        playerUnits.RemoveAll(x => !ActorDef.IsPlayerUnit(x));
        return playerUnits;
    }

    Tweener zoomTweener;
    public void Zoom(bool zoomedIn)
    {
        if(zoomTweener == null)
            zoomTweener = this.transform.DOScale(defaultScale * 1.2f, 0.19f).SetAutoKill(false).Pause();

        if (zoomedIn)
            zoomTweener.PlayForward();
        else
            zoomTweener.PlayBackwards();
    }

    internal FlatHexPoint GetPushDir(Cell targetCell) //direction from me to target cell
    {
        return targetCell.point - this.cell.point;
    }
	
	internal static Actor CreateActor(ActorType type)
    {
        Actor a = Helpers.CreateInstance<Actor>("Actor", null);
        a.Init(type);

        return a;
    }

    public bool moveAnimationPaused = false;
    public virtual bool HandleMoveAnimation(Cell Cell)
    {
        return false; //I am not handling this, so the actorDisplay should
    }

    public void UnpauseMoveAnimation()
    {
        moveAnimationPaused = false;
    }

    public bool IsAnimationPaused()
    {
        return moveAnimationPaused;
    }

    private List<Renderer> renderers = new List<Renderer>();
    private List<int> sortingOrder = new List<int>();
    void CacheSortingOrders()
    {
        renderers.Clear();
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        sortingOrder.Clear();
        foreach (Renderer r in this.renderers)
            sortingOrder.Add(r.sortingOrder);
    }

    public void ResetSortingOrders()
    {
        int index = 0;
        foreach (Renderer r in this.renderers)
        {
            r.sortingOrder = sortingOrder[index];
            index++;
        }
    }

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

    public BoxCollider GetCollider()
    {
        return null;
    }

    public int GetSortValue()
    {
        if (selected)
            return 2;

        return 1;
    }

    private bool selected = false;
    internal void SetSelected(bool isSelected)
    {
        this.selected = isSelected;
        outline.enabled = isSelected;

        if(cell != null)
        {
            List<MonoBehaviour> objects = new List<MonoBehaviour>(cell.actorDisplay.displayObjects);
            objects.Sort((MonoBehaviour x, MonoBehaviour y) => ((Sortable)x).GetSortValue() - ((Sortable)y).GetSortValue()); //takes an int

            Helpers.UpdateSortingOrderObjects(objects);
        }
    }
}
