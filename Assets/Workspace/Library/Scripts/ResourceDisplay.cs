﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

///Add instances of this to the scene to override default text-based resource tracking
public class ResourceDisplay : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public TextMeshPro textMeshPro;
    public ResourceType resourceType;
    public Transform gainTarget;
    private Color color;
    public TooltipShower tooltipShower;

    private void Awake()
    {
        Init();
    }

    public void Init(ResourceType type)
    {
        resourceType = type;
        Init();
    }

    void Init()
    {
        color = ResourceDef.GetResourceColor(resourceType);
        spriteRenderer.sprite = Helpers.GetSprite(ResourceDef.GetResourceIconString(resourceType));
        spriteRenderer.color = color;
        textMeshPro.color = color;

        if(tooltipShower != null)
        {
            tooltipShower.textToShow = ResourceDef.GetResourceDesc(resourceType);
        }
    }

    public void UpdateText(string newText)
    {
        textMeshPro.text = newText;
    }

}
