﻿using UnityEngine;
using System.Collections;
using System;

public class DieTooltipShower : TooltipShower
{
    public Die die;

    [HideInInspector]
	public DiceType diceType;

	void Awake()
	{
		prefabName = "DieTooltipObject";
	}

    public override TooltipObject ShowTooltip(Vector3 worldPos = default(Vector3))
    {
        if (PopupManager.Instance.IsShowingPopup())
            return null;

		TooltipObject tooltipObject = base.ShowTooltip();

		if (tooltipObject != null)
		{
			tooltipObject.GetComponent<DieTooltipObject>().Init(diceType, die != null ? die.dieFaceType : DieFace.None);
		}

		return tooltipObject;
	}

    internal void SetDie(Die die)
    {
        this.die = die;
        this.diceType = die.diceType;
    }
}
