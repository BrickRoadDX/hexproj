﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    private int maxHp;
    private int currentHp;

    public ObjectDisplay pipDisplay;

    public void Init(int hp, string prefabName, Color color = default(Color))
    {
        maxHp = hp;
        currentHp = hp;

        for(int i = 0; i< hp; i++)
        {
            HealthBarPip hbp = Helpers.CreateInstance<HealthBarPip>(prefabName, this.transform, true);
            pipDisplay.xOffset = hbp.X_OFFSET;
            pipDisplay.AddObject(hbp);

            if(color != default(Color))
                hbp.healthBarFill.color = color;
        }

        UpdateHealth(maxHp);
    }

    public void UpdateHealth(int currentHp)
    {
        this.currentHp = currentHp;
        List<HealthBarPip> pips = pipDisplay.GetCastedList<HealthBarPip>();
        for(int i = 0; i < maxHp; i++)
        {
            pips[i].healthBarFill.gameObject.SetActive(currentHp > i);
        }

        this.gameObject.SetActive(this.currentHp > 0);

    }

    internal void AdjustHp(int amount)
    {
        SetHp(currentHp + amount);
    }

    internal void SetHp(int newHp)
    {
        currentHp = Mathf.Min(newHp, maxHp);

        UpdateHealth(currentHp);
    }

    public bool IsDead()
    {
        return currentHp < 1;
    }

    internal bool IsMax()
    {
        return currentHp == maxHp;
    }

    public int CurrentHp()
    {
        return currentHp;
    }

    internal int MaxHp()
    {
        return maxHp;
    }
}
