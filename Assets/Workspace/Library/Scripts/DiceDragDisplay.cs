﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DiceDragDisplay : DragDisplay
{
    public TextMeshPro pipCountText;

    public override void Awake()
    {
        base.Awake();
        pipCountText.text = "";
    }

    public override void Drop(Draggable toDrop)
    {
        base.Drop(toDrop);

        int pipCount = 0;
        foreach (Die d in GetCastedList<Die>())
            pipCount += d.DieValue();

        pipCountText.text = "Total Die Value: " + pipCount.ToString();
    }
}
