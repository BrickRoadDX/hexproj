﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public enum Player
{
    None = 0,
    You = 1,
    Opponent = 2,
}

public class CardManagerBase : Singleton<CardManagerBase>
{
    public bool gameHasCards = true;

    public bool canRotateCards = false;
    public const int MAX_HAND_SIZE = 12;
    public const int STARTING_HAND_SIZE = 5;

    public const float DEFAULT_HAND_Y_POS = 4f;
    public const float CASTING_HAND_Y_POS = 1f;

    public ObjectDisplay playerHandDisplay;
    public ObjectDisplay castingCardDisplay;

    public List<CardID> playerDeck = new List<CardID>();
    public List<CardID> discard = new List<CardID>();

    [HideInInspector]
    public bool tabletMode = false;

    [HideInInspector]
    public Camera tk2dCamera;

    [HideInInspector]
    internal bool canRewindCard;

    [HideInInspector]
    public float soundDelay = 0f;

    float pressTime = 0f;

    [HideInInspector]
    public List<CardPile> cardPiles = new List<CardPile>(); //auto populated in CardPile

    private ScrollwheelHandler scrollwheelHandler;

    public override void Awake()
    {
        if (!(this is CardManager))
            Debug.LogError("Whoa, why is this the base class CardManagerBase");

        scrollwheelHandler = Helpers.MakeScrollWheelHandler(this.transform, RightScroll, LeftScroll);

        base.Awake();

        if (!gameHasCards)
            return;

        if (playerHandDisplay == null)
            Debug.LogError("No player hand display set in CardManager");

        if (castingCardDisplay == null)
            Debug.LogError("No casting card display set in CardManager");
    }

    private void LeftScroll()
    {
        if(GetLastCastCard() != null)
            GetLastCastCard().Rotate(false);
    }

    private void RightScroll()
    {
        if (GetLastCastCard() != null)
            GetLastCastCard().Rotate(true);
    }

    public void Init()
    {
        if (!gameHasCards)
            return;

        this.StopDelayedActions();

        playerHandDisplay.ClearAndDestroyAll();
        castingCardDisplay.ClearAndDestroyAll();

        playerHandDisplay.objectContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0f);

        playerDeck.Clear();
        discard.Clear();

        soundDelay = 0f;

        UpdateCardDisplays();
    }

    public void MakeRandomTestDeck()
    {
        for (int i = 0; i < 10; i++)
            AddToPlayerDeck(Helpers.GetRandomEnumType<CardType>());
    }

    public void MakeTestDeckForCard(CardType type)
    {
        for (int i = 0; i < 10; i++)
            AddToPlayerDeck(type);
    }

    public void AddToPlayerDeck(CardType cardType)
    {
        playerDeck.Add(new CardID(cardType));
        UpdateCardDisplays();
    }

    public void MakeRandomTestDiscardPile()
    {
        for (int i = 0; i < 20; i++)
            AddNewCardToDiscard(Helpers.GetRandomEnumType<CardType>());
    }

    public void ReshuffleDiscard()
    {
        playerDeck.AddRange(discard);
        discard.Clear();
        playerDeck.Shuffle();
        Debug.Log(string.Format("Reshuffled {0} cards  " + GetCardListString(playerDeck), playerDeck.Count));
        UpdateCardDisplays();
    }

    string GetCardListString(List<CardID> cards)
    {
        string cardIDstring = "";
        foreach (CardID c in cards)
            cardIDstring += c.GetCardString() + ",";

        cardIDstring = cardIDstring.Trim(',');
        return cardIDstring;
    }

    public void DrawCards(int numCards)
    {
        for (int i = 0; i < numCards; i++)
            DrawCard();
    }

    public void DrawUpTo(int numCards, Player player = Player.You, bool fast = false)
    {
        ObjectDisplay hand = GetHand(player);

        if (playerDeck.Count < 1)
            Debug.LogError("Player deck has no cards to draw");

        while (hand.Count < numCards && (playerDeck.Count > 0 || discard.Count > 0))
        {
            DrawCard(null, fast);
        }
    }

    public void DrawCard(CardID particularCardToDraw = null, bool fast = false)
    {
        if (GetHand(Player.You).Count >= MAX_HAND_SIZE)
        {
            Helpers.ShowReminderText(Helpers.AddIcons("Overdrew!"));
            return;
        }

        if (particularCardToDraw == null && playerDeck.Count < 1) //drawing card from deck
        {
            ReshuffleDiscard();
        }

        if (playerDeck.Count < 1)
        {
            Helpers.ShowReminderText("Nothing to draw!");
            return;
        }

        CardID drawnCard = playerDeck[0];
        playerDeck.RemoveAt(0);

        Card co = CreateCard(drawnCard);

        playerHandDisplay.AddObject(co, fast);

        co.SetColliderSize(true);
        UpdateCardDisplays();
    }

    internal bool IsCastingCard(Card cardTocheck)
    {
        return castingCardDisplay.Contains(cardTocheck);
    }

    public Card CreateCard(CardType type = CardType.None)
    {
        if (type == CardType.None)
            type = Helpers.GetRandomEnumType<CardType>();

        return CreateCard(new CardID(type));
    }

    public Card CreateCard(CardID cardID)
    {
        Card co = Helpers.CreateInstance<Card>("CardObject", null);
        co.SetupCard(cardID);
        return co;
    }

    internal void DiscardCard(Card cardToDiscard)
    {
        if (cardToDiscard == null)
            return;

        discard.Add(cardToDiscard.cardID);

        if (castingCardDisplay.Contains(cardToDiscard))
            castingCardDisplay.RemoveObject(cardToDiscard);

        if (playerHandDisplay.Contains(cardToDiscard))
            playerHandDisplay.RemoveObject(cardToDiscard);

        if (dragCard == cardToDiscard)
            dragCard = null;

        cardToDiscard.CleanUp();

        SetHandPos(false);

        UpdateCardDisplays();
    }

    public void AddNewCardToDiscard(CardType type)
    {
        discard.Add(new CardID(type));
        UpdateCardDisplays();
    }

    internal Card GetLastCastCard()
    {
        if (castingCardDisplay.Count < 1)
            return null;

        return castingCardDisplay.GetCastedList<Card>().Last();
    }

    internal bool HasCastingCard()
    {
        return castingCardDisplay.Count > 0;
    }

    public void UpdateCardDisplays()
    {
        foreach (CardPile pile in cardPiles)
            pile.UpdateCardPile();
    }

    [HideInInspector]
    public Card hoveredCard;
    Collider raycastCollider = null;
    CardHoverTransform hitCardHoverTransform = null;
    Card newHoveredCard;
    float middleXpos = 0f;
    void LateUpdate()
    {
        if (!gameHasCards)
            return;

        if (soundDelay > 0f)
            soundDelay -= Time.deltaTime;

        if (!Game.HasPrivateInstance() || Game.Instance.IsGameOver())
            return;

        hitCardHoverTransform = Helpers.RayCastForObjectType<CardHoverTransform>(Helpers.tk2dCamera);

        if (hitCardHoverTransform != null)
        {
            newHoveredCard = hitCardHoverTransform.card;

            if (newHoveredCard != null && hoveredCard != null && newHoveredCard != hoveredCard)
            {
                HoverOut(hoveredCard);
            }

            if (hoveredCard != newHoveredCard && newHoveredCard != null)
                NewHoveredCard(newHoveredCard);

            hoveredCard = newHoveredCard;
        }
        else if (hoveredCard != null)
        {
            HoverOut(hoveredCard);
        }

        if(!HasCastingCard())
            HandleDrag();

        if(tabletMode)
        {
            if (!Input.GetMouseButton(0) && hoveredCard != null)
            {
                middleXpos = hoveredCard.transform.localPosition.x;
                pressTime = 0f;
            }

            if (Input.GetMouseButton(0) && hoveredCard != null)
            {
                pressTime += Time.deltaTime;
                hoveredCard.transform.SetLocalPositionX(middleXpos + (pressTime * Helpers.RandomFloatFromRangeInclusive(-15f, 15f)));

                if (pressTime >= 1f)
                    DiscardHoveredCard();
            }
        }
    }

    public bool TryUndo()
    {
        if (HasCastingCard())
        {
            UndoPlayedCard();
            return true;
        }

        if (hoveredCard != null && newHoveredCard.IsInPlayerHand())
        {
            DiscardHoveredCard();
            return true;
        }

        return false;
    }

    public Card dragCard;
    private Vector3 lastMousePos = Vector3.zero;
    Vector3 startMousePos = Vector3.zero;   
    void HandleDrag()
    {
        if (hoveredCard != null)
        {
            if (Input.GetMouseButtonDown(0))
            {               
                if(hoveredCard.IsInPlayerHand())
                {
                    dragCard = hoveredCard;
                    PickUp();
                }
            }
        }

        if (Input.GetMouseButton(0)) //continue drag
        {
            if (dragCard != null)
                Drag();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (dragCard != null)
                Drop(dragCard);
        }
    }

    public virtual void PickUp()
    {
        lastMousePos = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);
        startMousePos = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);

        readyToDragPlay = false;
    }

    bool readyToDragPlay = false;
    public virtual void Drag()
    {
        Vector3 mouseMove = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition) - lastMousePos;
        dragCard.transform.position += mouseMove;
        lastMousePos = Helpers.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);

        if(!readyToDragPlay && Vector3.Distance(lastMousePos, startMousePos) > 2f)
        {
            dragCard.transform.parent = null;

            SetHandPos(true);

            readyToDragPlay = true;

            if (playerHandDisplay.Contains(dragCard))
                playerHandDisplay.RemoveObject(dragCard);

            if (CardDef.CanPlayOnBoard(dragCard.cardType))
                Board.Instance.ShowCardPlacements(dragCard);
        }

        if (!readyToDragPlay)
            return;

        Board.Instance.RemoveCellVisuals();
        if(dragCard.GetHoveredPoint().HasCell())
            Board.Instance.CreateCellVisual(dragCard.GetHoveredPoint().ToCell());
    }

    public virtual void Drop(Card toDrop)
    {
        if (DragDisplay.ReadyToDrop())
        {
            RemoveCardFromCasting(toDrop);
            DragDisplay.hoveredDisplay.Drop(toDrop);
            dragCard = null;
            return;
        }

        bool playingOnBoard = true;
        Cell toDropCell = toDrop.GetHoveredPoint().ToCell();

        if (!CardDef.CanPlayOnBoard(dragCard.cardType))
            playingOnBoard = false;

        if (toDropCell == null)
            playingOnBoard = false;

        if (!Board.Instance.cellHighlightCells.Contains(toDropCell))
            playingOnBoard = false;

        if (playingOnBoard)
        {
            if(CardDef.MovesToBoard(toDrop.cardType))
            {
                MoveCardToBoard(toDrop.GetHoveredPoint().ToCell(), toDrop);
            }
            else
            {
                PlayCard(toDrop);
                InputManager.Instance.HandleCellClicked(toDrop.GetHoveredPoint().ToCell());
            }
        }
        else
        {
            returnCard = toDrop;
            this.CallActionDelayed(ReturnCard);

            SetHandPos(false);

            InputManager.Instance.GoToBaseState();
        }

        dragCard = null;
    }

    Card returnCard;
    void ReturnCard()
    {
        if (returnCard == null)
            return;

        returnCard.cardCollider.enabled = true;

        if (!playerHandDisplay.Contains(returnCard))
            playerHandDisplay.AddObject(returnCard);
    }

    void NewHoveredCard(Card card)
    {
        if (soundDelay <= 0f)
            SoundManager.Instance.PlaySound("bip");

        //Game.Instance.ShowHoveredCard(newHoveredCardObject.card.type, null);

        card.SetCardHovered(true);
    }

    void HoverOut(Card card)
    {
        hoveredCard = null;
        card.SetCardHovered(false);
    }

    public void DiscardHoveredCard()
    {
        pressTime = 0f;

        DiscardCard(hoveredCard);
    }

    public bool CanUndo()
    {
        return canRewindCard && InputManager.Instance.CanUndoCards();
    }

    public void UndoPlayedCard()
    {
        if (HasCastingCard() && CanUndo())
        {
            soundDelay = 0.3f;
            SoundManager.Instance.PlaySound("warning");
            Card toUndo = GetLastCastCard();

            castingCardDisplay.RemoveObject(toUndo);
            playerHandDisplay.AddObject(toUndo);

            toUndo.SetRotation(Direction.Up);

            toUndo.SetColliderSize(true);

            toUndo.SetCardHovered(false);

            UpdateCardDisplays();

            if(castingCardDisplay.Count < 1)
            {
                InputManager.Instance.GoToBaseState();
                SetHandPos(false);
            }

            InputManager.Instance.HandleCardUndo(toUndo);
        }
    }

    public void DiscardHand(Player player)
    {
        DiscardTo(0, player);
    }

    public ObjectDisplay GetHand(Player player)
    {
        if (player == Player.You)
            return playerHandDisplay;

        Debug.Log("no hand defined for opponent");

        return null;
    }

    internal void DiscardTo(int numCards, Player player)
    {
        ObjectDisplay hand = GetHand(player);
        Helpers.StartWhileLoopCounter();
        while (hand.Count > numCards && Helpers.WhileLoopTick())
            hand.RemoveRandomObject();
    }

    public virtual bool PlayCard(Card cardToCast)
    {
        dragCard = null;

        SoundManager.Instance.PlaySound("doo dee");

        cardToCast.SetColliderSize(false);

        playerHandDisplay.RemoveObject(cardToCast);

        SetHandPos(true);

        castingCardDisplay.AddObject(cardToCast);

        canRewindCard = true;

        InputManager.Instance.GoToState(InputState.CastingCard);

        if(CardDef.CanPlayOnBoard(cardToCast.cardType))
        {
            Board.Instance.ShowCardPlacements(cardToCast);
            InputManager.Instance.GoToState(InputState.ChoosingSpace);
            return false;
        }
        return true;
    }

    void SetHandPos(bool castingCard)
    {
        foreach(Card card in GetHand(Player.You).GetCastedList<Card>())
            card.cardCollider.enabled = !castingCard;

        if(castingCard)
        {
            playerHandDisplay.DOKill();
            playerHandDisplay.objectContainer.DOLocalMoveY(CASTING_HAND_Y_POS, 0.5f);
        }
        else
        {
            playerHandDisplay.objectContainer.DOKill();
            playerHandDisplay.objectContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0.5f);
        }
    }

    internal void MoveCardToBoard(Cell cell, Card cardToMove = null)
    {
        if (cardToMove == null)
            cardToMove = GetLastCastCard();

        Helpers.SetLayer(cardToMove.gameObject, GameLayer.Default);
        RemoveCardFromCasting(cardToMove);
        cell.cardDisplay.AddObject(cardToMove);
    }

    public void RemoveCardFromCasting(Card cardToMove)
    {
        if (cardToMove == null)
            cardToMove = GetLastCastCard();

        castingCardDisplay.RemoveObject(cardToMove);
        cardToMove.HandleRemovedFromCardManager();
        cardToMove.hoverZoom.Init(CardDef.CardScaleOnBoard());
        SetHandPos(false);
        InputManager.Instance.GoToBaseState();
    }

    public void DonePlayingCard()
    {
        DiscardLastCastedCard();
        InputManager.Instance.GoToBaseState();
    }

    internal void DiscardLastCastedCard()
    {
        DiscardCard(GetLastCastCard());
    }

    public void TryClickCard(int cardNum)
    {
        if(playerHandDisplay.Count >= cardNum)
        {
            playerHandDisplay.GetCastedList<Card>()[cardNum - 1].HandleCardClicked();
        }
    }

    internal bool CardDisplayUp()
    {
        foreach (CardPile pile in cardPiles)
            if (pile.DisplayUp())
                return true;

        return false;
    }

    internal void ShowCardMoves(Card card, Cell origin)
    {
        if (card.cellPatternObject == null)
            return;

        if (!card.cellPatternObject.HasSpaceWithType(SpaceType.Start))
            return;

        Board.Instance.ClearDebugText();
        Board.Instance.RemoveCellHighlights();

        List<Cell> moves = new List<Cell>();

        CellPatternSpace start = card.cellPatternObject.GetSpaceWithType(SpaceType.Start);
        CellPatternSpace end = card.cellPatternObject.GetSpaceWithType(SpaceType.End);

        FlatHexPoint diff = end.position - start.position;

        foreach (Direction d in Helpers.MainDirections())
        {
            FlatHexPoint p = GridHelpers.GetRotatedFlatHexPoint(diff, d);
            FlatHexPoint newPoint = origin.point + p;
            if (newPoint.HasCell())
                moves.Add(newPoint.ToCell());
        }
        diff = diff.ReflectAboutX();
        foreach (Direction d in Helpers.MainDirections())
        {
            FlatHexPoint p = GridHelpers.GetRotatedFlatHexPoint(diff, d);
            FlatHexPoint newPoint = origin.point + p;
            if (newPoint.HasCell())
                moves.Add(newPoint.ToCell());
        }

        Board.Instance.HighlightCells(moves);

    }
}
