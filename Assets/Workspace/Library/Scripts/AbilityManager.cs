﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AbilityManager : Singleton<AbilityManager>
{
    public ObjectDisplay abilityButtonDisplay;
    public bool allowDuplicates = true;

    private AbilityType currentAbility;
    private AbilityButton currentAbilityButton;

    [HideInInspector]
    public int actionsRemaining = 1;

    public bool canRewind = true;

    public void Init()
    {
        abilityButtonDisplay.ClearAndDestroyAll();
    }

    public AbilityType CurrentAbility()
    {
        if (!IsUsingAbility())
            return AbilityType.None;

        return currentAbility;
    }

    public bool IsUsingAbility()
    {
        return currentAbility != AbilityType.None;
    }

    public void AbilityClicked(AbilityType type, AbilityButton abilityButton)
    {
        currentAbilityButton = abilityButton;
        currentAbility = type;
        InputManager.Instance.HandleAbiliityClicked(type);

        if (actionsRemaining < 1)//just starting first activation
        {
            canRewind = true;   
            actionsRemaining = AbilityDef.NumActions(type);
        }

        if(AbilityDef.RequiresUnitSelection(type))
        {
            Board.Instance.ShowReadyPlayerUnits();
            InputManager.Instance.GoToState(InputState.ChoosingSpace);
            ShowOnlyCurrentButton();
        }
        else
        {
            AbilityDef.DoAbility(type);
        }
    }

    public virtual bool TryUndo()
    {
        if(IsUsingAbility() && canRewind)
        {
            InputManager.Instance.TryUndoMove();
            RemoveCurrentAbility();
            return true;
        }

        if (IsUsingAbility() && !canRewind)
            return true; //we don't want to do normal selection/move rewinds if we're in the middle of something

        return false;
    }

    public void AbilityDone(bool remove = true)
    {
        if(actionsRemaining > 1)
        {
            InputManager.Instance.GoToBaseState();
            canRewind = false;
            actionsRemaining--;
            AbilityClicked(currentAbility, currentAbilityButton);
        }
        else
        {
            currentAbilityButton.actionsRemainingText.text = "";

            if (remove)
                DestroyAbilityButton(currentAbility);

            RemoveCurrentAbility();

            actionsRemaining = 0;
        }
    }

    void RemoveCurrentAbility()
    {
        canRewind = false;

        currentAbilityButton = null;
        currentAbility = AbilityType.None;
        InputManager.Instance.GoToBaseState();

        actionsRemaining = 0;

        UpdateAbilityVisuals();
    }

    public void UpdateAbilityVisuals()
    {
        foreach (AbilityButton button in abilityButtonDisplay.GetCastedList<AbilityButton>())
        {
            button.gameObject.SetActive(true);
            button.myCollider.enabled = true;

            if (AbilityDef.CanBeUsed(button.abilityType))
            {
                button.Init(button.abilityType);
            }
            else
            {
                button.abilitySprite.color = Color.gray + (Color.white * 0.2f);
                button.colorSetter.Init(Color.gray);
                button.myCollider.enabled = false;
            }
        }
    }

    public void AddAbility(AbilityType type)
    {
        if(!HasAbility(type) || allowDuplicates)
            CreateAbilityButton(type);

        UpdateAbilityVisuals();
    }

    public AbilityButton CreateAbilityButton(AbilityType type)
    {
        AbilityButton ab = Helpers.CreateInstance<AbilityButton>("AbilityButton", null);
        ab.Init(type);
        abilityButtonDisplay.AddObject(ab);
        return ab;
    }

    bool HasAbility(AbilityType type)
    {
        return abilityButtonDisplay.GetCastedList<AbilityButton>().Any(x=> x.abilityType == type);
    }

    public void DestroyAbilityButton(AbilityType type)
    {
        if(HasAbility(type))
        {
            AbilityButton b = abilityButtonDisplay.GetCastedList<AbilityButton>().Find(x => x.abilityType == type);
            abilityButtonDisplay.RemoveObject(b, RemoveAnim.GoToCenterScreenAndDestroy);
        }
        UpdateAbilityVisuals();
    }

    internal void ShowOnlyCurrentButton()
    {
        foreach (AbilityButton button in abilityButtonDisplay.GetCastedList<AbilityButton>())
            button.gameObject.SetActive(false);

        currentAbilityButton.gameObject.SetActive(true);
        currentAbilityButton.myCollider.enabled = false;
        currentAbilityButton.actionsRemainingText.text = actionsRemaining.ToString();
    }

    internal void DoNewAbility(AbilityType type)
    {
        AbilityButton ab = CreateAbilityButton(type);
        AbilityClicked(type, ab);
    }
}
