﻿using Gamelogic.Grids;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CellPattern : MonoBehaviour
{
    public Dictionary<FlatHexPoint, CellPatternSpace> patternSpaces = new Dictionary<FlatHexPoint, CellPatternSpace>();

    public List<CellPatternSpace> cellPatternSpaces = new List<CellPatternSpace>();
    public CellPatternType cellPatternType;

    public Direction currentDir = Direction.Up;
    bool placed = false;

    public bool makeVisuals = true;

    public void Init(CellPatternType cellPatternType)
    {
        if (!makeVisuals)
            return;

        this.cellPatternType = cellPatternType;
        foreach(CellPatternSpace space in patternSpaces.Values)
        {
            space.transform.parent = this.transform;
            space.cellPatternObject = this;

            Vector3 offset = Helpers.GetVectorPositionOffsetFromOrigin(space.position);
            space.transform.localPosition = offset;
            cellPatternSpaces.Add(space);
        }
        SetPlaced(false);
    }

    public void AddPatternSpace(CellPatternSpace toAdd)
    {
        patternSpaces.Add(toAdd.position, toAdd);
    }

    public CellPatternSpace GetSpaceWithType(SpaceType type)
    {
        foreach (CellPatternSpace space in patternSpaces.Values)
        {
            if (space.spaceType == type)
                return space;
        }
        return null;
    }

    internal bool HasSpaceWithType(SpaceType type)
    {
        return GetSpaceWithType(type) != null;
    }

    internal CellPatternSpace GetOriginSpace()
    {
        if (!patternSpaces.ContainsKey(new FlatHexPoint(0, 0)))
            Debug.LogError(string.Format("The cell pattern {0} does not have a point at 0,0", cellPatternType));

        return patternSpaces[new FlatHexPoint(0, 0)];
    }


    internal void LineUpMouse(Transform cps, Vector3 targetPos)
    {
        Vector3 originPos = cps.position;
        Vector3 offset = targetPos - originPos;
        offset.z = 0f;

        this.transform.position += offset;
    }

    public CellPatternSpace GetOriginSpaceObject()
    {
        return GetOriginSpace();
    }

    internal void SetColliders(bool active)
    {
        foreach(CellPatternSpace cps in cellPatternSpaces)
            cps.myCollider.enabled = active;
    }

    internal void Rotate(bool right, CellPatternSpace rotateAround)
    {
        Direction nextDirection = Helpers.MainDirections().GetNextElement(currentDir);
        if(right)
            nextDirection = Helpers.MainDirections().GetPrevElement(currentDir);

        transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Helpers.GetAngleFromDirection(nextDirection)));
        currentDir = nextDirection;

        LineUpMouse(rotateAround.transform, Game.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition));

        foreach (CellPatternSpace obj in GetCellPatternSpaceObjectList())
            obj.HandleRotated(right);
    }

    public List<Cell> GetBoardSpaces()
    {
        return Board.Instance.GetCellPatternBoardSpaces(this);
    }

    public bool IsOverAtleastOneBoardSpace()
    {
        return GetBoardSpaces().Count > 0;
    }

    public bool IsCompletelyOverBoardSpaces()
    {
        return GetBoardSpaces().Count >= patternSpaces.Count;
    }

    public void SetPlaced(bool placed)
    {
        this.placed = placed;
        Color color = placed ? Color.white : Color.magenta;
        foreach (CellPatternSpace cps in cellPatternSpaces)
            cps.spriteRenderer.color = color;
    }

    internal bool IsPlaced()
    {
        return placed;
    }

    public Dictionary<Cell, CellPatternSpace> GetBoardSpaceDict()
    {
        Dictionary<Cell, CellPatternSpace> dict = new Dictionary<Cell, CellPatternSpace>();
        foreach (CellPatternSpace cps in cellPatternSpaces)
        {
            Cell c = Board.Instance.GetBoardPointUnderCellPatternSpace(cps).ToCell();
            if (c != null)
            {
                dict.Add(c, cps);
            }
        }
        return dict;
    }

    public List<CellPatternSpace> GetCellPatternSpaceObjectList()
    {
        List<CellPatternSpace> list = new List<CellPatternSpace>();
        foreach (CellPatternSpace cps in cellPatternSpaces)
            list.Add(cps);
        return list;
    }

    public void DestroySelf()
    {
        GetCellPatternSpaceObjectList().ClearAndDestroyList();

        this.gameObject.DestroySelf();

        CellPatternManager.Instance.HandleDestroyed(this);
    }

}
